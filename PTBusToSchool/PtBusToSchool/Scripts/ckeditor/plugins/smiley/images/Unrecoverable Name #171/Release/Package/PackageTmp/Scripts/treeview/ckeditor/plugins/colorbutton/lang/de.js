      </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Generate(System.Data.Entity.Migrations.Model.AddPrimaryKeyOperation)">
            <summary>
                Generates SQL for a <see cref="T:System.Data.Entity.Migrations.Model.AddPrimaryKeyOperation"/>.
                Generated SQL should be added using the Statement method.
            </summary>
            <param name="addPrimaryKeyOperation">The operation to produce SQL for.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Generate(System.Data.Entity.Migrations.Model.DropPrimaryKeyOperation)">
            <summary>
                Generates SQL for a <see cref="T:System.Data.Entity.Migrations.Model.DropPrimaryKeyOperation"/>.
                Generated SQL should be added using the Statement method.
            </sum