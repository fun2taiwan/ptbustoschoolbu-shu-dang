cture.DbCompiledModel.CachedModelBuilder">
            <summary>
            A snapshot of the <see cref="T:System.Data.Entity.DbModelBuilder"/> that was used to create this compiled model.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbCompiledModel.ProviderInfo">
            <summary>
            The provider info (provider name and manifest token) that was used to create this model.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbComplexPropertyEntry">
            <summary>
                A non-generic version of the <see cref="T:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2"/> class.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbPropertyEntry">
            <summary>
                A non-generic version of the <see cref="T:System.Data.Entity.Infrastructure.DbPropertyEntry`2"/> class.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyEntry.Create(System.Data.Entity.Internal.InternalPropertyEntry)">
            <summary>
                Creates a <see cref="T:System.Data.Entity.Infrastructure.DbPropertyEntry"/> from information in the given <see cref="T:System.Data.Entity.Internal.InternalPropertyEntry"/>.
                Use this method in preference to the constructor since it may potentially create a subclass depending on
                the type of member represented by the InternalCollectionEntry instance.
            </summary>
            <param name="internalPropertyEntry">The internal property entry.</param>
            <returns>The new entry.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyEntry.#ctor(System.Data.Entity.Internal.InternalPropertyEntry)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbPropertyEntry"/> class.
            </summary>
            <param name="internalPropertyEntry">The internal entry.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbPropertyEntry.Cast``2">
            <summary>
                Returns the equivalent generic <see cref="T:System.Data.Entity.Infrastructure.DbPropertyEntry`2"/> object.
            </summary>
            <typeparam name="TEntity">The type of entity on which the member is declared.</typeparam>
            <typeparam name="TProperty">The type of the property.</typeparam>
            <returns>The equivalent generic object.</returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbPropertyEntry.Name">
            <summary>
                Gets the property name.
            </summary>
            <value>The property name.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbPropertyEntry.OriginalValue">
            <summary>
                Gets or sets the original value of this property.
            </summary>
            <value>The original value.</value>
        </member>
        <member name="P:Syste