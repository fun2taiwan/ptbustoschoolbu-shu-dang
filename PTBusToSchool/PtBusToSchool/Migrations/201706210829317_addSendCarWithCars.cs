namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSendCarWithCars : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendCars", "Cars", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SendCars", "Cars");
        }
    }
}
