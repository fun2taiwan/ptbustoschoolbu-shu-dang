    <summary>
                Non-composable functions do not permit the aggregate; niladic; or built-in attributes.
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.ComposableFunctionWithCommandText">
            <summary>
                Composable functions can not include command text attribute.
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.FunctionDeclaresCommandTextAndStoreFunctionName">
            <summary>
                Functions should not declare both a store name and command text (only one or the other can be used).
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.SystemNamespace">
            <summary>
                SystemNamespace
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.EmptyDefiningQuery">
            <summary>
                Empty DefiningQuery text
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.TableAndSchemaAreMutuallyExclusiveWithDefiningQuery">
            <summary>
                Schema, Table and DefiningQuery are all specified, and are mutually exclusive
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.InvalidConcurrencyMode">
            <summary>
                ConcurrencyMode value was malformed
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.ConcurrencyRedefinedOnSubTypeOfEntitySetType">
            <summary>
                Concurrency can't change for any sub types of an EntitySet type.
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.FunctionImportUnsupportedReturnType">
            <summary>
                Function import return type must be either empty, a collection of entities, or a singleton scalar.
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.F