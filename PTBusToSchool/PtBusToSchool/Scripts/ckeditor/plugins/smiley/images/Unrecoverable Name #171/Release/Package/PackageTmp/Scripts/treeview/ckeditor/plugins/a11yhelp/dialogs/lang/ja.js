<returns>The documentation for the controller.</returns>
      <param name="parameterDescriptor">The parameter descriptor.</param>
    </member>
    <member name="T:System.Web.Http.Dispatcher.DefaultAssembliesResolver">
      <summary> Provides an implementation of <see cref="T:System.Web.Http.Dispatcher.IAssembliesResolver" /> with no external dependencies. </summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultAssembliesResolver.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Dispatcher.DefaultAssembliesResolver" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultAssembliesResolver.GetAssemblies">
      <summary> Returns a list of assemblies available for the application. </summary>
      <returns>A &lt;see cref="T:System.Collections.ObjectModel.Collection`1" /&gt; of assemblies.</returns>
    </member>
    <member name="T:System.Web.Http.Dispatcher.DefaultHttpControllerActivator">
      <summary>Represents a default implementation of an <see cref="T:System.Web.Http.Dispatcher.IHttpControllerActivator" />. A different implementation can be registered via the <see cref="T:System.Web.Http.Services.DependencyResolver" />. We optimize for the case where we have an <see cref="T:System.Web.Http.Controllers.ApiControllerActionInvoker" />  instance per <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> instance but can support cases where there are many <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> instances for one <see cref="T:System.Web.Http.Controllers.ApiControllerActionInvoker" />  as well. In the latter case the lookup is slightly slower because it goes through the <see cref="P:HttpControllerDescriptor.Properties" /> dictionary. </summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultHttpControllerActivator.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Dispatcher.DefaultHttpControllerActivator" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultHttpControllerActivator.Create(System.Net.Http.HttpRequestMessage,System.Web.Http.Controllers.HttpControllerDescriptor,System.Type)">
      <summary> Creates the <see cref="T:System.Web.Http.Controllers.IHttpController" /> specified by <paramref name="controllerType" /> using the given <paramref name="request" />.</summary>
      <returns>An instance of type <paramref name="controllerType" />.</returns>
      <param name="request">The request message.</param>
      <param name="controllerDescriptor">The controller descriptor.</param>
      <param name="controllerType">The type of the controller.</param>
    </member>
    <member name="T:System.Web.Http.Dispatcher.DefaultHttpControllerSelector">
      <summary>Represents a default <see cref="T:System.Web.Http.Dispatcher.IHttpControllerSelector" /> instance for choosing a <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> given a <see cref="T:System.Net.Http.HttpRequestMessage" />. A different implementation can be registered via the <see cref="P:System.Web.Http.HttpConfiguration.Services" />. </summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultHttpControllerSelector.#ctor(System.Web.Http.HttpConfiguration)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.Dispatcher.DefaultHttpControllerSelector" /> class.</summary>
      <param name="configuration">The configuration.</param>
    </member>
    <member name="F:System.Web.Http.Dispatcher.DefaultHttpControllerSelector.ControllerSuffix">
      <summary>Specifies the suffix string in the controller name.</summary>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultHttpControllerSelector.GetControllerMapping">
      <summary>Returns a map, keyed by controller string, of all <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> that the selector can select. </summary>
      <returns>A map of all <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> that the selector can select, or null if the selector does not have a well-defined mapping of <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" />.</returns>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultHttpControllerSelector.GetControllerName(System.Net.Http.HttpRequestMessage)">
      <summary>Gets the name of the controller for the specified <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>The name of the controller for the specified <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message.</param>
    </member>
    <member name="M:System.Web.Http.Dispatcher.DefaultHttpControllerSelector.SelectController(System.Net.Http.HttpRequestMessage)">
      <summary>Selects a <see cref="T:System.Web.Http.Controllers.HttpControllerDescriptor" /> for the given <see cref="