﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;


namespace PtBusToSchool.Models
{
    public class MailTemplate
    {
        public String MailSubject { get; set; }
        
        public MailTemplate()
        {
            MailSubject = "新聞局數據平台系統通知";
        }

     


        public void SendForgetPasswordEmail(String account, String password, String Email)
        {
            string mailBody = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/MailTemp/ForgetPasswordEmail.html"), System.Text.Encoding.UTF8);
            mailBody = mailBody.Replace("##account##", account);
            mailBody = mailBody.Replace("##password##", password);

            String mailFrom = ConfigurationManager.AppSettings["MailFrom"];
            String mailPassword = ConfigurationManager.AppSettings["MailPassword"];

            Utility.SendGmailMail(mailFrom, Email, MailSubject, mailBody, mailPassword);
        }
    }
}