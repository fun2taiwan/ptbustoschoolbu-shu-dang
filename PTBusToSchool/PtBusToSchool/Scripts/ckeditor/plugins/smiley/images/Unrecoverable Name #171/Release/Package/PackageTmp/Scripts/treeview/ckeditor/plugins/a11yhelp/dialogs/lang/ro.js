lts">An object that contains the default route parameters.</param>
      <param name="constraints">An object that contains the route constraints.</param>
      <param name="dataTokens">The route data tokens.</param>
      <param name="handler">The message handler for the route.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.CreateRoute(System.String,System.Object,System.Object)">
      <summary>Creates an <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instance.</summary>
      <returns>The new <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instance.</returns>
      <param name="routeTemplate">The route template.</param>
      <param name="defaults">An object that contains the default route parameters.</param>
      <param name="constraints">An object that contains the route constraints.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.Dispose">
      <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.Dispose(System.Boolean)">
      <summary>Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.</summary>
      <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.GetEnumerator">
      <summary>Returns an enumerator that iterates through the collection.</summary>
      <returns>An <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.</returns>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.GetRouteData(System.Net.Http.HttpRequestMessage)">
      <summary>Gets the route data for a specified HTTP request.</summary>
      <returns>An<see cref="T:System.Web.Http.Routing.IHttpRouteData" /> instance that represents the route data.</returns>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.GetVirtualPath(System.Net.Http.HttpRequestMessage,System.String,System.Collections.Generic.IDictionary{System.String,System.Object})">
      <summary>Gets a virtual path.</summary>
      <returns>An <see cref="T:System.Web.Http.Routing.IHttpVirtualPathData" /> instance that represents the virtual path.</returns>
      <param name="request">The HTTP request.</param>
      <param name="name">The route name.</param>
      <param name="values">The route values.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.Insert(System.Int32,System.String,System.Web.Http.Routing.IHttpRoute)">
      <summary>Inserts an <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instance into the collection.</summary>
      <param name="index">The zero-based index at which <paramref name="value" /> should be inserted.</param>
      <param name="name">The route name.</param>
      <param name="value">The <see cref="T:System.Web.Http.Routing.IHttpRoute" /> to insert. The value cannot be null.</param>
    </member>
    <member name="P:System.Web.Http.HttpRouteCollection.IsReadOnly">
      <summary>Gets a value indicating whether the collection is read-only.</summary>
      <returns>true if the collection is read-only; otherwise, false.</returns>
    </member>
    <member name="P:System.Web.Http.HttpRouteCollection.Item(System.Int32)">
      <summary>Gets or sets the element at the specified index.</summary>
      <returns>The  <see cref="T:System.Web.Http.Routing.IHttpRoute" /> at the specified index.</returns>
      <param name="index">The zero-based index of the element to get or set.</param>
    </member>
    <member name="P:System.Web.Http.HttpRouteCollection.Item(System.String)">
      <summary>Gets or sets the element with the specified route name.</summary>
      <returns>The  <see cref="T:System.Web.Http.Routing.IHttpRoute" /> at the specified index.</returns>
      <param name="name">The route name.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.O