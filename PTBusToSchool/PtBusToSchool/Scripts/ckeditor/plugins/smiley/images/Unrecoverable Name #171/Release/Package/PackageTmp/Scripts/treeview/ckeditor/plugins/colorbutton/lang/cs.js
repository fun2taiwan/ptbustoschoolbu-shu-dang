 <summary>
                Quotes an identifier for SQL Server.
            </summary>
            <param name = "identifier">The identifier to be quoted.</param>
            <returns>The quoted identifier.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Statement(System.String,System.Boolean)">
            <summary>
                Adds a new Statement to be executed against the database.
            </summary>
            <param name = "sql">The statement to be executed.</param>
            <param name = "suppressTransaction">
                Gets or sets a value indicating whether this statement should be performed outside of
                the transaction scope that is used to make the migration process transactional.
                If set to true, this operation will not be rolled back if the migration process fails.
            </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Writer"