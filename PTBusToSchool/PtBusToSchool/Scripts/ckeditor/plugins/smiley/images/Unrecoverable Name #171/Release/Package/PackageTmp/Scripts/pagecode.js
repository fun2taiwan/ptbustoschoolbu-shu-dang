on-generic version.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbMemberEntry`2.GetValidationErrors">
            <summary>
                Validates this property.
            </summary>
            <returns>
                Collection of <see cref="T:System.Data.Entity.Validation.DbValidationError"/> objects. Never null. If the entity is valid the collection will be empty.
            </returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbMemberEntry`2.CurrentValue">
            <summary>
                Gets or sets the current value of this property.
            </summary>
            <value>The current value.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbMemberEntry`2.InternalMemberEntry">
            <summary>
                