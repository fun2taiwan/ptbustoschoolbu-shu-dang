namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addIsHaveTimeTableInCarLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CarLines", "IsHaveTimeTable", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CarLines", "IsHaveTimeTable");
        }
    }
}
