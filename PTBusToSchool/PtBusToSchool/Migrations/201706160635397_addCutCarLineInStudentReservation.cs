namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCutCarLineInStudentReservation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentReservations", "CutCarLine", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentReservations", "CutCarLine");
        }
    }
}
