#9758](http://dev.ckeditor.com/ticket/9758): Improved selection locking when selecting by dragging.
* Context menu:
    * [#9712](http://dev.ckeditor.com/ticket/9712): Opening the context menu destroys editor focus.
    * [#9366](http://dev.ckeditor.com/ticket/9366): Context menu should be displayed over the floating toolbar.
    * [#9706](http://dev.ckeditor.com/ticket/9706): Context menu generates a JavaScript error in inline mode when the editor is attached to a header element.
* [#9800](http://dev.ckeditor.com/ticket/9800): Hide float panel when resizing the window.
* [#9721](http://dev.ckeditor.com/ticket/9721): Padding in content of div-based editor puts the editing area under the bottom UI space.
* [#9528](http://dev.ckeditor.com/ticket/9528): Host page `box-sizing` style should not influence the editor UI elements.
* [#9503](http://dev.ckeditor.com/ticket/9503): [Form Elements](http://ckeditor.com/addon/forms) plugin adds context menu listeners only on supported input types. Added support for `tel`, `email`, `search` and `url` input types.
* [#9769](http://dev.ckeditor.com/ticket/9769): Improved floating toolbar positioning in a narrow window.
* [#9875](http://dev.ckeditor.com/ticket/9875): Table dialog window does not populate width correctly.
* [#8675](http://dev.ckeditor.com/ticket/8675): Deleting cells in a nested table removes the outer table cell.
* [#9815](http://dev.ckeditor.com/ticket/9815): Cannot edit dialog window fields in an editor initialized in the jQuery UI modal dialog.
* [#8888](http://dev.ckeditor.com/ticket/8888): CKEditor dialog windows do not show completely in a small window.
* [#9360](http://dev.ckeditor.com/ticket/9360): [Inline editor] Blocks shown for a `<div>` element stay permanently even after the user exits editing the `<div>`.
* [#9531](http://dev.ckeditor.com/ticket/9531): [Firefox & Inline editor] Toolbar is lost when closing the Format drop-down list by clicking its button.
* [#9553](http://dev.ckeditor.com/ticket/9553): Table width incorrectly set when the `border-width` style is specified.
* [#9594](http://dev.ckeditor.com/ticket/9594): Cannot tab past CKEditor when it is in read-only mode.
* [#9658](http://dev.ckeditor.com/ticket/9658): [IE9] Justify not working on selected images.
* [#9686](http://dev.ckeditor.com/ticket/9686): Added missing contents styles for `<pre>` elements.
* [#9709](http://dev.ckeditor.com/ticket/9709): [Paste from Word](http://ckeditor.com/addon/pastefromword) should not depend on configuration from other styles.
* [#9726](http://dev.ckeditor.com/ticket/9726): Removed [Color Dialog](http://ckeditor.com/addon/colordialog) plugin dependency from [Table Tools](http://ckeditor.com/addon/tabletools).
* [#9765](http://dev.ckeditor.com/ticket/9765): Toolbar Collapse command documented incorrectly in the [Accessibility Instructions](http://ckeditor.com/addon/a11yhelp) dialog window.
* [#9771](http://dev.ckeditor.com/ticket/9771): [WebKit & Opera] Fixed scrolling issues when pasting.
* [#9787](http://dev.ckeditor.com/ticket/9787): [IE9] `onChange` is not fired for checkboxes in dialogs.
* [#9842](http://dev.ckeditor.com/ticket/9842): [Firefox 17] When opening a toolbar menu for the first time and pressing the *Down Arrow* key, focus goes to the next toolbar button instead of the menu options.
* [#9847](http://dev.ckeditor.com/ticket/9847): [Elements Path](http://ckeditor.com/addon/elementspath) should not be initialized in the inline editor.
* [#9853](http://dev.ckeditor.com/ticket/9853): [`editor.addRemoveFormatFilter()`](http://docs.ckeditor.com/#!/api/CKEDITOR.editor-method-addRemoveFormatFilter) is exposed before it really works.
* [#8893](http://dev.ckeditor.com/ticket/8893): Value of the [`pasteFromWordCleanupFile`](http://docs.ckeditor.com/#!/api/CKEDITOR.config-cfg-pasteFromWordCleanupFile) configuration option is now taken from the instance configuration.
* [#9693](http://dev.ckeditor.com/ticket/9693): Removed "Live Preview" checkbox from UI color picker.


## CKEditor 4.0

The first stable release of the new CKEditor 4 code line.

The CKEditor JavaScript API has been kept compatible with CKEditor 4, whenever
possible. The list of relevant changes can be found in the [API Changes page of
the CKEditor 4 documentation][1].

[1]: http://docs.ckeditor.com/#!/guide/dev_api_changes "API Changes"
                                                                                                                                                                                                                                      n  �    $  �  �      $  RSA1     ����g�w:�މ8���e� `Y>�Ēe���?�?�1��3��`!g-��1����/%�}�o��Y���5�L9�EC��;�&=�����o���GP�\d��E��k*+G��e+��]�o     1�8V�6N5q  �                 w  System.Reflection.AssemblyName   _Name
_PublicKey_PublicKeyToken_CultureInfo	_CodeBase_Version_HashAlgorithm_HashAlgorithmForControl_StrongNameKeyPair_VersionCompatibility_Flags_HashForControl System.Version5System.Configuration.Assemblies.AssemblyHashAlgorithm5System.Configuration.Assemblies.AssemblyHashAlgorithm#System.Reflection.StrongNameKeyPair<System.Configuration.Assemblies.AssemblyVersionCompatibility#System.Reflection.AssemblyNameFlags`  mscorlib
	a     
	b  ����f���    ����f���    
����d���   ����c���    
x  �      	�      y  System.Reflection.AssemblyName   _Name
_PublicKey_PublicKeyToken_CultureInfo	_CodeBase_Version_HashAlgorithm_HashAlgorithmForControl_StrongNameKeyPair_VersionCompatibility_Flags_HashForControl System.Version5System.Configuration.Assemblies.AssemblyHashAlgorithm5System.Configuration.Assemblies.AssemblyHashAlgorithm#System.Reflection.StrongNameKeyPair<System.Configuration.Assemblies.AssemblyVersionCompatibility#System.Reflection.AssemblyNameFlagsh  %System.ComponentModel.DataAnnotations
	i     
	j  ����f���    ����f���    
����d���   ����c���    
z  �      	�      {  System.Reflection.AssemblyName   _Name
_PublicKey_PublicKeyToken_CultureInfo	_CodeBase_Version_HashAlgorithm_HashAlgorithmForControl_StrongNameKeyPair_VersionCompatibility_Flags_HashForControl System.Version5System.Configuration.Assemblies.AssemblyHashAlgorithm5System.Configuration.Assemblies.AssemblyHashAlgorithm#System.Reflection.StrongNameKeyPair<System.Configuration.Assemblies.AssemblyVersionCompatibility#System.Reflection.AssemblyNameFlagsp  System.Web.Razor
	q     
	r  ����f���    ����f���    
����d���   ����c���    
|  �      	�      }  System.Reflection.AssemblyName   _Name
_PublicKey_PublicKeyToken_CultureInfo	_CodeBase_Version_HashAlgorithm_HashAlgorithmForControl_StrongNameKeyPair_VersionCompatibility_Flags_HashForControl System.Version5System.Configuration.Assemblies.AssemblyHashAlgorithm5System.Configuration.Assemblies.AssemblyHashAlgorithm#System.Reflection.StrongNameKeyPair<System.Configuration.Assemblies.AssemblyVersionCompatibility#System.Reflection.AssemblyNameFlagsx  System.Core
	y     
	z  ����f���    ����f���    
����d���   ����c���    
~  �      	�        System.Reflection.AssemblyName   _Name
_PublicKey_PublicKeyToken_CultureInfo	_CodeBase_Version_HashAlgorithm_HashAlgorithmForControl_StrongNameKeyPair_VersionCompatibility_Flags_HashForControl System.Version5System.Configuration.Assemblies.AssemblyHashAlgorithm5System.Configuration.Assemblies.AssemblyHashAlgorithm#System.Reflection.StrongNameKeyPair<System.Configuration.Assemblies.AssemblyVersionCompatibility#System.Reflection.AssemblyNameFlags�  System.Web.WebPages.Razor
	�     
	�  }���f���    |���f���    
{���d���   z���c���    
�  �      	�      �  System.Reflection.AssemblyName   _Name
_PublicKey_PublicKeyToken_CultureInfo	_CodeBase_Version_HashAlgorithm_HashAlgorithmForControl_StrongNameKeyPair_VersionCompatibility_Flags_HashForControl System.Version5System.Configuration.Assemblies.AssemblyHashAlgorithm5System.Configuration.Assemblies.AssemblyHashAlgorithm#System.Reflection.StrongNam���� JFIF  T T  �� C 		
 $.' ",#(7),01444'9=82<.342�� C			2!!22222222222222222222222222222222222222222222222222��  p �" ��           	
�� �   } !1AQa"q2���#B��R��$3br�	
%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������        	
�� �  w !1AQaq"2�B����	#3R�br�
$4�%�&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������   ? ��zfsJU�8��刌��TN:WCi+�����	?t�K�K�ܰ�-��>���:��J�X�##�ҳU��Vg n8~+a���DX�	^�J�' �zP��B̛#gb~U�ɬ��e��I${�m�8��f��5�ݤ%L��y*�3~��� ��ʷ�e��Ȝ��H�<V^�7��#ԚT�IY�*q�H�*+��P$�B�6�ӧZ�>���qKab��^����O�V+��vi�����o؆�S;S����F�c�X�����7Z�N�sX��an��bu;��t:֝����`��[$�sH�QW�5Ĵv�s%͕�3DF�f;�R}�������̈I"�r,�pi����F�mf9�Q�O����Ѯ�f��BeH��s`��z�(���U�G���n��Q�X�##�@�Hү/��i,M�FT����8�g���.�4�2Y��5=I&���%.�FT�H c�o����х����G*�HA�T�W�E#��,"wB��{�N�;���Tg����Yb���a�.�
�Qp0���|�J]������zv�k_�ym���ZH�<�pH�]V��
�S�ѽ�0[i��l�r�ɓ��ǯz�|g}yms�,V��۫N�&$��q�z�h��$W�%�B4IVUh��ѹ��>Pl�Լ1����_iZ����:��F8V�q�:�9�Ҋ�n�|��nGΙ��*��bp(��}��#�t�[�_4�]҈���'߽ex�Y����YZ���FD�mP 9=8�k���BX5{�C��� *F8>���
��,���[ZG�2�D������q�ӭs�IJ��-Jv8{]F��i�s� ��H�6�n���}+����]��Lmf(ɀ��}1^dVm:��qL0�>pOC�����~5��4�a�KV�������r���QE=]�}��ċ]k�;�8�K\u��{`{�5��n�]ͼ��8&FbFv��9� "��P���aLevs:N�����}=�޵�$�t�9�Ak4i,�m�G9��2��q�=)Ӫ�~���Z��˭�$�Z���͵H����i�6�,�C����@v��z�95�sw-�Ս�ks++Gs�&<�s�I�k2��6���e*�|s��ڱI��g��^[��{����9 ����ҳm.jR8����01�z�l�<֌L6K�f�������D�0�T���{p8�F�}R`�ѯp� m�UX(����<3��|�mf� ,w���m��̑��t>��)�s^�%���-I�]�a�Yآ~�^U9��s��#�{7���-�<�[1�`��>{��5m^�I	�� $�޹�f��f�����J�C�c���*n��E��2�/!�uM>���y6�i� �Q�u��ܚ�ϐ������l�{�aO�.��[�{�y���"E8���>�<�I�vD2�#����%�a�]�?�Z��W�R]��U�D,�q�c��W��7�:�[�a30�H�[0$�n���^éxz�9����k�
7^� ����M���C�Kud�M2���w @���˝����<�.i�d�O���
AۜgߨW��Z��I�t��ӭ�SV�B� �A��)�3��Q-
ʉo�O���s�`T�F��]�a�N�Id�<�8�.Ԉ嶝��79�Iovm�F���s���U�#��jI<�y_*��Ǟ��t��S�sI$λ^ �j3��T��P����h1�)*�3�k"�Ha��̌C����S��/��BkĚ��VRdh،����]dz������}��gvfW$��<;מ�,S]"�#cs��S�� ���T�l���
1Ǚ����k8$�:[x5d��q۹�Pt���)�~�k�D��If10f+&Gr�cD��ݥ�s	'v>B`R�$���ҥ�]V����,�h�d0���&�
7�I]�����������Q�ڞ��GoK��9�׺9��y��v�eymsp�g�s $���c���j�����Hyc��
�1H7tx#��bd�9�㠭D�<y�Erۢ�t�!�d`6g��=�gI�`���B����{W�����9*F���͌OMm���9��T���nX�o;��v:U�
Æ}j;���2�L{`rk�n��Rl@�c�g4��y/�߲ZOW!�~����>��k[S$
��~����������q���
W�Q��O�y��d%���k
n�'�t�SE��ե��1�c�Kr�5Է���mm4�YՔ3�ȣ�b:m�}k���n%���dB�a���p��5�$m��Gbn��]&�G�=T�?^_:ZD�H�?�"��q&��#?���<��(�Ick����K���P�`�P�-�� QS����8/�9o>f��~C��Q�?x�"G���$�W�X|'������Te1�^MȒ���W�jz-֓�ͦj��-���1�v��M-l>�-h���0���H�Ye߁��y���LԮg�'����Ff����4q���}��
ܵ�ຶ�đ�Čvy� ����P�b�#,����UI@�w=�&H_%���
5Z����%´m�^�{J�����#9A���Z�Q���Kv�N�,m�d'����m.��m��%勫,���pU��8Ͻah:�ZC1x��ll�ۻ��֬ז��c=��.c%v���ڢ֕˄y����i0��TI��j�p���j;�����Hd1fA�����]=����2�,��A<cx�5�4��w��ގLf ;q�kMc���E������ʶv�� ��?�X<:�/㼀��Q�%}G�ҡ���K^����^8,�;�#ʞ{wԢ����I�;��>���+��ώ%E�zc�y��F�>�$NR4�b=��νY������x�K�T��ҬMi�+���-O:zS���m2��Gss&F�.s�g𨎶ʆD4��C�} l�<g����$��˖M����O�5Bg
%��I�����Go�y.��t�mY�ꑪ-�_i���5~;�Ĩ�bD�2wH�l{ר8���-F�;�¦Y$�K�����z[�Z���J�n�p[,8ǥe�gm�Z���mu}e<1�q�Z\3�<� � :*u�����-�d�u��l~�V���W36�,�"�����������x9�5O_x5;D^��p����5�H�|��9��c����f���QT�Ȍ��?P3��I6Ӌ[����L���O�
$@g�Nk`Yh�3-�����cb9>����y�M�I�DG����O~v�=j+f�*��c�&{{�wv�h�����C�ݣ��`pp{��Y�p��=+��[���i#��Nr�@��i���7V���1� ��x�� Z���v2�)]�!���J�54Z}�bt�_5U�f$c��(�;��<�$rݫ8hU*����$}jrd%�����$� �=p�t��4��߲hZ����W 2��s�������_�4�������|�!ϘOor*X�C�,��"]�� X^��H�WT�3�܊G� ��Vi��\�}
Ay��4�+lT�h���s������ǥyŧ�&��0ۛ�-��dc�ec���k/[�u�X)��)�C*�2O�Z<J��˖��α�xG�`��pˑ���\޵���6����Gŀd s��\�6���䗹���