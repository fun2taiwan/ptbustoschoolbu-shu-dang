.String,System.Data.Entity.Migrations.Utilities.IndentedTextWriter,System.String,System.Boolean,System.Collections.Generic.IEnumerable{System.String})">
            <summary>
                Generates a namespace, using statements and class definition.
            </summary>
            <param name = "namespace">Namespace that code should be generated in.</param>
            <param name = "className">Name of the class that should be generated.</param>
            <param name = "writer">Text writer to add the generated code to.</param>
            <param name = "base">Base class for the generated class.</param>
            <param name = "designer">A value indicating if this class is being generated for a code-behind file.</param>
            <param name="namespaces">Namespaces for which Imports directives will be added. If null, then the namespaces returned from GetDefaultNamespaces will be used.</param>
        </member>
        <member name="M:S