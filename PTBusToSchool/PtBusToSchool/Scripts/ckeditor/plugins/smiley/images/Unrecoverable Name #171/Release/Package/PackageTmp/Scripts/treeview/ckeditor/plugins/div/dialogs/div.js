value.
            </summary>
            <param name="value">The value.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JValue"/> string with the given value.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.CreateNull">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Linq.JValue"/> null value.
            </summary>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JValue"/> null value.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.CreateUndefined">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Linq.JValue"/> undefined value.
            </summary>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JValue"/> undefined value.</returns>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JValue.Type">
            <summary>
            Gets the node type for this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <value>The type.</value>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JValue.Value">
            <summary>
            Gets or sets the underlying token value.
            </summary>
            <value>The underlying token value.</value>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])">
            <summary>
            Writes this token to a <see cref="T:Newtonsoft.Json.JsonWriter"/>.
            </summary>
            <param name="writer">A <see cref="T:Newtonsoft.Json.JsonWriter"/> into which this method will write.</param>
            <param name="converters">A collection of <see cref="T:Newtonsoft.Json.JsonConverter"/> which will be used when writing the token.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.Equals(Newtonsoft.Json.Linq.JValue)">
            <summary>
            Indicates whether the current object is equal to another object of the same type.
            </summary>
            <returns>
            true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
            </returns>
            <param name="other">An object to compare with this object.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.Equals(System.Object)">
            <summary>
            Determines whether the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>.
            </summary>
            <param name="obj">The <see cref="T:System.Object"/> to compare with the current <see cref="T:System.Object"/>.</param>
            <returns>
            true if the specified <see cref="T:System.Object"/> is equal to the current <see cref="T:System.Object"/>; otherwise, false.
            </returns>
            <exception cref="T:System.NullReferenceException">
            The <paramref name="obj"/> parameter is null.
            </exception>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.GetHashCode">
            <summary>
            Serves as a hash function for a particular type.
            </summary>
            <returns>
            A hash code for the current <see cref="T:System.Object"/>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.ToString">
            <summary>
            Returns a <see cref="T:System.String"/> that represents this instance.
            </summary>
            <returns>
            A <see cref="T:System.String"/> that represents this instance.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.ToString(System.String)">
            <summary>
            Returns a <see cref="T:System.String"/> that represents this instance.
            </summary>
            <param name="format">The format.</param>
            <returns>
            A <see cref="T:System.String"/> that represents this instance.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JValue.ToString(System.IFormatProvider)">
            <summary>
            Returns a <see cref="T:System.String"/> that represents this instance.
            </summary>
            <param name="formatProvider">The format provider.</param>
            <returns>
            A <see cref="T:System.String"/> that represents this instance.
            </r