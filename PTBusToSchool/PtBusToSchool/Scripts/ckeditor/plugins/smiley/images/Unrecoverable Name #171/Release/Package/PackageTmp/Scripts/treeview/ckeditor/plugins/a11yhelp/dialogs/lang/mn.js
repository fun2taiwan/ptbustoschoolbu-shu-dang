t already exist on the server.
            </summary>
            <returns>True if the database did not exist and was created; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.Exists">
            <summary>
                Checks whether or not the database exists on the server.
            </summary>
            <returns>True if the database exists; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.Delete">
            <summary>
                Deletes the database on the database server if it exists, otherwise does nothing.
                Calling this method from outside of an initializer will mark the database as having
                not been initialized. This means that if an attempt is made to use the database again
                after it has been deleted, then any initializer set will run again and, usually, will
                try to create the database again automatically.
            </summary>
            <returns>True if the database did exist and was deleted; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.Exists(System.String)">
            <summary>
                Checks whether or not the database exists on the server.
                The connection to the database is created using the given database name or connection string
                in the same way as is described in the documentation for the <see cref="T:System.Data.Entity.DbContext"/> class.
            </summary>
            <param name="nameOrConnectionString">The database name or a connection string to the database.</param>
            <returns>True if the database exists; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.Delete(System.String)">
            <summary>
                Deletes the database on the database server if it exists, otherwise does nothing.
                The connection to the database is created using the given database name or connection string
                in the same way as is described in the documentation for the <see cref="T:System.Data.Entity.DbContext"/> class.
            </summary>
            <param name="nameOrConnectionString">The database name or a connection string to the database.</param>
            <returns>True if the database did exist and was deleted; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.Exists(System.Data.Common.DbConnection)">
            <summary>
                Checks whether or not the database exists on the server.
            </summary>
            <param name = "existingConnection">An existing connection to the database.</param>
            <returns>True if the database exists; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.Delete(System.Data.Common.DbConnection)">
            <summary>
                Deletes the database on the database server if it exists, otherwise does nothing.
            </summary>
            <param name = "existingConnection">An existing connection to the database.</param>
            <returns>True if the database did exist and was deleted; false otherwise.</returns>
        </member>
        <member name="M:System.Data.Entity.Database.ResetDefaultConnectionFactory">
            <summary>
                Resets the DefaultConnectionFactory to its initial value.
                Currently, this method is only used by test code.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Database.PerformDatabaseOp(System.Data.Entity.Internal.LazyInternalConnection,System.Func{System.Data.Objects.ObjectContext,System.Boolean})">
            <summary>
                Performs the operation defined by the given delegate using the given lazy connection, ensuring
   