n.
                It will be passed to <see cref="T:System.ComponentModel.DataAnnotations.ValidationContext"/>
                and will be exposed as <see cref="P:System.ComponentModel.DataAnnotations.ValidationContext.Items"/>.
                This parameter is optional and can be null.</param>
            <returns>Entity validation result. Possibly null when overridden.</returns>
        </member>
        <member name="M:System.Data.Entity.DbContext.CallValidateEntity(System.Data.Entity.Infrastructure.DbEntityEntry)">
            <summary>
                Internal method that calls the protected ValidateEntity method.
            </summary>
            <param name="entityEntry">DbEntityEntry instance to be validated.</param>
            <param name="items">User defined dictionary containing additional info for custom validation.
                It wil