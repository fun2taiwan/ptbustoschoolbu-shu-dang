﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class SendCar :BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

          //ForeignKey
        [Display(Name = "上層編號")]
        public int? ParentId { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentId")]
        public virtual SendCar ParentSendCar { get; set; }


          [Display(Name = "發車日期")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? StartDate { get; set; }


       //ForeignKey
        [Display(Name = "路線名稱")]
        public int? CarLineId { get; set; }

        [ForeignKey("CarLineId")]
        [JsonIgnore]
        public virtual CarLine LineName { get; set; }

        //ForeignKey
        [Display(Name = "班次")]
        public int? TimeScheduleId { get; set; }

        [ForeignKey("TimeScheduleId")]
        [JsonIgnore]
        public virtual TimeSchedule CarNoTime { get; set; }

        [Display(Name = "預約人數")]
        public string People { get; set; }

        [Display(Name = "候補人數")]//已達每站車上最大容客量
        public string NoReservationPeople { get; set; }

        [Display(Name = "派車數")]
        public int? CarNum { get; set; }

        [Display(Name = "上上層")]
        public int? GrandParentId { get; set; }

        [Display(Name = "派車")]
        public string Cars { get; set; }

        [Display(Name = "載客數")]
        public int? Passengers { get; set; }

        [Display(Name = "駕駛")]
        public string Drivers { get; set; }

        [Display(Name = "層級")]
        [NotMapped]
        public int Level
        {
            get
            {
                int level = 1;
                SendCar parentClass = this.ParentSendCar;
                while (parentClass != null)
                {
                    level++;
                    parentClass = parentClass.ParentSendCar;
                }

                return level;
            }

        }



        public virtual ICollection<SendCar> ChildSendCar{ get; set; }
    }

    
}