namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTimetableWithCarNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timetables", "CarNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Timetables", "CarNo");
        }
    }
}
