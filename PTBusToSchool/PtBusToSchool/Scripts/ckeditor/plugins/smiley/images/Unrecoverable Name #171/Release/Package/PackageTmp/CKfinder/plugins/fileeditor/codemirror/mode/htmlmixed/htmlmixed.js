member name="F:Newtonsoft.Json.ReferenceLoopHandling.Serialize">
            <summary>
            Serialize loop references.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Required">
            <summary>
            Indicating whether a property is required.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Required.Default">
            <summary>
            The property is not required. The default state.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Required.AllowNull">
            <summary>
            The property must be defined in JSON but can be a null value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Required.Always">
            <summary>
            The property must be defined in JSON and cannot be a null value.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.Required.DisallowNull">
            <summary>
            The property is not required but it cannot be a null value.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.Schema.Extensions">
            <summary>
            <para>
            Contains the JSON schema extension methods.
            </para>
            <note type="caution">
            JSON Schema validation has been moved to its own package. See <see href="http://www.newtonsoft.com/jsonschema">http://www.newtonsoft.com/jsonschema</see> for more details.
            </note>
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Schema.Extensions.IsValid(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema)">
            <summary>
            <para>
            Determines whether the <see cref="T:Newtonsoft.Json.Linq.JToken"/> is valid.
            </para>
            <note type="caution">
            JSON Schema validation has been moved to its own package. See <see href="http://www.newtonsoft.com/jsonschema">http://www.newtonsoft.com/jsonschema</see> for more details.
            </note>
            </summary>
            <param name="source">The source <see cref="T:Newtonsoft.Json.Linq.JToken"/> to test.</param>
            <param name="schema">The schema to test with.</param>
            <returns>
            	<c>true</c> if the specified <see cref="T:Newtonsoft.Json.Linq.JToken"/> is valid; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Schema.Extensions.IsValid(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema,System.Collections.Generic.IList{System.String}@)">
            <summary>
            <para>
            Determines whether the <see cref="T:Newtonsoft.Json.Linq.JToken"/> is valid.
            </para>
            <note type="caution">
            JSON Schema validation has been moved to its own package. See <see href="http://www.newtonsoft.com/jsonschema">http://www.newtonsoft.com/jsonschema</see> for more details.
            </note>
            </summary>
            <param name="source">The source <see cref="T:Newtonsoft.Json.Linq.JToken"/> to test.</param>
            <param name="schema">The schema to test with.</param>
            <param name="errorMessages">When this method returns, contains any error messages generated while validating. </param>
            <returns>
            	<c>true</c> if the specified <see cref="T:Newtonsoft.Json.Linq.JToken"/> is valid; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Schema.Extensions.Validate(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema)">
            <summary>
            <para>
            Validates the specified <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </para>
            <note type="caution">
            JSON Schema validation has been moved to its own package. See <see href="http://www.newtonsoft.com/jsonschema">http://www.newtonsoft.com/jsonschema</see> for more details.
            </note>
            </summary>
            <param name="source">The source <see cref="T:Newtonsoft.Json.Linq.JToken"/> to test.</param>
            <param name="schema">The schema to test with.</param>
        </member>
        <member name="M:Newtonsoft.Json.Schema.Extensions.Validate(Newtonsoft.Json.Linq.JToken,Newtonsoft.Json.Schema.JsonSchema,Newtonsoft.Json.Schema.ValidationEventHandler)">
            <summary>
            <para>
            Validates the specified <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </para>
            <note type="caution">
            JSON Schema validation has been moved to its own package. See <see href="http://www.newtonsoft.com/jsonschema">http://www.newtonsoft.com/jsonschema</see> for more details.
            </note>
            </summary>
            <param name="source">The so