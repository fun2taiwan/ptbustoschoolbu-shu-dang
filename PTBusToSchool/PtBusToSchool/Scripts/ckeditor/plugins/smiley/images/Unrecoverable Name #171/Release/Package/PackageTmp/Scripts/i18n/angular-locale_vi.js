 <returns>An object representing the navigation property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Collection``1(System.Linq.Expressions.Expression{System.Func{`0,System.Collections.Generic.ICollection{``0}}})">
            <summary>
                Gets an object that represents the collection navigation property from this
                entity to a collection of related entities.
            </summary>
            <typeparam name = "TElement">The type of elements in the collection.</typeparam>
            <param name = "navigationProperty">An expression representing the navigation property.</param>
            <returns>An object representing the navigation property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Property(System.String)">
            <summary>
                Gets an object that represents a scalar or complex property of this entity.
            </summary>
            <param name = "propertyName">The name of the property.</param>
            <returns>An object representing the property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Property``1(System.String)">
            <summary>
                Gets an object that represents a scalar or complex property of this entity.
            </summary>
            <typeparam name = "TProperty">The type of the property.</typeparam>
            <param name = "propertyName">The name of the property.</param>
            <returns>An object representing the property.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry`1.Property``1(System.Linq.Expressions.Expression{System.Func{`0,``0}})">
            <summary>
                Gets an object that represents a scalar or complex property of this entity.
            </summary>
            <typeparam name = "TProperty">The type of the property.</typeparam>
            <param name = "navigationProperty">An expression representing the property.</param>
            <returns>An object representing the property.</returns>
   