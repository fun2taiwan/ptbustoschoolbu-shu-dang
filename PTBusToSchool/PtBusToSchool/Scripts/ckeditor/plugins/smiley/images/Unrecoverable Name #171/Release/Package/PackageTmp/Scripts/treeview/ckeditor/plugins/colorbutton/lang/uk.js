           <summary>
            For mocking.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Internal.ClonedObjectContext.#ctor(System.Data.Entity.Internal.MockingProxies.ObjectContextProxy,System.String,System.Boolean)">
            <summary>
            Creates a clone of the given <see cref="P:System.Data.Entity.Internal.ClonedObjectContext.ObjectContext"/>. The underlying <see cref="T:System.Data.Common.DbConnection"/> of
            the context is also cloned and the given connection string is used for the connection string of
            the cloned connection.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Internal.ClonedObjectContext.TransferLoadedAssemblies(System.Data.Entity.Internal.MockingProxies.ObjectContextProxy)">
            <summary>
                Finds the assemblies that were used for loading o-space types in the source context
                and loads those assemblies in the cloned context.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Internal.ClonedObjectContext.Dispose">
            <summary>
            Disposes both the underlying ObjectContext and its store connection.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Internal.ClonedObjectContext.ObjectContext">
            <summary>
            The cloned context.
            </su