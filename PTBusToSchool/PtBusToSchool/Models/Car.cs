﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class Car :BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "車牌編號")]
        public string Subject { get; set; }

        //ForeignKey
        [Display(Name = "駕駛")]
        public int? DriverId { get; set; }

        [JsonIgnore]
        [ForeignKey("DriverId")]
        public virtual Driver CarAndDriver { get; set; }

        [Display(Name = "保管人")]
        public string Custodian { get; set; }

        [Display(Name = "座位數")]
        public int? SeatAmount { get; set; }

        [Display(Name = "立位數")]
        public int? StandAmount { get; set; }

        [Display(Name = "車況")]
        public CarState CarStatus { get; set; }

      [Display(Name = "車種")]
        public CarStyle CarStyles { get; set; }
    }
}