e parameters to apply to the command string.</param>
            <returns>The result returned by the database after executing the command.</returns>
        </member>
        <member name="P:System.Data.Entity.Database.Connection">
            <summary>
                Returns the connection being used by this context.  This may cause the
                connection to be created if it does not already exist.
            </summary>
            <exception cref="T:System.InvalidOperationException">Thrown if the context has been disposed.</exception>
        </member>
        <member name="P:System.Data.Entity.Database.InitializerDelegate">
            <summary>
                Returns the <see cref="T:System.Data.Entity.IDatabaseInitializer`1"/> as a delegate that can be called with
                an instance of the <see cref="T:System.Data.Entity.DbContext"/> that owns this Database object, or returns null if
                there is no initializer set for this context type.
            </summary>
            <value>The initializer delegate or null.</value>
        </member>
        <member name="P:System.Data.Entity.Database.DefaultConnectionFactory">
            <summary>
                The connection factory to use when creating a <see cref="T:System.Data.Common.DbConnection"/> from just
                a database name or a connection string.
            </summary>
            <remarks>
                This is used when just a database name or connection string is given to <see cref="T:System.Data.Entity.DbContext"/> or when
                the no database name or connection is given to DbContext in which case the name of
                the context class is passed to this factory in order to generate a DbConnection.
                By default, the <see cref="T:System.Data.Entity.Infrastructure.IDbConnectionFactory"/> instance to use is read from the applications .config
                file from the "EntityFramework DefaultConnectionFactory" entry in appSettings. If no entry is found in
                the config file then <see cref="T:System.Data.Entity.Infrastructure.SqlConnectionFactory"/> is used. Setting this property in code
                always overrides whatever value is found in the config file.
            </remarks>
        </member>
        <member name="P:System.Data.Entity.Database.DefaultConnectionFactoryChanged">
            <summary>
                Checks wether or not the DefaultConnectionFactory has been set to something other than its default value.
            </summary>
        </member>
        <!-- Badly formed XML comment ignored for member "M:System.Data.Entity.DbExtensions.Include``1(System.Linq.IQueryable{``0},System.String)" -->
        <!-- Badly formed XML comment ignored for member "M:System.Data.Entity.DbExtensions.Include(System.Linq.IQueryable,System.String)" -->
        <member name="M:System.Data.Entity.DbExtensions.CommonInclude``1(``0,System.String)">
            <summary>
                Common code for generic and non-generic string Include.
            </summary>
        </member>
        <!-- Badly formed XML comment ignored for member "M:System.Data.Entity.DbExtensions.Include``2(System.Linq.IQueryable{``0},System.Linq.Expressions.Expression{System.Func{``0,``1}})" -->
        <member name="M:System.Data.Entity.DbExtensions.AsNoTracking``1(System.Linq.IQueryable{``0})">
            <summary>
                Returns a new query where the entities returned will not be cached in the <see cref="T:System.Data.Entity.DbContext"/>
                or <see cref="T:System.Data.Objects.ObjectContext"/>.  This method works by calling the AsNoTracking method of the
                underlying query object.  If the underlying query object does not have a AsNoTracking method,
                then calling this method will have no affect.
            </summary>
            <typeparam name="T">The element type