     </summary>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1.CallMethodNoResult(System.String,System.Dynamic.DynamicMetaObjectBinder,System.Linq.Expressions.Expression[],Newtonsoft.Json.Utilities.DynamicProxyMetaObject{`0}.Fallback)">
            <summary>
            Helper method for generating a MetaObject which calls a
            specific method on Dynamic, but uses one of the arguments for
            the result.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.DynamicProxyMetaObject`1.GetRestrictions">
            <summary>
            Returns a Restrictions object which includes our current restrictions merged
            with a restriction limiting our type
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.EnumUtils.GetNamesAndValues``1">
            <summary>
            Gets a dictionary of the names and values of an Enum type.
            </summary>
            <returns></returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.EnumUtils.GetNamesAndValues``1(System.Type)">
            <summary>
            Gets a dictionary of the names and values of an Enum type.
            </summary>
            <param name="enumType">The enum type to get names and values for.</param>
            <returns></returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.GetCollectionItemType(System.Type)">
            <summary>
            Gets the type of the typed collection's items.
            </summary>
            <param name="type">The type.</param>
            <returns>The type of the typed collection's items.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.GetMemberUnderlyingType(System.Reflection.MemberInfo)">
            <summary>
            Gets the member's underlying type.
            </summary>
            <param name="member">The member.</param>
            <returns>The underlying type of the member.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.IsIndexedProperty(System.Reflection.MemberInfo)">
            <summary>
            Determines whether the member is an indexed property.
            </summary>
            <param name="member">The member.</param>
            <returns>
            	<c>true</c> if the member is an indexed property; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.IsIndexedProperty(System.Reflection.PropertyInfo)">
            <summary>
            Determines whether the property is an indexed property.
            </summary>
            <param name="property">The property.</param>
            <returns>
            	<c>true</c> if the property is an indexed property; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.GetMemberValue(System.Reflection.MemberInfo,System.Object)">
            <summary>
            Gets the member's value on the object.
            </summary>
            <param name="member">The member.</param>
            <param name="target">The target object.</param>
            <returns>The member's value on the object.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.SetMemberValue(System.Reflection.MemberInfo,System.Object,System.Object)">
            <summary>
            Sets the member's value on the target object.
            </summary>
            <param name="member">The member.</param>
            <param name="target">The target.</param>
            <param name="value">The value.</param>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.CanReadMemberValue(System.Reflection.MemberInfo,System.Boolean)">
            <summary>
            Determines whether the specified MemberInfo can be read.
            </summary>
            <param name="member">The MemberInfo to determine whether can be read.</param>
            /// <param name="nonPublic">if set to <c>true</c> then allow the member to be gotten non-publicly.</param>
            <returns>
            	<c>true</c> if the specified MemberInfo can be read; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.ReflectionUtils.CanSetMemberValue(System.Reflection.MemberInfo,System.Boolean,System.Boolean)">
            <summary>
            Determines whether the specified MemberInfo can be set.
            </summary>
            <param name="member">The MemberInfo to determine whether can be set.</param>
            <param name="nonPublic">if set to <c>true</c> then allow the member to be set non-publicly.</param>
            <param name="canSetReadOnly">if set to <c>true</c> then allow the member to be set if read-only.</param>
            <returns>
            	<c>true</c> if the specified MemberInfo can be set; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="T:Newtonsoft.Json.Utilities.StringBuffer">
            <summary>
            Builds a string. Unlike StringBuilder this class lets you reuse it's internal buffer.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.StringUtils.IsWhiteSpace(System.String)">
            <summary>
            Determines whether the string is all white space. Empty string will return false.
            </summary>
            <param name="s">The string to test whether it is all white space.</param>
            <returns>
            	<c>true</c> if the string is all white space; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.StringUtils.NullEmptyString(System.String)">
            <summary>
            Nulls an empty string.
            </summary>
            <param name="s">The string.</para