he database.
            </summary>
            <returns>Ids of pending migrations.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.ToolingFacade.Update(System.String,System.Boolean)">
            <summary>
                Updates the database to the specified migration.
            </summary>
            <param name = "targetMigration">
                The Id of the migration to migrate to.
                If null is supplied, the database will be updated to the latest migration.
            </param>
            <param name = "force">Value indicating if data loss during automatic migration is acceptable.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.ToolingFacade.ScriptUpdate(System.String,System.String,System.Boolean)">
            <summary>
                Generates a SQL script to migr