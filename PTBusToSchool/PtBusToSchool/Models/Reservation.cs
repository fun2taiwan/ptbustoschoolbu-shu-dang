﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class Reservation:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Display(Name = "學號")]
        public string StudentId { get; set; }

       [Display(Name = "學生姓名")]
        public string Subject { get; set; }

       [Display(Name = "預約的路線")]
       public string ReservationCarLine { get; set; }

       [Display(Name = "起點")]
       public string Startstation { get; set; }

       [Display(Name = "迄點")]
       public string Endstation { get; set; }

       [Display(Name = "預約的時間")]
       public string ReservationCarTime { get; set; }

       [Display(Name = "預約的班次")]
       public string ReservationCarNo { get; set; }

       [Display(Name = "預約結果")]
       public ReservationState ReservationSorF { get; set; }

    }
}