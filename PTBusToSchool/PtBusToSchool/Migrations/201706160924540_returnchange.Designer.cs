// <auto-generated />
namespace PtBusToSchool.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class returnchange : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(returnchange));
        
        string IMigrationMetadata.Id
        {
            get { return "201706160924540_returnchange"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
