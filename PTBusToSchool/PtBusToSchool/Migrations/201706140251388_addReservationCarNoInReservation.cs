namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addReservationCarNoInReservation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "ReservationCarNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reservations", "ReservationCarNo");
        }
    }
}
