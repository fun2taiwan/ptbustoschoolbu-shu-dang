ef="T:Newtonsoft.Json.Linq.IJEnumerable`1"/> with the specified key.
            </summary>
            <value></value>
        </member>
        <member name="T:Newtonsoft.Json.Linq.JArray">
            <summary>
            Represents a JSON array.
            </summary>
            <example>
              <code lang="cs" source="..\Src\Newtonsoft.Json.Tests\Documentation\LinqToJsonTests.cs" region="LinqToJsonCreateParseArray" title="Parsing a JSON Array from Text" />
            </example>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JArray.ChildrenTokens">
            <summary>
            Gets the container's children tokens.
            </summary>
            <value>The container's children tokens.</value>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JArray.Type">
            <summary>
            Gets the node type for this <see cref="T:Newtonsoft.Json.Linq.JToken"/>.
            </summary>
            <value>The type.</value>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.#ctor">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Linq.JArray"/> class.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.#ctor(Newtonsoft.Json.Linq.JArray)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Linq.JArray"/> class from another <see cref="T:Newtonsoft.Json.Linq.JArray"/> object.
            </summary>
            <param name="other">A <see cref="T:Newtonsoft.Json.Linq.JArray"/> object to copy from.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.#ctor(System.Object[])">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Linq.JArray"/> class with the specified content.
            </summary>
            <param name="content">The contents of the array.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.#ctor(System.Object)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Linq.JArray"/> class with the specified content.
            </summary>
            <param name="content">The contents of the array.</param>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Load(Newtonsoft.Json.JsonReader)">
            <summary>
            Loads an <see cref="T:Newtonsoft.Json.Linq.JArray"/> from a <see cref="T:Newtonsoft.Json.JsonReader"/>. 
            </summary>
            <param name="reader">A <see cref="T:Newtonsoft.Json.JsonReader"/> that will be read for the content of the <see cref="T:Newtonsoft.Json.Linq.JArray"/>.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JArray"/> that contains the JSON that was read from the specified <see cref="T:Newtonsoft.Json.JsonReader"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Load(Newtonsoft.Json.JsonReader,Newtonsoft.Json.Linq.JsonLoadSettings)">
            <summary>
            Loads an <see cref="T:Newtonsoft.Json.Linq.JArray"/> from a <see cref="T:Newtonsoft.Json.JsonReader"/>. 
            </summary>
            <param name="reader">A <see cref="T:Newtonsoft.Json.JsonReader"/> that will be read for the content of the <see cref="T:Newtonsoft.Json.Linq.JArray"/>.</param>
            <param name="settings">The <see cref="T:Newtonsoft.Json.Linq.JsonLoadSettings"/> used to load the JSON.
            If this is null, default load settings will be used.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JArray"/> that contains the JSON that was read from the specified <see cref="T:Newtonsoft.Json.JsonReader"/>.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Parse(System.String)">
            <summary>
            Load a <see cref="T:Newtonsoft.Json.Linq.JArray"/> from a string that contains JSON.
            </summary>
            <param name="json">A <see cref="T:System.String"/> that contains JSON.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JArray"/> populated from the string that contains JSON.</returns>
            <example>
              <code lang="cs" source="..\Src\Newtonsoft.Json.Tests\Documentation\LinqToJsonTests.cs" region="LinqToJsonCreateParseArray" title="Parsing a JSON Array from Text" />
            </example>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Parse(System.String,Newtonsoft.Json.Linq.JsonLoadSettings)">
            <summary>
            Load a <see cref="T:Newtonsoft.Json.Linq.JArray"/> from a string that contains JSON.
            </summary>
            <param name="json">A <see cref="T:System.String"/> that contains JSON.</param>
            <param name="settings">The <see cref="T:Newtonsoft.Json.Linq.JsonLoadSettings"/> used to load the JSON.
            If this is null, default load settings will be used.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JArray"/> populated from the string that contains JSON.</returns>
            <example>
              <code lang="cs" source="..\Src\Newtonsoft.Json.Tests\Documentation\LinqToJsonTests.cs" region="LinqToJsonCreateParseArray" title="Parsing a JSON Array from Text" />
            </example>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.FromObject(System.Object)">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Linq.JArray"/> from an object.
            </summary>
            <param name="o">The object that will be used to create <see cref="T:Newtonsoft.Json.Linq.JArray"/>.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JArray"/> with the values of the specified object</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.FromObject(System.Object,Newtonsoft.Json.JsonSerializer)">
            <summary>
            Creates a <see cref="T:Newtonsoft.Json.Linq.JArray"/> from an object.
            </summary>
            <param name="o">The object that will be used to create <see cref="T:Newtonsoft.Json.Linq.JArray"/>.</param>
            <param name="jsonSerializer">The <see cref="T:Newtonsoft.Json.JsonSerializer"/> that will be used to read the object.</param>
            <returns>A <see cref="T:Newtonsoft.Json.Linq.JArray"/> with the values of the specified object</returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.WriteTo(Newtonsoft.Json.JsonWriter,Newtonsoft.Json.JsonConverter[])">
            <summary>
            Writes this token to a <see cref="T:Newtonsoft.Json.JsonWriter"/>.
            </summary>
            <param name="writer">A <see cref="T:Newtonsoft.Json.JsonWriter"/> into which this method will write.</param>
            <param name="converters">A collection of <see cref="T:Newtonsoft.Json.JsonConverter"/> which will be used when writing the token.</param>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JArray.Item(System.Object)">
            <summary>
            Gets the <see cref="T:Newtonsoft.Json.Linq.JToken"/> with the specified key.
            </summary>
            <value>The <see cref="T:Newtonsoft.Json.Linq.JToken"/> with the specified key.</value>
        </member>
        <member name="P:Newtonsoft.Json.Linq.JArray.Item(System.Int32)">
            <summary>
            Gets or sets the <see cref="T:Newtonsoft.Json.Linq.JToken"/> at the specified index.
            </summary>
            <value></value>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.IndexOf(Newtonsoft.Json.Linq.JToken)">
            <summary>
            Determines the index of a specific item in the <see cref="T:System.Collections.Generic.IList`1"/>.
            </summary>
            <param name="item">The object to locate in the <see cref="T:System.Collections.Generic.IList`1"/>.</param>
            <returns>
            The index of <paramref name="item"/> if found in the list; otherwise, -1.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Insert(System.Int32,Newtonsoft.Json.Linq.JToken)">
            <summary>
            Inserts an item to the <see cref="T:System.Collections.Generic.IList`1"/> at the specified index.
            </summary>
            <param name="index">The zero-based index at which <paramref name="item"/> should be inserted.</param>
            <param name="item">The object to insert into the <see cref="T:System.Collections.Generic.IList`1"/>.</param>
            <exception cref="T:System.ArgumentOutOfRangeException">
            	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"/>.</exception>
            <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"/> is read-only.</exception>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.RemoveAt(System.Int32)">
            <summary>
            Removes the <see cref="T:System.Collections.Generic.IList`1"/> item at the specified index.
            </summary>
            <param name="index">The zero-based index of the item to remove.</param>
            <exception cref="T:System.ArgumentOutOfRangeException">
            	<paramref name="index"/> is not a valid index in the <see cref="T:System.Collections.Generic.IList`1"/>.</exception>
            <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.IList`1"/> is read-only.</exception>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.GetEnumerator">
            <summary>
            Returns an enumerator that iterates through the collection.
            </summary>
            <returns>
            A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Add(Newtonsoft.Json.Linq.JToken)">
            <summary>
            Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1"/>.
            </summary>
            <param name="item">The object to add to the <see cref="T:System.Collections.Generic.ICollection`1"/>.</param>
            <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only.</exception>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Clear">
            <summary>
            Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1"/>.
            </summary>
            <exception cref="T:System.NotSupportedException">The <see cref="T:System.Collections.Generic.ICollection`1"/> is read-only. </exception>
        </member>
        <member name="M:Newtonsoft.Json.Linq.JArray.Contains(Newtonsoft.Json.Linq.JTok