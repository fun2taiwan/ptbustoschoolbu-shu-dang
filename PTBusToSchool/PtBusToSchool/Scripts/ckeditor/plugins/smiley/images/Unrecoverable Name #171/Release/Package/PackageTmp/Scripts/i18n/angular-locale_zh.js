  </remarks>
            <param name="contextType">The type deriving from <see cref="T:System.Data.Entity.DbContext"/>.</param>
            <param name="connectionStringSettings">A collection of connection strings.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbContextInfo.#ctor(System.Type,System.Configuration.Configuration)">
            <summary>
                Creates a new instance representing a given <see cref="T:System.Data.Entity.DbContext"/> type. An external config 
                object (e.g. app.config or web.config) can be supplied and will be used during connection string
                resolution. This includes looking for connection strings and DefaultConnectionFactory entries.
            </summary>
            <param name="contextType">The type deriving from <see cref="T:System.Data.Entity.DbContext"/>.</param>
            <param name="config">An object representing the config file.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbContextInfo.#ctor(System.Type,System.Configuration.Configuration,System.Data.Entity.Infrastructure.DbConnectionInfo)">
            <summary>
                Creates a new instance representing a given <see cref="T:System.Data.Entity.DbContext"/>, targeting a specific database.
                An external config object (e.g. app.config or web.config) can be supplied and will be used during connection string
                resolution. This includes looking for connection strings and DefaultConnectionFactory entries.
            </summary>
            <param name="contextType">The type deriving from <see cref="T:System.Data.Entity.DbContext"/>.</param>
            <param name="config">An object representing the config file.</param>
            <param name="connectionInfo">Connection information for the database to be used.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbContextInfo.#ctor(System.Type,System.Data.Entity.Infrastructure.DbProviderInfo)">
            <summary>
                Cre