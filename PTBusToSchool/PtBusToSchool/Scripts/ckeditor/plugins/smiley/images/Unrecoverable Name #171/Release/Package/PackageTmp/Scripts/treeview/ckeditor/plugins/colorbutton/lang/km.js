ault value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.String)">
            <summary>
                Generates code to specify the default value for a <see cref="T:System.String"/> column.
            </summary>
            <param name="defaultValue">The value to be used as the default.</param>
            <returns>Code representing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.TimeSpan)">
            <summary>
                Generates code to specify the default value for a <see cref="T:System.TimeSpan"/> column.
            </summary>
            <param name="defaultValue">The value to be used as the default.</param>
            <returns>Code representing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Spatial.DbGeography)">
            <summary>
                Generates code to specify the default value for a <see cref="T:System.Data.Spatial.DbGeography"/> column.
            </summary>
            <param name="defaultValue">The value to be used