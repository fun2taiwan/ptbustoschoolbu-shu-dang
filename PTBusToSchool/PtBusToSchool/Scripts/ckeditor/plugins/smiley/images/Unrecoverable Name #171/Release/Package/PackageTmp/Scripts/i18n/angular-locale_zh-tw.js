ructure.DbContextInfo.#ctor(System.Data.Entity.DbContext)">
            <summary>
            Called internally when a context info is needed for an existing context, which may not be constructable.
            </summary>
            <param name="context">The context instance to get info from.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbContextInfo.CreateInstance">
            <summary>
                If instances of the underlying <see cref="T:System.Data.Entity.DbContext"/> type can be created, returns
                a new instance; otherwise returns null.
            </summary>
            <returns>A <see cref="T:System.Data.Entity.DbContext"/> instance.</returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextInfo.ContextType">
            <summary>
                The concrete <see cref="T:System.Data.Entity.DbContext"/> type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextInfo.IsConstructible">
            <summary>
                Whether or not instances of the underlying <see cref="T:System.Data.Entity.DbContext"/> type can be created.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextInfo.ConnectionString">
            <summary>
                The connection string used by the underlying <see cref="T:System.Data.Entity.DbContext"/> type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextInfo.ConnectionStringName">
            <summary>
                The connection string name used by the underlying <see cref="T:System.Data.Entity.DbContext"/> type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbContextInfo.ConnectionProviderName">
            <summary>
                The ADO.NET provider name of the connection used by the underlying <see cref="T:System.Data.Entity.DbContext"/> type.
            </summary>
        