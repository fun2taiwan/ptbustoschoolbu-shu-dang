 multi-point type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeometricMultiLinestring">
            <summary>
                Geometric multi-linestring type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeometricMultiPolygon">
            <summary>
                Geometric multi-polygon type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeometryCollection">
            <summary>
                Geometric collection type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographicPoint">
            <summary>
                Geographic point type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographicLinestring">
            <summary>
                Geographic linestring type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographicPolygon">
            <summary>
                Geographic polygon type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographicMultiPoint">
            <summary>
                Geographic multi-point type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographicMultiLinestring">
            <summary>
                Geographic multi-linestring type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographicMultiPolygon">
            <summary>
                Geographic multi-polygon type kind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.EdmPrimitiveTypeKind.GeographyCollection">
            <summary>
                Geographic collection type kind
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.EdmProperty">
            <summary>
                Allows the construction and modification of a primitive- or complex-valued property of an Entity Data Model (EDM) entity or complex type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmProperty.CollectionKind">
            <summary>
                Gets or sets an <see cref="T:System.Data.Entity.Edm.EdmCollectionKind"/> value that indicates which collection semantics - if any - apply to the property.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmProperty.ConcurrencyMode">
            <summary>
                Gets or sets a <see cref="T:System.Data.Entity.Edm.EdmConcurrencyMode"/> value that indicates whether the property is used for concurrency validation.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.E