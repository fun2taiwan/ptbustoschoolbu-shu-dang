﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class CarSiteController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var carsite = _db.CarSite.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                carsite = carsite.Where(w => w.Subject.Contains(searchBySubject)); 
            }
            if (hasViewData("SearchByStartDate") && !hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate")).AddDays(1);        
                    carsite = carsite.Where(w => w.InitDate >= startDate && w.InitDate <= endDate); 
            }
            if (!hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);
               
                    carsite = carsite.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);       
            }
            if (hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);
                carsite = carsite.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);         
            }

//            ViewBag.Subject = Subject;
            return View(carsite.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /admin/CarSite/Details/5

        public ActionResult Details(int id = 0)
        {
            CarSite carsite = _db.CarSite.Find(id);
            if (carsite == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(carsite);
        }

        //
        // GET: /admin/CarSite/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /admin/CarSite/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(CarSite carsite )
        {
            if (ModelState.IsValid)
            {

                _db.CarSite.Add(carsite);
                carsite.Create(_db,_db.CarSite);
                return RedirectToAction("Index");
            }

            return View(carsite);
        }

        //
        // GET: /admin/CarSite/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CarSite carsite = _db.CarSite.Find(id);
            if (carsite == null)
            {
                return HttpNotFound();
            }
            return View(carsite);
        }

        //
        // POST: /admin/CarSite/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(CarSite carsite)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(carsite).State = EntityState.Modified;
                carsite.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(carsite);
        }

        //
        // GET: /admin/CarSite/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CarSite carsite = _db.CarSite.Find(id);
            if (carsite == null)
            {
                return HttpNotFound();
            }
            return View(carsite);
        }

        //
        // POST: /admin/CarSite/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarSite carsite = _db.CarSite.Find(id);
            _db.CarSite.Remove(carsite);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
