﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class CarController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var car = _db.Car.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                car = car.Where(w => w.Subject.Contains(searchBySubject)); 
            } 
 
            if (hasViewData("SearchByCarStatus")) 
            { 
            string CarStatus = getViewDateStr("SearchByCarStatus");             
             PtBusToSchool.Models.CarState searchByCarStatus= (PtBusToSchool.Models.CarState)Enum.Parse(typeof(PtBusToSchool.Models.CarState), CarStatus, false); 
             
                car = car.Where(w => w.CarStatus == searchByCarStatus); 
            }
            if (hasViewData("SearchByStartDate") && !hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate")).AddDays(1);
                car = car.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);
            }
            if (!hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);

                car = car.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);
            }
            if (hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);
                car = car.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);
            }

//            ViewBag.Subject = Subject;
            return View(car.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /admin/Car/Details/5

        public ActionResult Details(int id = 0)
        {
            Car car = _db.Car.Find(id);
            if (car == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(car);
        }

        //
        // GET: /admin/Car/Create

        public ActionResult Create()
        {
            ViewBag.DriverId = new SelectList(_db.Drivers.OrderBy(p => p.InitDate), "Id", "Subject");
            ViewBag.Custodian = ViewBag.DriverId;
            return View();
        }

        //
        // POST: /admin/Car/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Car car )
        {
            int custodianid = Convert.ToInt16(car.Custodian);
            var custodianname = _db.Drivers.Find(custodianid);

            car.Custodian = custodianname.Subject;
            if (ModelState.IsValid)
            {

                _db.Car.Add(car);
    
                car.Create(_db,_db.Car);
                return RedirectToAction("Index");
            }

            return View(car);
        }

        //
        // GET: /admin/Car/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Car car = _db.Car.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CarLineId = new SelectList(_db.CarLine.Where(x => x.ParentId == null).OrderBy(p => p.InitDate), "Id", "Subject",car.CarLineId);
            ViewBag.DriverId = new SelectList(_db.Drivers.OrderBy(p => p.InitDate), "Id", "Subject",car.DriverId);
            ViewBag.Custodian = new SelectList(_db.Drivers.OrderBy(p => p.InitDate), "Id", "Subject", car.Custodian);
            return View(car);
        }

        //
        // POST: /admin/Car/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Car car)
        {
            //ViewBag.CarLineId = new SelectList(_db.CarLine.Where(x => x.ParentId == null).OrderBy(p => p.InitDate), "Id", "Subject", car.CarLineId);
            //if (car.Passengers == null)
            //{
            //    ViewBag.message = "編輯失敗，因有欄位輸入錯誤，請修正";
            //    return View(car);
            //}
            if (ModelState.IsValid)
            {

               //_db.Entry(car).State = EntityState.Modified;
                car.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(car);
        }

        //
        // GET: /admin/Car/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Car car = _db.Car.Find(id);
            if (car == null)
            {
                return HttpNotFound();
            }
            return View(car);
        }

        //
        // POST: /admin/Car/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Car car = _db.Car.Find(id);
            _db.Car.Remove(car);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
