ers.ReflectedHttpActionDescriptor.SupportedHttpMethods">
      <summary>Gets or sets the supported http methods.</summary>
      <returns>The supported http methods.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.#ctor(System.Web.Http.Controllers.HttpActionDescriptor,System.Reflection.ParameterInfo)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor" /> class.</summary>
      <param name="actionDescriptor">The action descriptor.</param>
      <param name="parameterInfo">The parameter information.</param>
    </member>
    <member name="P:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.DefaultValue">
      <summary>Gets the default value for the parameter.</summary>
      <returns>The default value for the parameter.</returns>
    </member>
    <member name="M:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.GetCustomAttributes``1">
      <summary>Retrieves a collection of the custom attributes from the parameter.</summary>
      <returns>A collection of the custom attributes from the parameter.</returns>
      <typeparam name="T">The type of the custom attributes.</typeparam>
    </member>
    <member name="P:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.IsOptional">
      <summary>Gets a value that indicates whether the parameter is optional.</summary>
      <returns>true if the parameter is optional; otherwise false.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.ParameterInfo">
      <summary>Gets or sets the parameter information.</summary>
      <returns>The parameter information.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.ParameterName">
      <summary>Gets the name of the parameter.</summary>
      <returns>The name of the parameter.</returns>
    </member>
    <member name="P:System.Web.Http.Controllers.ReflectedHttpParameterDescriptor.ParameterType">
      <summary>Gets the type of the parameter.</summary>
      <returns>The type of the parameter.</returns>
    </member>
    <member name="T:System.Web.Http.Controllers.ResponseMessageResultConverter">
      <summary>Represents a converter for actions with a return type of <see cref="T:System.Net.Http.HttpResponseMessage" />. </summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ResponseMessageResultConverter.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ResponseMessageResultConverter" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ResponseMessageResultConverter.Convert(System.Web.Http.Controllers.HttpControllerContext,System.Object)">
      <summary>Converts a <see cref="T:System.Web.Http.Controllers.ResponseMessageResultConverter" /> object to another object.</summary>
      <returns>The converted object.</returns>
      <param name="controllerContext">The controller context.</param>
      <param name="actionResult">The action result.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.ServicesContainer">
      <summary>An abstract class that provides a container for services used by ASP.NET Web API.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ServicesContainer" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.Add(System.Type,System.Object)">
      <summary> Adds a service to the end of services list for the given service type.  </summary>
      <param name="serviceType">The service type.</param>
      <param name="service">The service instance.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.AddRange(System.Type,System.Collections.Generic.IEnumerable{System.Object})">
      <summary> Adds the services of the specified collection to the end of the services list for the given service type. </summary>
      <param name="serviceType">The service type.</param>
      <param name="services">The services to add.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.Clear(System.Type)">
      <summary> Removes all the service instances of the given service type.  </summary>
      <param name="serviceType">The service type to clear from the services list.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.ClearMultiple(System.Type)">
      <summary>Removes all instances of a multi-instance service type.</summary>
      <param name="serviceType">The service type to remove.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ServicesContainer.ClearSingle(System.Type)">
      <summary>Removes a s