ttribute.</param>
            <returns>A collection of <see cref="T:System.Attribute"/>s, or an empty collection.</returns>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.ReflectionAttributeProvider.GetAttributes(System.Type,System.Boolean)">
            <summary>
            Returns a collection of attributes, identified by type, or an empty collection if there are no attributes.
            </summary>
            <param name="attributeType">The type of the attributes.</param>
            <param name="inherit">When true, look up the hierarchy chain for the inherited custom attribute.</param>
            <returns>A collection of <see cref="T:System.Attribute"/>s, or an empty collection.</returns>
        </member>
        <member name="T:Newtonsoft.Json.Serialization.ReflectionValueProvider">
            <summary>
            Get and set values for a <see cref="T:System.Reflection.MemberInfo"/> using reflection.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.ReflectionValueProvider.#ctor(System.Reflection.MemberInfo)">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Serialization.ReflectionValueProvider"/> class.
            </summary>
            <param name="memberInfo">The member info.</param>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.ReflectionValueProvider.SetValue(System.Object,System.Object)">
            <summary>
            Sets the value.
            </summary>
            <param name="target">The target to set the value on.</param>
            <param name="value">The value to set on the target.</param>
        </member>
        <member name="M:Newtonsoft.Json.Serialization.ReflectionValueProvider.GetValue(System.Object)">
            <summary>
            Gets the value.
            </summary>
            <param name="target">The target to get the value from.</param>
            <returns>The value.</returns>
        </member>
        <member name="T:Newtonsoft.Json.StringEscapeHandling">
            <summary>
            Specifies how strings are escaped when writing JSON text.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.StringEscapeHandling.Default">
            <summary>
            Only control characters (e.g. newline) are escaped.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.StringEscapeHandling.EscapeNonAscii">
            <summary>
            All non-ASCII and control characters (e.g. newline) are escaped.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.StringEscapeHandling.EscapeHtml">
            <summary>
            HTML (&lt;, &gt;, &amp;, &apos;, &quot;) and control characters (e.g. newline) are escaped.
            </summary>
        </member>
        <member name="T:Newtonsoft.Json.TypeNameHandling">
            <summary>
            Specifies type name handling options for the <see cref="T:Newtonsoft.Json.JsonSerializer"/>.
            </summary>
            <remarks>
            <see cref="T:Newtonsoft.Json.TypeNameHandling"/> should be used with caution when your application deserializes JSON from an external source.
            Incoming types should be validated with a custom <see cref="T:System.Runtime.Serialization.SerializationBinder"/>
            when deserializing with a value other than <c>TypeNameHandling.None</c>.
            </remarks>
        </member>
        <member name="F:Newtonsoft.Json.TypeNameHandling.None">
            <summary>
            Do not include the .NET type name when serializing types.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.TypeNameHandling.Objects">
            <summary>
            Include the .NET type name when serializing into a JSON object structure.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.TypeNameHandling.Arrays">
            <summary>
            Include the .NET type name when serializing into a JSON array structure.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.TypeNameHandling.All">
            <summary>
            Always include the .NET type name when serializing.
            </summary>
        </member>
        <member name="F:Newtonsoft.Json.TypeNameHandling.Auto">
            <summary>
            Include the .NET type name when the type of the object being serialized is not the same as its declared type.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.CollectionUtils.IsNullOrEmpty``1(System.Collections.Generic.ICollection{``0})">
            <summary>
            Determines whether the collection is null or empty.
            </summary>
            <param name="collection">The collection.</param>
            <returns>
            	<c>true</c> if the collection is null or empty; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:Newtonsoft.Json.Utilities.CollectionUtils.AddRange``1(System.Collections.Generic.IList{``0},System.Collections.Generic.IEnumerable{``0})">
    