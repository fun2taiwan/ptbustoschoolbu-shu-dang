namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTimetableWithFileName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timetables", "FileName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Timetables", "FileName");
        }
    }
}
