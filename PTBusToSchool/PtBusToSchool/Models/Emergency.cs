﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class Emergency : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "事件名稱")]
        public string Subject { get; set; }

         [Required(ErrorMessage = "{0}必填")]
        [Display(Name = "事發日期")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? IncidentDate { get; set; }


         [Display(Name = "路線")]
         public string Line { get; set; }


         [Display(Name = "班次")]
         public string CarNo { get; set; }


         [Display(Name = "原因")]
         public string Momo { get; set; }
      

    }
}