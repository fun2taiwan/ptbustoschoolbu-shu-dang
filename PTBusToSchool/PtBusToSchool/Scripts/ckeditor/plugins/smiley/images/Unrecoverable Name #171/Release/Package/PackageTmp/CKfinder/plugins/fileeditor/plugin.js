ension">
      <summary> Gets the <see cref="T:System.Uri" /> path extension. </summary>
      <returns>The <see cref="T:System.Uri" /> path extension.</returns>
    </member>
    <member name="F:System.Net.Http.Formatting.UriPathExtensionMapping.UriPathExtensionKey">
      <summary>The <see cref="T:System.Uri" /> path extension key.</summary>
    </member>
    <member name="T:System.Web.Http.AcceptVerbsAttribute">
      <summary>Represents an attribute that specifies which HTTP methods an action method will respond to.</summary>
    </member>
    <member name="M:System.Web.Http.AcceptVerbsAttribute.#ctor(System.String[])">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.AcceptVerbsAttribute" /> class by using a list of HTTP methods that the action method will respond to.</summary>
      <param name="methods">The HTTP methods that the action method will respond to.</param>
    </member>
    <member name="P:System.Web.Http.AcceptVerbsAttribute.HttpMethods">
      <summary>Gets or sets the list of HTTP methods that the action method will respond to.</summary>
      <returns>Gets or sets the list of HTTP methods that the action method will respond to.</returns>
    </member>
    <member name="T:System.Web.Http.ActionNameAttribute">
      <summary>Represents an attribute that is used for the name of an action.</summary>
    </member>
    <member name="M:System.Web.Http.ActionNameAttribute.#ctor(System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ActionNameAttribute" /> class.</summary>
      <param name="name">The name of the action.</param>
    </member>
    <member name="P:System.Web.Http.ActionNameAttribute.Name">
      <summary>Gets or sets the name of the action.</summary>
      <returns>The name of the action.</returns>
    </member>
    <member name="T:System.Web.Http.AllowAnonymousAttribute">
      <summary>Specifies that actions and controllers are skipped by <see cref="T:System.Web.Http.AuthorizeAttribute" /> during authorization.</summary>
    </member>
    <member name="M:System.Web.Http.AllowAnonymousAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.AllowAnonymousAttribute" /> class.</summary>
    </member>
    <member name="T:System.Web.Http.ApiController">
      <summary>Defines properties and methods for API controller.</summary>
    </member>
    <member name="M:System.Web.Http.ApiController.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ApiController" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.ApiController.Configuration">
      <summary>Gets or sets the <see cref="T:System.Web.Http.HttpConfiguration" /> of the current <see cref="T:System.Web.Http.ApiController" />.</summary>
      <returns>The <see cref="T:System.Web.Http.HttpConfiguration" /> of the current <see cref="T:System.Web.Http.ApiController" />.</returns>
    </member>
    <member name="P:System.Web.Http.ApiController.ControllerContext">
      <summary>Gets the <see cref="T:System.Web.Http.Controllers.HttpControllerContext" /> of the current <see cref="T:System.Web.Http.ApiController" />.</summary>
      <returns>The <see cref="T:System.Web.Http.Controllers.HttpControllerContext" /> of the current <see cref="T:System.Web.Http.ApiController" />.</returns>
    </member>
    <member name="M:System.Web.Http.ApiController.Dispose">
      <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
    </member>
    <member name="M:System.Web.Http.ApiController.Dispose(System.Boolean)">
      <summary>Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.</summary>
      <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
    </member>
    <member name="M:System.Web.Http.ApiController.ExecuteAsync(System.Web.Http.Controllers.HttpControllerContext,System.Threading.CancellationToken)">
      <summary>Executes asynchronously a single HTTP operation.</summary>
      <returns>The newly started task.</returns>
      <param name="controllerContext">The controller context for a single HTTP operation.</param>
      <param name="cancellationToken">The cancellation token assigned for the HTTP operation.</param>
    </member>
    <member name="M:System.Web.Http.ApiController.Initialize(System.Web.Http.Controllers.HttpControllerContext)">
      <summary>Initializes the <see cref="T:System.Web.Http.ApiController" /> instance with the specified <paramref name="controllerContext" />.</summary>
      <param name="controllerContext">The <see cref="T:System.Web.Http.Controllers.HttpControllerContext" /> object that is used for the initialization.</param>
    </member>
    <member name="P:System.Web.Http.ApiController.ModelState">
      <summary>Gets the model state after the model binding process.</summary>
      <returns>The model state after the model binding process.</returns>
    </member>
    <member name="P:System.Web.Http.ApiController.Request">
      <summary>Gets or sets the <see cref="T:System.Net.Http.HttpRequestMessage" /> of the current <see cref="T:System.Web.Http.ApiController" />.</summary>
      <returns>The <see cref="T:System.Net.Http.HttpRequestMessage" /> of the current <see cref="T:System.Web.Http.ApiController" />.</returns>
    </member>
    <member name="P:System.Web.Http.ApiController.Url">
      <summary>Returns an instance of a <see cref="T:System.Web.Http.Routing.UrlHelper" />, which is used to generate URLs to other APIs.</summary>
      <returns>A <see cref="T:System.Web.Http.Routing.UrlHelper" /> object which is used to generate URLs to other APIs.</returns>
    </member>
    <member name="P:System.Web.Http.ApiController.User">
      <summary> Returns the current principal associated with this request. </summary>
      <returns>The current principal associated with this request.</returns>
    </member>
    <member name="T:System.Web.Http.AuthorizeAttribute">
      <summary>Specifies the authorization filter that verifies the request's <see cref="T:System.Security.Principal.IPrincipal" />.</summary>
    </member>
    <member name="M:System.Web.Http.AuthorizeAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.AuthorizeAttribute" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.AuthorizeAttribute.HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Processes requests that fail authorization.</summary>
      <param name="actionContext">The context.</param>
    </member>
    <member name="M:System.Web.Http.AuthorizeAttribute.IsAuthorized(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Indicates whether the specified control is authorized.</summary>
      <returns>true if the control is authorized; otherwise, false.</returns>
      <param name="actionContext">The context.</param>
    </member>
    <member name="M:System.Web.Http.AuthorizeAttribute.OnAuthorization(System.Web.Http.Controllers.HttpActionContext)">
      <summary>Calls when an action is being authorized.</summary>
      <param name="actionContext">The context.</param>
      <exception cref="T:System.ArgumentNullException">The context parameter is null.</exception>
    </member>
    <member name="P:System.Web.Http.AuthorizeAttribute.Roles">
      <summary>Gets or sets the authorized roles. </summary>
      <returns>The roles string. </returns>
    </member>
    <member name="P:System.Web.Http.AuthorizeAttribute.TypeId">
      <summary>Gets a unique identifier for this attribute.</summary>
      <returns>A unique identifier for this attribute.</returns>
    </member>
    <member name="P:System.Web.Http.AuthorizeAttribute.Users">
      <summary>Gets or sets the authorized users. </summary>
      <returns>The users string. </returns>
    </member>
    <member name="T:System.Web.Http.FromBodyAttribute">
      <summary> An attribute that specifies that an action parameter comes only from the entity body of the incoming <see cref="T:System.Net.Http.HttpRequestMessage" />. </summary>
    </member>
    <member name="M:System.Web.Http.FromBodyAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.FromBodyAttribute" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.FromBodyAttribute.GetBinding(System.Web.Http.Controllers.HttpParameterDescriptor)">
      <summary>Gets a parameter binding.</summary>
      <returns>The parameter binding.</returns>
      <param name="parameter">The parameter description.</param>
    </member>
    <member name="T:System.Web.Http.FromUriAttribute">
      <summary>An attribute that specifies that an action parameter comes from the URI of the incoming <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
    </member>
    <member name="M:System.Web.Http.FromUriAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.FromUriAttribute" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.FromUriAttribute.GetValueProviderFactories(System.Web.Http.HttpConfiguration)">
      <summary>Gets the value provider factories for the model binder.</summary>
      <returns>A collection of <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" /> objects.</returns>
      <param name="configuration">The configuration.</param>
    </member>
    <member name="T:System.Web.Http.HttpBindNeverAttribute">
      <summary>Represents attributes that specifies that HTTP binding should exclude a property.</summary>
    </me