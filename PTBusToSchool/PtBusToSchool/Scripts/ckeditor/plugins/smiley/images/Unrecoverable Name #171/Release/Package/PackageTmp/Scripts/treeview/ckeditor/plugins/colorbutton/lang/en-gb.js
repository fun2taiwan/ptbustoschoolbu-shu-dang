         The inverse cannot be automatically calculated, 
                if it was not supplied to the constructor this property will return null.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.DropTableOperation.IsDestructiveChange">
            <inheritdoc />
        </member>
        <member name="T:System.Data.Entity.Migrations.Model.InsertHistoryOperation">
            <summary>
                Represents inserting a new record into the migrations history table.
                The migrations history table is used to store a log of the migrations that have been applied to the database.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Model.InsertHistoryOperation.#ctor(System.String,System.String,System.Byte[],System.Object)">
            <summary>
                Initializes