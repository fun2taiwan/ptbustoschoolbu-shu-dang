namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addICollectionTimetableWithCarLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timetables", "CarLineId", c => c.Int());
            AddForeignKey("dbo.Timetables", "CarLineId", "dbo.CarLines", "Id");
            CreateIndex("dbo.Timetables", "CarLineId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Timetables", new[] { "CarLineId" });
            DropForeignKey("dbo.Timetables", "CarLineId", "dbo.CarLines");
            DropColumn("dbo.Timetables", "CarLineId");
        }
    }
}
