  there may be occasional sleeping.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.UtcNowGenerator.UtcNowAsMigrationIdTimestamp">
            <summary>
            Same as UtcNow method bur returns the time in the timestamp format used in migration IDs.
            </summary>
        </member>
        <member name="T:System.Data.Entity.ModelConfiguration.Conventions.ColumnOrderingConventionStrict">
            <summary>
                Convention to apply column ordering specified via <see cref="T:System.ComponentModel.DataAnnotations.ColumnAttribute"/> 
                or the <see cref="T:System.Data.Entity.DbModelBuilder"/> API. This convention throws if a duplicate configured column order
                is detected.
            </summary>
        </member>
        <member name="T:System.Data.Entity.ModelConfiguration.Conventions.ColumnOrderingConvention">
            <summary>
                Convention to apply column ordering specified via <see cref="T:System.ComponentModel.DataAnnotations.ColumnAttribute"/> 
                or the <see cref="T:System.Data.Entity.DbModelBuilder"/> API.
            </summary>