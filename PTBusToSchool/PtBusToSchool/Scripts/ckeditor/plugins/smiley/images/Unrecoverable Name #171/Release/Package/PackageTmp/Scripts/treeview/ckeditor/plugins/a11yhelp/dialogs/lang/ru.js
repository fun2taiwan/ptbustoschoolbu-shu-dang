uests.</summary>
    </member>
    <member name="M:System.Web.Http.HttpOptionsAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpOptionsAttribute" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.HttpOptionsAttribute.HttpMethods">
      <summary>Gets the collection of methods supported by HTTP OPTIONS requests.</summary>
      <returns>The collection of methods supported by HTTP OPTIONS requests.</returns>
    </member>
    <member name="T:System.Web.Http.HttpPatchAttribute">
      <summary>Represents a HTTP patch attribute.</summary>
    </member>
    <member name="M:System.Web.Http.HttpPatchAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpPatchAttribute" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.HttpPatchAttribute.HttpMethods">
      <summary>Gets a collection of HTTP methods.</summary>
      <returns>A collection of HTTP methods.</returns>
    </member>
    <member name="T:System.Web.Http.HttpPostAttribute">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.HttpPostAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpPostAttribute" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.HttpPostAttribute.HttpMethods">
      <summary>Gets a collection of HTTP methods.</summary>
      <returns>A collection of HTTP methods.</returns>
    </member>
    <member name="T:System.Web.Http.HttpPutAttribute">
      <summary>Represents an attribute that is used to restrict an HTTP method so that the method handles only HTTP PUT requests.</summary>
    </member>
    <member name="M:System.Web.Http.HttpPutAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpPutAttribute" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.HttpPutAttribute.HttpMethods">
      <summary>Gets the read-only collection of HTTP PUT methods.</summary>
      <returns>The read-only collection of HTTP PUT methods.</returns>
    </member>
    <member name="T:System.Web.Http.HttpResponseException">
      <summary> An exception that allows for a given <see cref="T:System.Net.Http.HttpResponseMessage" /> to be returned to the client. </summary>
    </member>
    <member name="M:System.Web.Http.HttpResponseException.#ctor(System.Net.Http.HttpResponseMessage)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpResponseException" /> class.</summary>
      <param name="response">The HTTP response to return to the client.</param>
    </member>
    <member name="M:System.Web.Http.HttpResponseException.#ctor(System.Net.HttpStatusCode)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.HttpResponseException" /> class. </summary>
      <param name="statusCode">The status code of the response.</param>
    </member>
    <member name="P:System.Web.Http.HttpResponseException.Response">
      <summary>Gets the HTTP response to return to the client.</summary>
      <returns>The <see cref="T:System.Net.Http.HttpResponseMessage" /> that represents the HTTP response.</returns>
    </member>
    <member name="T:System.Web.Http.HttpRouteCollection">
      <summary>A collection of <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instances.</summary>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpRouteCollection" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.#ctor(System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpRouteCollection" /> class.</summary>
      <param name="virtualPathRoot">The virtual path root.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.Add(System.String,System.Web.Http.Routing.IHttpRoute)">
      <summary>Adds an <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instance to the collection.</summary>
      <param name="name">The name of the route.</param>
      <param name="route">The <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instance to add to the collection.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.Clear">
      <summary>Removes all items from  the collection.</summary>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.Contains(System.Web.Http.Routing.IHttpRoute)">
      <summary>Determines whether the collection contains a specific <see cref="T:System.Web.Http.Routing.IHttpRoute" />.</summary>
      <returns>true if the <see cref="T:System.Web.Http.Routing.IHttpRoute" /> is found in the collection; otherwise, false.</returns>
      <param name="item">The object to locate in the collection.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.ContainsKey(System.String)">
      <summary>Determines whether the collection contains an element with the specified key.</summary>
      <returns>true if the collection contains an element with the key; otherwise, false.</returns>
      <param name="name">The key to locate in the collection.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollection.CopyTo(System.Collections.Generic.KeyValuePair{System.String,System.Web.Http.Routing.IHttpRoute}[],System.Int32)">
      <summary>Copies the <see cref="T:System.Web.Http.Routing.IHttpRoute" /> instances of the collection to an array, starting at a particular array index.</summary>
      <param name="array">The array that is the destination of the elements copied from the collection.</param>
      <param name="arrayIndex">The zero-based index in <paramref nam