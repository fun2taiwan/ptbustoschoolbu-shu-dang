namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editCarForeignKeyCarLine : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "CarLineId", "dbo.Cars");
            DropIndex("dbo.Cars", new[] { "CarLineId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Cars", "CarLineId");
            AddForeignKey("dbo.Cars", "CarLineId", "dbo.Cars", "Id");
        }
    }
}
