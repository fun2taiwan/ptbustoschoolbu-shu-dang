namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDriverAndICollectionCar : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "CarLineId", "dbo.CarLines");
            DropIndex("dbo.Cars", new[] { "CarLineId" });
            CreateTable(
                "dbo.Drivers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 50),
                        StaffNo = c.String(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Cars", "DriverId", c => c.Int());
            AddColumn("dbo.Cars", "Custodian", c => c.String());
            AddColumn("dbo.Cars", "SeatAmount", c => c.Int());
            AddColumn("dbo.Cars", "StandAmount", c => c.Int());
            AddForeignKey("dbo.Cars", "DriverId", "dbo.Drivers", "Id");
            CreateIndex("dbo.Cars", "DriverId");
            DropColumn("dbo.Cars", "CarLineId");
            DropColumn("dbo.Cars", "Passengers");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cars", "Passengers", c => c.Int());
            AddColumn("dbo.Cars", "CarLineId", c => c.Int());
            DropIndex("dbo.Cars", new[] { "DriverId" });
            DropForeignKey("dbo.Cars", "DriverId", "dbo.Drivers");
            DropColumn("dbo.Cars", "StandAmount");
            DropColumn("dbo.Cars", "SeatAmount");
            DropColumn("dbo.Cars", "Custodian");
            DropColumn("dbo.Cars", "DriverId");
            DropTable("dbo.Drivers");
            CreateIndex("dbo.Cars", "CarLineId");
            AddForeignKey("dbo.Cars", "CarLineId", "dbo.CarLines", "Id");
        }
    }
}
