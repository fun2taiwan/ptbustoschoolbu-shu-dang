resents renaming an existing column.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Model.RenameColumnOperation.#ctor(System.String,System.String,System.String,System.Object)">
            <summary>
                Initializes a new instance of the RenameColumnOperation class.
            </summary>
            <param name = "table">Name of the table the column belongs to.</param>
            <param name = "name">Name of the column to be renamed.</param>
            <param name = "newName">New name for the column.</param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="P:System.Data.Entity.Migr