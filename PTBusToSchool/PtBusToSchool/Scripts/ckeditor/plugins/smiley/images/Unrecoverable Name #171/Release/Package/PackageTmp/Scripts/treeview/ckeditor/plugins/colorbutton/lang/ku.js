arpMigrationCodeGenerator.Generate(System.Collections.Generic.IEnumerable{System.String},System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to specify a set of column names using a lambda expression.
            </summary>
            <param name = "columns">The columns to generate code for.</param>
            <param name = "writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.AddPrimaryKeyOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform an <see cref="T:System.Data.Entity.Migrations.Model.AddPrimaryKeyOperation"/>.
            </summary>
            <param name="addPrimaryKeyOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.DropPrimaryKeyOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:Syst