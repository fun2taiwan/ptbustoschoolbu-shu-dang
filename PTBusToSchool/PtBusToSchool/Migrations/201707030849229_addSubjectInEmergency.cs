namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSubjectInEmergency : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Emergencies", "Subject", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Emergencies", "Subject");
        }
    }
}
