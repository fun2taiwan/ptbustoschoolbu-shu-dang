re.DbMemberEntry.Name">
            <summary>
                Gets the name of the property.
            </summary>
            <value>The property name.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbMemberEntry.CurrentValue">
            <summary>
                Gets or sets the current value of this property.
            </summary>
            <value>The current value.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbMemberEntry.EntityEntry">
            <summary>
                The <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> to which this member belongs.
            </summary>
            <value>An entry for the entity that owns this member.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbMemberEntry.InternalMemberEntry">
            <summary>
                Gets the <see cref="P:System.Data.Entity.Infrastructure.DbMemberEntry.InternalMemberEntry"/> backing this object.
            </summary>
            <value>The internal member entry.</value>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbCollectionEntry.Create(System.Data.Entity.Internal.InternalCollectionEntry)">
            <summary>
                Creates a <see cref="T:System.Data.Entity.Infrastructure.DbCollectionEntry"/> from information in the given <see cref="T:System.Data.Entity.Internal.InternalCollectionEntry"/>.
                Use this method in preference to the constructor since it may potentially create a subclass depending on
                the type of member represented by the InternalCollectionEntry instance.
            </summary>
            <param name="internalCollectionEntry">The internal collection entry.</param>
            <returns>The new entry.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbCollectionEntry.#ctor(System.Data.Entity.Internal.InternalCollectionEntry)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbCollectionEntry"/> class.
            </summary>
            <param name="internalCollectionEntry">The internal entry.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbCollectionEntry.Load">
            <summary>
                Loads the collection of entities from the database.
                Note that entities that already exist in the context are not overwritten with values from the database.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbCollectionEntry.Query">
            <summary>
                Returns the query that would be used to load this collection from the database.
                The returned query can be modified using LINQ to perform filtering or operations in the database, such
                as counting the number of entities in the collection in the database without actually loading them.
            </summary>
            <returns>A query for the collection.</returns>
        </mem