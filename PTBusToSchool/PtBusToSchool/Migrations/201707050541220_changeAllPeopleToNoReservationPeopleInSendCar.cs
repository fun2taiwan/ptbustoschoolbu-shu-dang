namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changeAllPeopleToNoReservationPeopleInSendCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendCars", "NoReservationPeople", c => c.String());
            DropColumn("dbo.SendCars", "AllPeople");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SendCars", "AllPeople", c => c.String());
            DropColumn("dbo.SendCars", "NoReservationPeople");
        }
    }
}
