olumn(System.String,System.String,System.Func{System.Data.Entity.Migrations.Builders.ColumnBuilder,System.Data.Entity.Migrations.Model.ColumnModel},System.Object)">
            <summary>
                Adds an operation to alter the definition of an existing column.
            </summary>
            <param name = "table">
                The name of the table the column exists in.
                Schema name is optional, if no schema is specified then dbo is assumed.
            </param>
            <param name = "name">The name of the column to be changed.</param>
            <param name = "columnAction">
                An action that specifies the new definition for the column.
                i.e. c => c.String(nullable: false, defaultValue: "none")
            </param>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by 