     </member>
        <member name="M:System.Data.Entity.Migrations.Builders.ColumnBuilder.DateTime(System.Nullable{System.Boolean},System.Nullable{System.Byte},System.Nullable{System.DateTime},System.String,System.String,System.String)">
            <summary>
                Creates a new column definition to store DateTime data.
            </summary>
            <param name = "nullable">Value indicating whether or not the column allows null values.</param>
            <param name = "precision">The precision of the column.</param>
            <param name = "defaultValue">Constant value to use as the default value for this column.</param>
            <param name = "defaultValueSql">SQL expression used as the default value for this column.</param>
            <param name = "name">The name of the column.</param>
            <param name = "storeType">Provider specific data ty