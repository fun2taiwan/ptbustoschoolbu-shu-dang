namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delReservationCarNoInReservation : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Reservations", "ReservationCarNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Reservations", "ReservationCarNo", c => c.String());
        }
    }
}
