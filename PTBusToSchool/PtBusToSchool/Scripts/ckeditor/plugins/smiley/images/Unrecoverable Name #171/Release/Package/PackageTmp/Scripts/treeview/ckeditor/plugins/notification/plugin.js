            Get or set how dates are written to JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonWriter.DateTimeZoneHandling">
            <summary>
            Get or set how <see cref="T:System.DateTime"/> time zones are handling when writing JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonWriter.StringEscapeHandling">
            <summary>
            Get or set how strings are escaped when writing JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonWriter.FloatFormatHandling">
            <summary>
            Get or set how special floating point numbers, e.g. <see cref="F:System.Double.NaN"/>,
            <see cref="F:System.Double.PositiveInfinity"/> and <see cref="F:System.Double.NegativeInfinity"/>,
            are written to JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonWriter.DateFormatString">
            <summary>
            Get or set how <see cref="T:System.DateTime"/> and <see cref="T:System.DateTimeOffset"/> values are formatting when writing JSON text.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.JsonWriter.Culture">
            <summary>
            Gets or sets the culture used when writing JSON. Defaults to <see cref="P:System.Globalization.CultureInfo.InvariantCulture"/>.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.#ctor">
            <summary>
            Creates an instance of the <c>JsonWriter</c> class. 
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.Flush">
            <summary>
            Flushes whatever is in the buffer to the underlying streams and also flushes the underlying stream.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.Close">
            <summary>
            Closes this stream and the underlying stream.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteStartObject">
            <summary>
            Writes the beginning of a JSON object.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteEndObject">
            <summary>
            Writes the end of a JSON object.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteStartArray">
            <summary>
            Writes the beginning of a JSON array.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteEndArray">
            <summary>
            Writes the end of an array.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteStartConstructor(System.String)">
            <summary>
            Writes the start of a constructor with the given name.
            </summary>
            <param name="name">The name of the constructor.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteEndConstructor">
            <summary>
            Writes the end constructor.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WritePropertyName(System.String)">
            <summary>
            Writes the property name of a name/value pair on a JSON object.
            </summary>
            <param name="name">The name of the property.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WritePropertyName(System.String,System.Boolean)">
            <summary>
            Writes the property name of a name/value pair on a JSON object.
            </summary>
            <param name="name">The name of the property.</param>
            <param name="escape">A flag to indicate whether the text should be escaped when it is written as a JSON property name.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteEnd">
            <summary>
            Writes the end of the current JSON object or array.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteToken(Newtonsoft.Json.JsonReader)">
            <summary>
            Writes the current <see cref="T:Newtonsoft.Json.JsonReader"/> token and its children.
            </summary>
            <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read the token from.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteToken(Newtonsoft.Json.JsonReader,System.Boolean)">
            <summary>
            Writes the current <see cref="T:Newtonsoft.Json.JsonReader"/> token.
            </summary>
            <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader"/> to read the token from.</param>
            <param name="writeChildren">A flag indicating whether the current token's children should be written.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteToken(Newtonsoft.Json.JsonToken,System.Object)">
            <summary>
            Writes the <see cref="T:Newtonsoft.Json.JsonToken"/> token and its value.
            </summary>
            <param name="token">The <see cref="T:Newtonsoft.Json.JsonToken"/> to write.</param>
            <param name="value">
            The value to write.
            A value is only required for tokens that have an associated value, e.g. the <see cref="T:System.String"/> property name for <see cref="F:Newtonsoft.Json.JsonToken.PropertyName"/>.
            A null value can be passed to the method for token's that don't have a value, e.g. <see cref="F:Newtonsoft.Json.JsonToken.StartObject"/>.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteToken(Newtonsoft.Json.JsonToken)">
            <summary>
            Writes the <see cref="T:Newtonsoft.Json.JsonToken"/> token.
            </summary>
            <param name="token">The <see cref="T:Newtonsoft.Json.JsonToken"/> to write.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteEnd(Newtonsoft.Json.JsonToken)">
            <summary>
            Writes the specified end token.
            </summary>
            <param name="token">The end token to write.</param>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteIndent">
            <summary>
            Writes indent characters.
            </summary>
        </member>
        <member name="M:Newtonsoft.Json.JsonWriter.WriteValueDelimiter">
          