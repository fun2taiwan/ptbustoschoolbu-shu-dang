﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class ReservationEndController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc , int? id,DateTime? date)
        {
            if (id.HasValue && id != 0)
            {
            }
            else
            {
                //記住搜尋條件
                GetCatcheRoutes(page, fc);
            }
		

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var reservationend = _db.ReservationEnd.Include(r => r.ParentSendCar).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.StartDate = new SelectList(_db.ReservationEnd.OrderBy(p => p.InitDate), "Id", "StartDate");


            if (id.HasValue && id != 0)
            {
                ReservationEnd reservationendGrand = _db.ReservationEnd.Find(id);
                if (reservationendGrand.Level == 1)
                {
                    ViewBag.linename = id;
                    reservationend = reservationend.Where(x => x.ParentId == id);
                    ViewBag.Path = "→" + reservationendGrand.StartDate.GetValueOrDefault().Date.ToString("yyyy/MM/dd");

                }
                if (reservationendGrand.Level == 2)
                {
                   
                    //ViewBag.grandparentid = sendCar.ParentId;
                    ViewBag.carnotime = reservationendGrand.Line;
                    reservationend = reservationend.Where(x => x.GrandParentId == reservationendGrand.ParentId && x.ParentId == id);
                    ViewBag.Path = "→" + reservationendGrand.ParentSendCar.StartDate.GetValueOrDefault().Date.ToString("yyyy/MM/dd") + "→" + reservationendGrand.LineName;
                }
            }
            else
            {
                reservationend = reservationend.Where(x => x.ParentId == null);

            }



            if (hasViewData("SearchByStartDate")) 
            {
              
                DateTime searchByStartDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                reservationend = reservationend.Where(w => w.StartDate == searchByStartDate);
            } 


            return View(reservationend.OrderByDescending(p => p.StartDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /admin/ReservationEnd/Details/5

        public ActionResult Details(int id = 0)
        {
            ReservationEnd reservationend = _db.ReservationEnd.Find(id);
            if (reservationend == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(reservationend);
        }

    

      
        //
        // GET: /admin/ReservationEnd/Delete/5

    

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
