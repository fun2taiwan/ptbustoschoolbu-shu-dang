﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using ClosedXML.Excel;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class TimescheduleController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




        public ActionResult Index(int? page, FormCollection fc, int? id)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var timeschedule = _db.TimeSchedule.Include(t => t.LineName).Include(t => t.ParentCarLine).OrderByDescending(p => p.InitDate).AsQueryable();
            ViewBag.CarLineId = new SelectList(_db.CarLine.Where(p => p.ParentId == null).OrderBy(p => p.InitDate), "Id", "Subject");

            if (id.HasValue && id != 0)
            {
                var getcarlineid = _db.TimeSchedule.Find(id);
                var carline = _db.CarLine.Find(getcarlineid.CarLineId);
                ViewBag.AllLine = carline.Line;

                ViewBag.Path = "→" + carline.Subject;

                timeschedule = timeschedule.Where(x => x.ParentId == id);
            }
            else
            {
                timeschedule = timeschedule.Where(x => x.ParentId == null);
            }

            if (hasViewData("SearchByCarLineId"))
            {
                int searchByCarLineId = getViewDateInt("SearchByCarLineId");
                timeschedule = timeschedule.Where(w => w.CarLineId == searchByCarLineId);
            }
            if (hasViewData("SearchByParentId"))
            {
                int searchByParentId = getViewDateInt("SearchByParentId");
                timeschedule = timeschedule.Where(w => w.ParentId == searchByParentId);
            }
            if (hasViewData("SearchByStartDate") && !hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate")).AddDays(1);
                timeschedule = timeschedule.Where(w => w.InitDate >= startDate && w.InitDate <= endDate && w.ParentId == null);
            }
            if (!hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);

                timeschedule = timeschedule.Where(w => w.InitDate >= startDate && w.InitDate <= endDate && w.ParentId == null);
            }
            if (hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);
                timeschedule = timeschedule.Where(w => w.InitDate >= startDate && w.InitDate <= endDate && w.ParentId == null);
            }
            if (Session["message"] != null)
            {
                ViewBag.Message = Session["message"];
                Session["message"] = null;
            }

            return View(timeschedule.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /admin/Timeschedule/Details/5

        public ActionResult Details(int id = 0)
        {
            TimeSchedule timeschedule = _db.TimeSchedule.Find(id);
            if (timeschedule == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(timeschedule);
        }

        //
        // GET: /admin/Timeschedule/Create

        public ActionResult Create()
        {
            var carlinesid = new SelectList(_db.CarLine.Where(p => p.ParentId == null && p.IsHaveTimeTable != BooleanType.是).OrderBy(p => p.ListNum), "Id", "Subject");
            if (!carlinesid.Any())
            {
                Session["message"] = "抱歉您目前所有路線皆有時刻表，如需修改請按編輯，如需新增請先新增路線";
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.CarLineId = carlinesid;

            }

            return View();
        }

        //
        // POST: /admin/Timeschedule/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(TimeSchedule timeschedule, HttpPostedFileBase UpFiles, String updateTorF)
        {
            if (ModelState.IsValid)
            {
                if (UpFiles == null)
                {
                    _db.TimeSchedule.Add(timeschedule);
                    timeschedule.Create(_db, _db.TimeSchedule);
                }
                else
                {
                    //取得副檔名
                    string extension = UpFiles.FileName.Split('.')[UpFiles.FileName.Split('.').Length - 1];
                    //新檔案名稱
                    if (!extension.Equals("xlsx") && !extension.Equals("xls"))
                    {
                        ViewBag.Message = "您上傳的檔案非EXCEL格式";
                        return View(timeschedule);
                    }
                    string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, extension);
                    string savedName = Path.Combine(Server.MapPath("~/FileUpload/"), fileName);
                    UpFiles.SaveAs(savedName);
                    string strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " +
                                 Request.PhysicalApplicationPath + "FileUpload\\" + fileName +
                                 ";Extended Properties='Excel 12.0;HDR=NO; IMEX=1'"; //連結字串中的HDR=YES，代表略過第一欄資料
                    OleDbConnection oledb_con = new OleDbConnection(strCon);
                    oledb_con.Open();
                    OleDbCommand oledb_com = new OleDbCommand(" SELECT * FROM [工作表1$] ", oledb_con);
                    OleDbDataReader oledb_dr = oledb_com.ExecuteReader();

                    int i = 0;
                    string carlinestr = "";
                    int parentId = 0;


                    if (updateTorF == "on")
                    {
                        while (oledb_dr.Read())
                        {
                            i++;
                            if (i == 1)
                            {
                                timeschedule.FileName = fileName;
                                timeschedule.Create(_db, _db.TimeSchedule);
                                var parenttimetable =
                                    _db.TimeSchedule.FirstOrDefault(
                                        x => x.CarLineId == timeschedule.CarLineId && x.FileName == fileName);
                                parentId = parenttimetable.Id;

                            }
                            else if (i >= 4)
                            {
                                string cartime = "";
                                string carNo = "";
                                for (int j = 0; j < oledb_dr.FieldCount; j++)
                                {
                                    if (j == 0)
                                    {
                                        carNo = oledb_dr[j].ToString().Replace("班次", "");
                                        if (string.IsNullOrEmpty(carNo))
                                        {
                                            j = oledb_dr.FieldCount;
                                        }
                                    }
                                    else
                                    {
                                        cartime += Convert.ToDateTime(oledb_dr[j].ToString()).ToString("HH:mm") + "、";

                                    }

                                }
                                if (!string.IsNullOrEmpty(carNo))
                                {
                                    cartime += "、.";
                                    cartime = cartime.Replace("、、.", "");
                                    cartime = cartime.Replace("、.", "");
                                    TimeSchedule timeschedulechild = new TimeSchedule();
                                    timeschedulechild.FileName = fileName;
                                    timeschedulechild.CarNo = carNo;
                                    timeschedulechild.ParentId = parentId;
                                    timeschedulechild.Time = cartime;
                                    timeschedulechild.CarLineId = timeschedule.CarLineId;
                                    _db.TimeSchedule.Add(timeschedulechild);
                                    _db.SaveChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        while (oledb_dr.Read())
                        {
                            i++;

                            if (i == 1)
                            {

                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    carlinestr = carlinestr + oledb_dr[j].ToString() + "→";
                                    string countstr = oledb_dr[j].ToString();
                                    var carsiteold = _db.CarSite.FirstOrDefault(x => x.Subject == countstr);
                                    if (carsiteold == null)
                                    {
                                        CarSite carSite = new CarSite();
                                        carSite.Subject = oledb_dr[j].ToString();
                                        carSite.Poster = timeschedule.Poster;
                                        carSite.InitDate = timeschedule.InitDate;
                                        _db.CarSite.Add(carSite);
                                    }
                                }
                                carlinestr += "→.";
                                carlinestr = carlinestr.Replace("→→.", "");
                                carlinestr = carlinestr.Replace("→.", "");

                                timeschedule.FileName = fileName;
                                //timetable.CarLine = carlinestr;
                                //_db.TimeSchedule.Add(timeschedule);
                                timeschedule.Create(_db, _db.TimeSchedule);

                                CarLine carLine = _db.CarLine.Find(timeschedule.CarLineId);
                                carLine.Line = carlinestr;
                                carLine.IsHaveTimeTable = BooleanType.是;
                                var carlineallsite = _db.CarLine.Where(x => x.ParentId == carLine.Id);
                                foreach (var item in carlineallsite)
                                {
                                    _db.CarLine.Remove(item);
                                }

                                _db.SaveChanges();

                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {

                                    CarLine carLinenewsite = new CarLine();
                                    carLinenewsite.Subject = oledb_dr[j].ToString();
                                    carLinenewsite.ParentId = timeschedule.CarLineId;
                                    carLinenewsite.ListNum = j;
                                    carLinenewsite.MaxPassengers = 0;
                                    _db.CarLine.Add(carLinenewsite);
                                }
                                _db.SaveChanges();
                                var parenttimetable =
                                    _db.TimeSchedule.FirstOrDefault(
                                        x => x.CarLineId == timeschedule.CarLineId && x.FileName == fileName);
                                parentId = parenttimetable.Id;



                            }
                            else if (i == 2)
                            {
                                string[] carlinearr = carlinestr.Split('→');
                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    string site = carlinearr[j - 1];
                                    CarSite carSite = _db.CarSite.FirstOrDefault(x => x.Subject == site);
                                    carSite.Lat = oledb_dr[j].ToString();
                                }
                                _db.SaveChanges();
                            }
                            else if (i == 3)
                            {
                                string[] carlinearr = carlinestr.Split('→');
                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    string site = carlinearr[j - 1];
                                    CarSite carSite = _db.CarSite.FirstOrDefault(x => x.Subject == site);
                                    carSite.Lon = oledb_dr[j].ToString();
                                }
                                _db.SaveChanges();
                            }
                            else if (i >= 4)
                            {
                                string cartime = "";
                                string carNo = "";
                                for (int j = 0; j < oledb_dr.FieldCount; j++)
                                {
                                    if (j == 0)
                                    {
                                        carNo = oledb_dr[j].ToString().Replace("班次", "");
                                        if (string.IsNullOrEmpty(carNo))
                                        {
                                            j = oledb_dr.FieldCount;
                                        }
                                    }
                                    else
                                    {
                                        cartime += Convert.ToDateTime(oledb_dr[j].ToString()).ToString("HH:mm") + "、";

                                    }

                                }
                                if (!string.IsNullOrEmpty(carNo))
                                {
                                    cartime += "、.";
                                    cartime = cartime.Replace("、、.", "");
                                    cartime = cartime.Replace("、.", "");
                                    TimeSchedule timeschedulechild = new TimeSchedule();
                                    timeschedulechild.FileName = fileName;
                                    timeschedulechild.CarNo = carNo;
                                    timeschedulechild.ParentId = parentId;
                                    timeschedulechild.Time = cartime;
                                    timeschedulechild.CarLineId = timeschedule.CarLineId;
                                    _db.TimeSchedule.Add(timeschedulechild);
                                    _db.SaveChanges();
                                }
                            }
                        
                        }
                    }
                    oledb_dr.Close();
                    oledb_con.Close();


                }

                return RedirectToAction("Index");
            }




            //_db.TimeSchedule.Add(timeschedule);
            //timeschedule.Create(_db,_db.TimeSchedule);


            ViewBag.CarLineId = new SelectList(_db.CarLine.Where(p => p.ParentId == null).OrderBy(p => p.ListNum), "Id", "Subject", timeschedule.CarLineId);

            return View(timeschedule);
        }

        //
        // GET: /admin/Timeschedule/Edit/5

        public ActionResult Edit(int id = 0)
        {
            TimeSchedule timeschedule = _db.TimeSchedule.Find(id);

            if (timeschedule == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarLineId = new SelectList(_db.CarLine.Where(p => p.ParentId == null).OrderBy(p => p.ListNum), "Id", "Subject", timeschedule.CarLineId);
            ViewBag.ParentId = new SelectList(_db.TimeSchedule.OrderBy(p => p.InitDate), "Id", "CarNo", timeschedule.ParentId);
            return View(timeschedule);
        }

        //
        // POST: /admin/Timeschedule/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(TimeSchedule timeschedule, HttpPostedFileBase UpFiles, String updateTorF)
        {

            if (Request["excel"] != null)
            {

                var timescheduleall = _db.TimeSchedule.Where(x => x.ParentId == timeschedule.Id);
                Byte[] fileContent = ExportExcel(timescheduleall.ToList(), timeschedule);

                var carline = _db.CarLine.Find(timeschedule.CarLineId);
                //string extension = timetable.FileName.Split('.')[timetable.FileName.Split('.').Length - 1]+"x";
                string filename = carline.Subject + "時刻表.xlsx";
                return File(fileContent, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                //return File(filepath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", timetable.Subject + "時刻表." + extension);


            }

            //ModelState.Remove("LineName");
            if (ModelState.IsValid)
            {
                if (UpFiles == null)
                {
                    timeschedule.Update();



                    return RedirectToAction("Index", new { Page = -1 });
                }
                else
                {  //取得副檔名
                    string extension = UpFiles.FileName.Split('.')[UpFiles.FileName.Split('.').Length - 1];
                    //新檔案名稱
                    if (!extension.Equals("xlsx") && !extension.Equals("xls"))
                    {
                        ViewBag.Message = "您上傳的檔案非EXCEL格式";
                        return View(timeschedule);
                    }
                    string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, extension);
                    string savedName = Path.Combine(Server.MapPath("~/FileUpload/"), fileName);
                    UpFiles.SaveAs(savedName);
                    string strCon = strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " +
                                 Request.PhysicalApplicationPath + "FileUpload\\" + fileName +
                                 ";Extended Properties='Excel 12.0;HDR=NO; IMEX=1'"; //連結字串中的HDR=YES，代表略過第一欄資料
                    List<TimeSchedule> timeSchedules =
                         _db.TimeSchedule.Where(x => x.ParentId == timeschedule.Id).ToList();
                    string editIdInsendcars = "";
                    string mewIdIntimeSchedules = "";
                    if (updateTorF == "on")
                    {
                        //strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " +
                        //         Request.PhysicalApplicationPath + "FileUpload\\" + fileName +
                        //         ";Extended Properties='Excel 12.0;HDR=YES; IMEX=1'"; //連結字串中的HDR=YES，代表略過第一欄資料
                    }
                    else
                    {
                        //strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " +
                        //         Request.PhysicalApplicationPath + "FileUpload\\" + fileName +
                        //         ";Extended Properties='Excel 12.0;HDR=NO; IMEX=1'"; //連結字串中的HDR=YES，代表略過第一欄資料


                        //foreach (var item in timeSchedules)
                        //{

                        //    List<SendCar> sendCars = _db.SendCars.Where(x => x.TimeScheduleId == item.Id).ToList();
                        //    foreach (var items in sendCars)
                        //    {
                        //        items.TimeScheduleId = null;
                        //        editIdInsendcars += items.Id + "、";
                        //        _db.SaveChanges();
                        //    }




                        //    _db.TimeSchedule.Remove(item);
                        //}
                        //_db.SaveChanges();
                    }


                    OleDbConnection oledb_con = new OleDbConnection(strCon);
                    oledb_con.Open();
                    OleDbCommand oledb_com = new OleDbCommand(" SELECT * FROM [工作表1$] ", oledb_con);
                    OleDbDataReader oledb_dr = oledb_com.ExecuteReader();

                    int i = 0;
                    string carlinestr = "";

                    if (updateTorF == "on")
                    {
                        var timescheduleall =_db.TimeSchedule.Where(x => x.ParentId == timeschedule.Id).ToList();
                        int allcount = timescheduleall.Count();
                        while (oledb_dr.Read())
                        {
                            i++;
                            if (i >=4)
                            {
                             
                                string cartime = "";
                                string carNo = "";
                                for (int j = 0; j < oledb_dr.FieldCount; j++)
                                {
                                    if (j == 0)
                                    {
                                        carNo = oledb_dr[j].ToString().Replace("班次", "");
                                       }
                                    else
                                    {
                                        cartime += Convert.ToDateTime(oledb_dr[j].ToString()).ToString("HH:mm") + "、";

                                    }

                                }
                                cartime += "、.";
                                cartime = cartime.Replace("、、.", "");
                                cartime = cartime.Replace("、.", "");
                                var timescheduleold =
                                    _db.TimeSchedule.FirstOrDefault(x => x.CarNo == carNo && x.ParentId == timeschedule.Id);
                                if (timescheduleold==null)
                                {

                                    TimeSchedule timeschedulechild = new TimeSchedule();
                                    timeschedulechild.FileName = fileName;
                                    timeschedulechild.CarNo = carNo;
                                    timeschedulechild.ParentId = timeschedule.Id;
                                    timeschedulechild.Time = cartime;
                                    timeschedulechild.CarLineId = timeschedule.CarLineId;
                                    _db.TimeSchedule.Add(timeschedulechild);
                                    _db.SaveChanges();
                                }
                                else
                                {
                                    timescheduleold.Time = cartime;
                                    _db.SaveChanges();
                                }



                            }
                        }
                        if ((i - 3) < allcount)
                        {
                            for (int j = 0; j < timescheduleall.Count; j++)
                            {
                                if (j == timescheduleall.Count - 1)
                                {
                                    _db.TimeSchedule.Remove(timescheduleall[j]);
                                    _db.SaveChanges();
                                }
                            }
                        }

                        //Boolean delYN = false;
                        //while (oledb_dr.Read())
                        //{
                        //    i++;

                        //    if (i == 1)
                        //    {




                        //        timeschedule.FileName = fileName;
                        //        //timetable.CarLine = carlinestr;
                        //        timeschedule.Update();


                        //    }

                        //    string cartime = "";
                        //    string carNo = "";
                        //    for (int j = 0; j < oledb_dr.FieldCount; j++)
                        //    {
                        //        if (j == 0)
                        //        {
                        //            carNo = oledb_dr[j].ToString();
                        //        }
                        //        else
                        //        {
                        //            cartime += Convert.ToDateTime(oledb_dr[j]).ToString("HH:mm") + "、";
                        //        }

                        //    }

                        //    cartime += "、.";
                        //    cartime = cartime.Replace("、、.", "");
                        //    cartime = cartime.Replace("、.", "");

                        //    if ((i - 1) >= timeSchedules.Count)
                        //    {
                        //        delYN = true;
                        //        TimeSchedule timeschedulechildon = new TimeSchedule();
                        //        timeschedulechildon.FileName = fileName;
                        //        timeschedulechildon.CarNo = carNo;
                        //        timeschedulechildon.ParentId = timeschedule.Id;
                        //        timeschedulechildon.Time = cartime;
                        //        timeschedulechildon.CarLineId = timeschedule.CarLineId;
                        //        _db.TimeSchedule.Add(timeschedulechildon);
                        //    }
                        //    else
                        //    {
                        //        timeSchedules[i - 1].Time = cartime;
                        //        timeSchedules[i - 1].FileName = fileName;
                        //    }






                        //}
                        //_db.SaveChanges();

                        //if (delYN)
                        //{
                        //    List<SendCar> sendCarsGrandparents = _db.SendCars.Where(x => x.StartDate >= DateTime.Today.Date).ToList();
                        //    foreach (var item in sendCarsGrandparents)
                        //    {
                        //        List<SendCar> sendCars = _db.SendCars.Where(x => x.GrandParentId == item.Id).ToList();
                        //        List<SendCar> sendCarsParents = _db.SendCars.Where(x => x.ParentId == item.Id).ToList();
                        //        foreach (var items in sendCars)
                        //        {
                        //            _db.SendCars.Remove(items);
                        //        }
                        //        _db.SaveChanges();
                        //        foreach (var items in sendCarsParents)
                        //        {
                        //            _db.SendCars.Remove(items);
                        //        }
                        //        _db.SaveChanges();

                        //        _db.SendCars.Remove(item);
                        //        _db.SaveChanges();
                        //    }
                        //}
                        //if (i < timeSchedules.Count)
                        //{
                        //    for (int j = i; j < timeSchedules.Count; j++)
                        //    {
                        //        int id = timeSchedules[j].Id;
                        //        List<SendCar> sendCars = _db.SendCars.Where(x => x.TimeScheduleId == id).ToList();

                        //        foreach (var item in sendCars)
                        //        {
                        //            _db.SendCars.Remove(item);
                        //        }

                        //        _db.SaveChanges();


                        //        _db.TimeSchedule.Remove(timeSchedules[j]);
                        //    }
                        //    _db.SaveChanges();
                        //}
                    }
                    else
                    {
                        while (oledb_dr.Read())
                        {
                            i++;

                            if (i == 1)
                            {

                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    carlinestr = carlinestr + oledb_dr[j].ToString() + "→";
                                    string countstr = oledb_dr[j].ToString();
                                    var carsiteold = _db.CarSite.FirstOrDefault(x => x.Subject == countstr);
                                    if (carsiteold == null)
                                    {
                                        CarSite carSite = new CarSite();
                                        carSite.Subject = oledb_dr[j].ToString();
                                        carSite.Poster = timeschedule.Poster;
                                        carSite.InitDate = timeschedule.InitDate;
                                        _db.CarSite.Add(carSite);




                                    }


                                }
                                carlinestr += "→.";
                                carlinestr = carlinestr.Replace("→→.", "");
                                carlinestr = carlinestr.Replace("→.", "");

                                timeschedule.FileName = fileName;
                                //timetable.CarLine = carlinestr;
                                timeschedule.Update();

                                CarLine carLine = _db.CarLine.Find(timeschedule.CarLineId);
                                carLine.Line = carlinestr;
                                var carlineallsite = _db.CarLine.Where(x => x.ParentId == carLine.Id);
                                foreach (var item in carlineallsite)
                                {
                                    _db.CarLine.Remove(item);
                                }

                                _db.SaveChanges();

                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    CarLine carLinenewsite = new CarLine();
                                    carLinenewsite.Subject = oledb_dr[j].ToString();
                                    carLinenewsite.ParentId = timeschedule.CarLineId;
                                    carLinenewsite.ListNum = j;
                                    carLinenewsite.MaxPassengers = 0;
                                    _db.CarLine.Add(carLinenewsite);
                                }
                                _db.SaveChanges();




                            }
                            else if (i == 2)
                            {
                                string[] carlinearr = carlinestr.Split('→');
                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    string site = carlinearr[j - 1];
                                    CarSite carSite = _db.CarSite.FirstOrDefault(x => x.Subject == site);
                                    carSite.Lat = oledb_dr[j].ToString();
                                }
                                _db.SaveChanges();
                            }
                            else if (i == 3)
                            {
                                string[] carlinearr = carlinestr.Split('→');
                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    string site = carlinearr[j - 1];
                                    CarSite carSite = _db.CarSite.FirstOrDefault(x => x.Subject == site);
                                    carSite.Lon = oledb_dr[j].ToString();
                                }
                                _db.SaveChanges();
                            }
                            else
                            {
                                string cartime = "";
                                string carNo = "";
                                for (int j = 0; j < oledb_dr.FieldCount; j++)
                                {
                                    if (j == 0)
                                    {
                                        carNo = oledb_dr[j].ToString().Replace("班次", "");
                                    }
                                    else
                                    {
                                        cartime += Convert.ToDateTime(oledb_dr[j].ToString()).ToString("HH:mm") + "、";
                                    }

                                }

                                cartime += "、.";
                                cartime = cartime.Replace("、、.", "");
                                cartime = cartime.Replace("、.", "");


                                var timescheduleold =
                                _db.TimeSchedule.FirstOrDefault(x => x.CarNo == carNo && x.ParentId == timeschedule.Id);
                                if (timescheduleold == null)
                                {

                                    TimeSchedule timeschedulechild = new TimeSchedule();
                                    timeschedulechild.FileName = fileName;
                                    timeschedulechild.CarNo = carNo;
                                    timeschedulechild.ParentId = timeschedule.Id;
                                    timeschedulechild.Time = cartime;
                                    timeschedulechild.CarLineId = timeschedule.CarLineId;
                                    _db.TimeSchedule.Add(timeschedulechild);
                                    _db.SaveChanges();
                                }
                                else
                                {
                                    timescheduleold.Time = cartime;
                                    _db.SaveChanges();
                                }


                              
                            
                            }
                        }


                        SendCar sendCar = _db.SendCars.FirstOrDefault(x => x.StartDate == DateTime.Today.Date);
                        if (sendCar != null)
                        {
                            var senCarchild = _db.SendCars.Where(x => x.ParentId == sendCar.Id);
                            var senCarchildchild = _db.SendCars.Where(x => x.GrandParentId == sendCar.Id);
                            foreach (var item in senCarchildchild)
                            {
                                _db.SendCars.Remove(item);

                            }
                            _db.SaveChanges();
                            foreach (var item in senCarchild)
                            {
                                _db.SendCars.Remove(item);

                            }
                            _db.SaveChanges();

                            _db.SendCars.Remove(sendCar);
                            _db.SaveChanges();
                        }
                     









                    }
                    oledb_dr.Close();
                    oledb_con.Close();



                }
                //_db.Entry(timeschedule).State = EntityState.Modified;
                //timeschedule.Update();
                return RedirectToAction("Index", new { Page = -1 });
            }
            ViewBag.CarLineId = new SelectList(_db.CarLine.Where(x => x.ParentId == null).OrderBy(p => p.ListNum), "Id", "Subject", timeschedule.CarLineId);
            ViewBag.ParentId = new SelectList(_db.TimeSchedule.OrderBy(p => p.InitDate), "Id", "CarNo", timeschedule.ParentId);
            return View(timeschedule);
        }


        public byte[] ExportExcel(IEnumerable<TimeSchedule> list, TimeSchedule timetable)
        {
            //建立Excel
            XLWorkbook workbook = new XLWorkbook();
            var sheet = workbook.Worksheets.Add("工作表1");
            int colIdx = 1;
            var carLine = _db.CarLine.Find(timetable.CarLineId);
            string[] carLinestr = carLine.Line.Split('→');

            string columnstr = "站點";
            string Latstr = "經度Lat";
            string Lonstr = "緯度Lon";
            for (int i = 0; i < carLinestr.Length; i++)
            {
                string carSitename = carLinestr[i];
                CarSite carSite = _db.CarSite.FirstOrDefault(x => x.Subject == carSitename);
                Latstr += "#" + carSite.Lat;
                Lonstr += "#" + carSite.Lon;
                columnstr += "#" + carLinestr[i];
            }
            String[] columns = columnstr.Split('#');
            String[] Lat = Latstr.Split('#');
            String[] Lon = Lonstr.Split('#');

            //String[] columns = { "訂單編號", "訂單日期", "購買人姓名", "購買人電話", "付款方式", "訂單處理狀態", "小計(不含運費)", "商城名稱", "供應商訂單處理狀態", "運費" };
            //String[] columns = { list.FirstOrDefault().Account,  list.FirstOrDefault().Name, list.FirstOrDefault().Gender, "E-Mail", "電話", "住址", "發佈者", "發佈時間", list.FirstOrDefault().Account};
            foreach (string colName in columns)
            {
                sheet.Cell(1, colIdx++).Value = colName;
            }
            //修改標題列Style
            //var header = sheet.Range("A1:J1");
            //header.Style.Fill.BackgroundColor = XLColor.Yellow;
            //header.Style.Font.FontColor = XLColor.Red;
            //header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

            int rowIdx = 2;
            colIdx = 1;
            foreach (string colName in Lat)
            {
                sheet.Cell(rowIdx, colIdx++).Value = colName;
            }
            rowIdx++;
            colIdx = 1;
            foreach (string colName in Lon)
            {
                sheet.Cell(rowIdx, colIdx++).Value = colName;
            }
            rowIdx++;


            foreach (var item in list)
            {
                //if(item.)
                //sheet.Cell(rowIdx, 1).Style.NumberFormat.Format = "############################";
                sheet.Cell(rowIdx, 1).Value = "班次" + item.CarNo;
                string[] timearr = item.Time.Split('、');
                int count = 2;
                for (int i = 0; i < timearr.Length; i++)
                {
                    sheet.Cell(rowIdx, count).Value = "'" + timearr[i];
                    count++;
                }



                rowIdx++;


            }






            ////第一欄隱藏
            //sheet.Column(1).Hide();
            ////自動伸縮欄寬
            //sheet.Column(2).AdjustToContents();
            //sheet.Column(2).Width += 2;
            //sheet.Column(3).Width = 50;
            ////寫入檔案-0
            //workbook.SaveAs(excelPath);
            for (int i = 1; i < columns.Length; i++)
            {
                sheet.Column(i).AdjustToContents();

            }

            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                bytes = memoryStream.ToArray();
            }
            return bytes;
        }


        //
        // GET: /admin/Timeschedule/Delete/5

        public ActionResult Delete(int id = 0)
        {
            TimeSchedule timeschedule = _db.TimeSchedule.Find(id);
            if (timeschedule == null)
            {
                return HttpNotFound();
            }
            return View(timeschedule);
        }

        //
        // POST: /admin/Timeschedule/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            List<TimeSchedule> timeschedulechild = _db.TimeSchedule.Where(x => x.ParentId == id).ToList();
            if (timeschedulechild.Count > 0)
            {
                foreach (var item in timeschedulechild)
                {


                    List<SendCar> sendCars = _db.SendCars.Where(x => x.TimeScheduleId == item.Id).ToList();
                    foreach (var items in sendCars)
                    {
                        _db.SendCars.Remove(items);

                        _db.SaveChanges();
                    }


                }
                foreach (var item in timeschedulechild)
                {
                    List<SendCar> sendCarsParents = _db.SendCars.Where(x => x.CarLineId == item.CarLineId).ToList();
                    foreach (var items in sendCarsParents)
                    {
                        _db.SendCars.Remove(items);

                        _db.SaveChanges();
                    }




                    _db.TimeSchedule.Remove(item);
                }
                _db.SaveChanges();
            }

            TimeSchedule timeschedule = _db.TimeSchedule.Find(id);
            CarLine carLine = _db.CarLine.Find(timeschedule.CarLineId);
            carLine.IsHaveTimeTable = BooleanType.否;
            _db.TimeSchedule.Remove(timeschedule);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
