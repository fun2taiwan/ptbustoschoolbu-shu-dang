ummary>Provides an abstract class to implement a metadata provider.</summary>
      <typeparam name="TModelMetadata">The type of the model metadata.</typeparam>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1.CreateMetadataFromPrototype(`0,System.Func{System.Object})">
      <summary>When overridden in a derived class, creates the model metadata for the property using the specified prototype.</summary>
      <returns>The model metadata for the property.</returns>
      <param name="prototype">The prototype from which to create the model metadata.</param>
      <param name="modelAccessor">The model accessor.</param>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1.CreateMetadataPrototype(System.Collections.Generic.IEnumerable{System.Attribute},System.Type,System.Type,System.String)">
      <summary>When overridden in a derived class, creates the model metadata for the property.</summary>
      <returns>The model metadata for the property.</returns>
      <param name="attributes">The set of attributes.</param>
      <param name="containerType">The type of the container.</param>
      <param name="modelType">The type of the model.</param>
      <param name="propertyName">The name of the property.</param>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1.GetMetadataForProperties(System.Object,System.Type)">
      <summary>Retrieves a list of properties for the model.</summary>
      <returns>A list of properties for the model.</returns>
      <param name="container">The model container.</param>
      <param name="containerType">The type of the container.</param>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1.GetMetadataForProperty(System.Func{System.Object},System.Type,System.String)">
      <summary>Retrieves the metadata for the specified property using the container type and property name.</summary>
      <returns>The metadata for the specified property.</returns>
      <param name="modelAccessor">The model accessor.</param>
      <param name="containerType">The type of the container.</param>
      <param name="propertyName">The name of the property.</param>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.AssociatedMetadataProvider`1.GetMetadataForType(System.Func{System.Object},System.Type)">
      <summary>Returns the metadata for the specified property using the type of the model.</summary>
      <returns>The metadata for the specified property.</returns>
      <param name="modelAccessor">The model accessor.</param>
      <param name="modelType">The type of the container.</param>
    </member>
    <member name="T:System.Web.Http.Metadata.Providers.CachedDataAnnotationsMetadataAttributes">
      <summary>Provides prototype cache data for <see cref="T:System.Web.Http.Metadata.Providers.CachedModelMetadata`1" />.</summary>
    </member>
    <member name="M:System.Web.Http.Metadata.Providers.CachedDataAnnotationsMetadataAttributes.#ctor(System.Collections.Generic.IEnumerable{System.Attribute})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Metadata.Providers.CachedDataAnnotationsMetadataAttributes" /> class.</summary>
      <param name="attributes">The attributes that provides data for the initialization.</param>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedDataAnnotationsMetadataAttributes.Display">
      <summary>Gets or sets the metadata display attribute.</summary>
      <returns>The metadata display attribute.</returns>
    </member>
    <member name="P:System.Web.Http.Metadata.Providers.CachedDataAnnotationsMetadataAttributes.DisplayFormat">
      <summary>Gets or sets the metadata display format attribute.</summary>
      <