m>
            <returns>The equivalent generic object.</returns>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbComplexPropertyEntry`2">
            <summary>
                Instances of this class are returned from the ComplexProperty method of
                <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> and allow access to the state of a complex property.
            </summary>
            <typeparam name="TEntity">The type of the entity to which this property belongs.</typeparam>
            <typeparam name="TComplexProperty">The type of the property.</typeparam>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbPropertyEntry`2">
            <summary>
                Instances of this class are returned from the Property method of
                <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> and allow access to the state of the scalar
                or complex property.
            </summary>
            <type