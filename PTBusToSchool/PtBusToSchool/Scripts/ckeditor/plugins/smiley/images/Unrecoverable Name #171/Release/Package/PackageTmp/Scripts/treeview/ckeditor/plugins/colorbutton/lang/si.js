his column.</param>
            <returns>The newly constructed column definition.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Builders.ColumnBuilder.String(System.Nullable{System.Boolean},System.Nullable{System.Int32},System.Nullable{System.Boolean},System.Nullable{System.Boolean},System.Nullable{System.Boolean},System.String,System.String,System.String,System.String)">
            <summary>
                Creates a new column definition to store String data.
            </summary>
            <param name = "nullable">Value indicating whether or not the column allows null values.</param>
            <param name = "maxLength">The maximum allowable length of the string data.</param>
            <param name = "fixedLength">Value indicating whether or not all data should be padded to the maximum length.</param>
            <param name = "isMaxLength">Value indicating whether or not the max