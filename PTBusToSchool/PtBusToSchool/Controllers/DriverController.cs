﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class DriverController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var drivers = _db.Drivers.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                drivers = drivers.Where(w => w.Subject.Contains(searchBySubject)); 
            } 
 

//            ViewBag.Subject = Subject;
            return View(drivers.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /admin/Driver/Details/5

        public ActionResult Details(int id = 0)
        {
            Driver driver = _db.Drivers.Find(id);
            if (driver == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(driver);
        }

        //
        // GET: /admin/Driver/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /admin/Driver/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Driver driver )
        {
            if (ModelState.IsValid)
            {

                _db.Drivers.Add(driver);
                driver.Create(_db,_db.Drivers);
                return RedirectToAction("Index");
            }

            return View(driver);
        }

        //
        // GET: /admin/Driver/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Driver driver = _db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        //
        // POST: /admin/Driver/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Driver driver)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(driver).State = EntityState.Modified;
                driver.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(driver);
        }

        //
        // GET: /admin/Driver/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Driver driver = _db.Drivers.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            return View(driver);
        }

        //
        // POST: /admin/Driver/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Driver driver = _db.Drivers.Find(id);
            _db.Drivers.Remove(driver);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
