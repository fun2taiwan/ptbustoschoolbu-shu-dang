m>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbUpdateException.DbUpdateExceptionState.InvolvesIndependentAssociations">
            <summary>
                Gets or sets a value indicating whether the exception involved independent associations.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException.#ctor(System.Data.Entity.Internal.InternalContext,System.Data.OptimisticConcurrencyException)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException"/> class.
            </summary>
            <param name="context">The context.</param>
            <param name="innerException">The inner exception.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException.#ctor">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbUpdateException"/> class.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException.#ctor(System.String)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbUpdateException"/> class.
            </summary>
            <param name="message">The message.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException.#ctor(System.String,System.Exception)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbUpdateException"/> class.
            </summary>
            <param name="message">The message.</param>
            <param name="innerException">The inner exception.</param>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.EdmMetadata">
            <summary>
                Represents an entity used to store metadata about an EDM in the database.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.EdmMetadata.TryGetModelHash(System.Data.Entity.DbContext)">
            <summary>
                Attempts to get the model hash calculated by Code First for the given context.
                This method will return null if the context is not being used in Code First mode.
            </summary>
            <param name = "context">The context.</param>
            <returns>The hash string.</returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.EdmMetadata.Id">
            <summary>
                Gets or sets the ID of the metadata entity, which is currently always 1.
            </summary>
            <value>The id.</value>
        </member>
        <member name="