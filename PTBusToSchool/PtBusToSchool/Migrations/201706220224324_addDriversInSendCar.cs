namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDriversInSendCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendCars", "Drivers", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SendCars", "Drivers");
        }
    }
}
