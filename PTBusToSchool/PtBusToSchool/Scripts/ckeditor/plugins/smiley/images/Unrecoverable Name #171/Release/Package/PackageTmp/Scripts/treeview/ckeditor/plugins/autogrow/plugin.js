name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.InvalidRelationshipEndType">
            <summary>
                Invalid type used for a Relationship End Type
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.InvalidPrimitiveTypeKind">
            <summary>
                Invalid PrimitiveTypeKind
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.InvalidTypeConversionDestinationType">
            <summary>
                Invalid TypeConversion DestinationType
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.ByteValueExpected">
            <summary>
                Expected a integer value between 0 - 255
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.FunctionWithNonScalarTypeNotSupported">
            <summary>
                Invalid Type specified in function
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.PrecisionMoreThanAllowedMax">
            <summary>
                Precision must not be greater than 28
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.EntityKeyMustBeScalar">
            <summary>
                Properties that are part of entity key must be of scalar type
            </summary>
        </member>
        <member name="F:System.Data.Entity.Edm.Parsing.Xml.Internal.XmlErrorCode.BinaryEntityKeyCurrentlyNotSupported">
            <summary>
                Binary type properties which are part of entity key are currently not supported
            </summary>
        </me