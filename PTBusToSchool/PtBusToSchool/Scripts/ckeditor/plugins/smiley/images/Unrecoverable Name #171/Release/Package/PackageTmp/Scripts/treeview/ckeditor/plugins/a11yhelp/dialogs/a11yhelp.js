y>
                Marks the given entity as Deleted such that it will be deleted from the database when SaveChanges
                is called.  Note that the entity must exist in the context in some other state before this method
                is called.
            </summary>
            <param name = "entity">The entity to remove.</param>
            <returns>The entity.</returns>
            <remarks>
                Note that if the entity exists in the context in the Added state, then this method
                will cause it to be detached from the context.  This is because an Added entity is assumed not to
                exist in the database such that trying to delete it does not make sense.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.IDbSet`1.Attach(`0)">
            <summary>
                Attaches the given entity to the context underlying the set.  That is, the entity is placed
                into the context in the Unchanged state, just as if it had been read from the database.
            </summary>
            <param name = "entity">The entity to attach.</param>
            <returns>The entity.</returns>
            <remarks>
                Attach is used to repopulate a context with an entity that is known to already exist in the database.
                SaveChanges will therefore not attempt to insert an attached entity into the database because
                it is assumed to already be there.
                Note that entities that are already in the context in some other state will have their state set
                to Unchanged.  Attach is a no-op if the entity is already in the context in the Unchanged state.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.IDbSet`1.Create">
            <summary>
                Creates a new instance of an entity for the type of this set.
                Note that this instance is NOT added or attached to the set.
                The instance returned will be a proxy if the underlying context is configured to create
                proxies and the entity type meets the requirements for creating a proxy.
            </summary>
            <returns>The entity instance, which may be a proxy.</returns>
        </member>
        <member name="M:System.Data.Entity.IDbSet`1.Create``1">
            <summary>
                Creates a new instance of an entity for the type of this set or for a type derived
                from the type of this set.
                Note that this instance is NOT added or attached to the set.
                The instance returned will be a proxy if the underlying context is configured to create
                proxi