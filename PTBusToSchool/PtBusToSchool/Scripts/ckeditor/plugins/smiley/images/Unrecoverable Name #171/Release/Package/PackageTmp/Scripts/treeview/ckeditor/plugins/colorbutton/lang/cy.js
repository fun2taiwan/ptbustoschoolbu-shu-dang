r.Generate(System.Guid)">
            <summary>
                Generates SQL to specify a constant Guid default value being set on a column.
                This method just generates the actual value, not the SQL to set the default value.
            </summary>
            <param name = "defaultValue">The value to be set.</param>
            <returns>SQL representing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Sql.SqlServerMigrationSqlGenerator.Generate(System.String)">
            <summary>
                Generates SQL to specify a constant string default value being set on a column.
                This method just generates the actual value, not the SQL to set the default value.
            </summary>
            <param name = "defaultValue">The value to be set.</param>
            <returns>SQL representing the default v