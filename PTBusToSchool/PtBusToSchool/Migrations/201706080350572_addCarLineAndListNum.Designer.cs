// <auto-generated />
namespace PtBusToSchool.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class addCarLineAndListNum : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addCarLineAndListNum));
        
        string IMigrationMetadata.Id
        {
            get { return "201706080350572_addCarLineAndListNum"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
