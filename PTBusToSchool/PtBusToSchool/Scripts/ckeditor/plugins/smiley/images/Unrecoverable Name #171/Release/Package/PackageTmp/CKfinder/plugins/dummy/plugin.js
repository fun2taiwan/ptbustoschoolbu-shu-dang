<?xml version="1.0"?>
<doc>
    <assembly>
        <name>Newtonsoft.Json</name>
    </assembly>
    <members>
        <member name="T:Newtonsoft.Json.Bson.BsonObjectId">
            <summary>
            Represents a BSON Oid (object id).
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Bson.BsonObjectId.Value">
            <summary>
            Gets or sets the value of the Oid.
            </summary>
            <value>The value of the Oid.</value>
        </member>
        <member name="M:Newtonsoft.Json.Bson.BsonObjectId.#ctor(System.Byte[])">
            <summary>
            Initializes a new instance of the <see cref="T:Newtonsoft.Json.Bson.BsonObjectId"/> class.
            </summary>
            <param name="value">The Oid value.</param>
        </member>
        <member name="T:Newtonsoft.Json.Bson.BsonReader">
            <summary>
            Represents a reader that provides fast, non-cached, forward-only access to serialized JSON data.
            </summary>
        </member>
        <member name="P:Newtonsoft.Json.Bson.BsonReader.JsonNet35BinaryCompatibility">
            <summary>
            Gets or sets a value indicating whether binary data reading should compatible with incorrect Json.NET 3.5 written binary.
            </summary>
            <value>
            	<c>true</c> if binary data reading will be compatible with incorrect Json.NET 3.5 written binary; otherwise, <c>false</c>.
            </value>
        </member>
        <member name="P:Newtonsoft.Json.Bson.BsonReader.ReadRootValueAsArray">
            <summary>
            Gets or sets a value indicating whether the root object will be read as a JSON array.
            </summary>
            <value>