// <auto-generated />
namespace PtBusToSchool.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class addIsHaveTimeTableInCarLine : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addIsHaveTimeTableInCarLine));
        
        string IMigrationMetadata.Id
        {
            get { return "201707030319521_addIsHaveTimeTableInCarLine"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
