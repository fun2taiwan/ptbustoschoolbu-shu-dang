namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarForeignKeyCarLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "CarLineId", c => c.Int());
            AddForeignKey("dbo.Cars", "CarLineId", "dbo.Cars", "Id");
            AddForeignKey("dbo.Cars", "CarLineId", "dbo.CarLines", "Id");
            CreateIndex("dbo.Cars", "CarLineId");
            CreateIndex("dbo.Cars", "CarLineId");
            DropColumn("dbo.Cars", "CarLine");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Cars", "CarLine", c => c.String());
            DropIndex("dbo.Cars", new[] { "CarLineId" });
            DropIndex("dbo.Cars", new[] { "CarLineId" });
            DropForeignKey("dbo.Cars", "CarLineId", "dbo.CarLines");
            DropForeignKey("dbo.Cars", "CarLineId", "dbo.Cars");
            DropColumn("dbo.Cars", "CarLineId");
        }
    }
}
