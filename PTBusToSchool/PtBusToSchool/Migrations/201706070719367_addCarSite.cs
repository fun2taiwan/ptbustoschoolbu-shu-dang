namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarSite : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarSites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 50),
                        Lat = c.String(),
                        Lon = c.String(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CarSites");
        }
    }
}
