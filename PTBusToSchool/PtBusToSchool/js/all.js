﻿// $(".timeTable").tableHeadFixer();
// $(".timeTable").parent("div").css("height","300px");

function popLoadingPage (){
  $('.loading-page').css('display','block')
}
function closeLoadingPage (){
  $('.loading-page').css('display','none')
}

$('.sidebar-toggle-button').click(function(event) {
  $('.sidebar-menu li > div > a > span').toggleClass('hide-to-show');
  $('#content').toggleClass('avoid-cover');
});

$('.popup_btn').click(function () {
    $(".error_msg").hide();
    $(".success_msg").hide();
    $(".popup_tip").show();
    var popupPanel = $(this).attr('href');
    $(popupPanel).css('display', 'block');
})
$('.popup_content a:contains("取消")').click(function(e){
    $(this).parents('.popup_background').css('display', 'none');
    e.preventDefault();
})
$('.closepop img').click(function (event) {
    $('.popup_background').css('display', 'none');
});

// $(".sidebar-menu > li:has(ul) > div > a").attr('href','###');
// $('.sidebar-menu > li').click(function(event) {
//   $(this).find('ul').slideDown().end().siblings().find('ul').slideUp();
// });

// $('.js-toggle-menu').click(function(e){
//   e.preventDefault();
//   $(this).toggleClass('open');
//   $('#sidebar').toggleClass('open');
//   $('#content').toggleClass('open');
// });

$('.label-tab-content li').hide();
$('.label-tab-btn li:nth-of-type(1) a').addClass('active');
$('.label-tab-content li:nth-of-type(1)').show();
$('.label-tab-btn li').click(function(event) {
  $(this).find('a').addClass('active');
  $(this).siblings().find('a').removeClass('active');
  let tab_target = $(this).find('a').attr('href');
  $(tab_target).slideDown();
  $(tab_target).siblings().hide();
  event.preventDefault();
});

var d = new Date();
var monthNames = ["1月", "2月", "3月", "4月", "5月", "6月", "7月", "8月", "9月", "10月", "11月", "12月"];
var weekday = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
var date = d.getDate();
var month = monthNames[d.getMonth()];
var week = weekday[d.getDay()];
var year = d.getFullYear();
function setDate() {
    $(".date").text(date + "日");
    $(".month").text(month);
    $(".week").text(week);
    $(".year").text(year + "年");
}
setDate();


var msec = 22220000;
var saveClock1;
function logOut() {
  console.log("logout");
  $(document).off('mousemove',resetClock);
  $('#logoutForm').submit();
}
function resetClock() {
  clearTimeout(saveClock1);
  saveClock1 = setTimeout(logOut, msec);
}
resetClock();
$(document).on('mousemove',resetClock);




function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function dateValidationCheck(str) {
  var re = new RegExp("^([0-9]{4})[./]{1}([0-9]{1,2})[./]{1}([0-9]{1,2})$");
  var strDataValue;
  var infoValidation = true;
  if ((strDataValue = re.exec(str)) != null) {
    var i;
    i = parseFloat(strDataValue[1]);
    if (i <= 0 || i > 9999) { /*年*/
      infoValidation = false;
    }
    i = parseFloat(strDataValue[2]);
    if (i <= 0 || i > 12) { /*月*/
      infoValidation = false;
    }
    i = parseFloat(strDataValue[3]);
    if (i <= 0 || i > 31) { /*日*/
      infoValidation = false;
    }
  } else {
    infoValidation = false;
  }
  return infoValidation;
}
