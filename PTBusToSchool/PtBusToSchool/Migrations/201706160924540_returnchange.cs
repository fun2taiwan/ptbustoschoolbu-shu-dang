namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class returnchange : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Timetables", "CarLineId", "dbo.CarLines");
            DropIndex("dbo.Timetables", new[] { "CarLineId" });
            AddColumn("dbo.Timetables", "Subject", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.Timetables", "CarLine", c => c.String());
            DropColumn("dbo.Timetables", "CarLineId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Timetables", "CarLineId", c => c.Int());
            DropColumn("dbo.Timetables", "CarLine");
            DropColumn("dbo.Timetables", "Subject");
            CreateIndex("dbo.Timetables", "CarLineId");
            AddForeignKey("dbo.Timetables", "CarLineId", "dbo.CarLines", "Id");
        }
    }
}
