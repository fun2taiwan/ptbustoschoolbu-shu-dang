namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addPassengersInSendCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendCars", "Passengers", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SendCars", "Passengers");
        }
    }
}
