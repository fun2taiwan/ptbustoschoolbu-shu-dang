﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Office2010.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{

    [PermissionFilters]

    public class StudentReservationController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;


        //
        // GET: /admin/StudentReservation/Edit/5

        //public ActionResult AddReservation(string Student, string StudentId, string StudentCardId, string ReservationCarLine, string Startstation, string Endstation, string ReservationTime)
        //{
        [HttpPost]
        public ActionResult AddReservation(StudentReservation studentreservation)
        {
            if (studentreservation.ReservationDate == null)
            {
                return Content("Error!沒有預約的日期");
            }
            if (studentreservation.ReservationDate >= DateTime.Today.Date.AddDays(14))
            {
                return Content("Error!你只能預約14天內的班次");
            }
           Reservationreset(studentreservation.ReservationDate);
            string[] startstationarr = studentreservation.Startstation.Split(':');
            string[] endstationarr = studentreservation.Endstation.Split(':');
            string[] studentreservationCarTimearr = studentreservation.ReservationTime.Split('#');
            if (Convert.ToInt16(startstationarr[0]) > Convert.ToInt16(endstationarr[0]))
            {
                return Content("Error!起站跟迄站順序有誤");
            }
            studentreservation.Startstation = startstationarr[1];
            studentreservation.Endstation = endstationarr[1];
            studentreservation.ReservationTime = studentreservationCarTimearr[1];
            studentreservation.ReservationCarNo = studentreservationCarTimearr[0];
            int CarLineId = Convert.ToInt16(studentreservation.ReservationCarLine);
            var carline = _db.CarLine.Find(CarLineId);


            var carlinesite = _db.CarLine.Where(x => x.ParentId == CarLineId).OrderBy(x => x.ListNum).ToList();
            string cutcarlinestr = "";
            int Q = Convert.ToInt16( startstationarr[0]);//起站的編號設為FOR的種子
            int t = Convert.ToInt16(endstationarr[0]);//迄站的編號設為FOR的結束
            for (int i = Q; i < t; i++)
            {
                cutcarlinestr += carlinesite[i].Subject + "→";
            }
         
            cutcarlinestr += "→";
            cutcarlinestr = cutcarlinestr.Replace("→→", "");
            studentreservation.CutCarLine = cutcarlinestr;


            ArrayList linesitepeople = new ArrayList();
            var sendCar = _db.SendCars.FirstOrDefault(x => x.StartDate == studentreservation.ReservationDate);
            var sendCarchild = _db.SendCars.FirstOrDefault(x => x.ParentId == sendCar.Id && x.CarLineId == CarLineId);
            var sendCargrandchild = _db.SendCars.FirstOrDefault(x => x.ParentId == sendCarchild.Id && x.CarNoTime.CarNo == studentreservation.ReservationCarNo);
            string[] peoplearr = sendCargrandchild.People.Split('、');
            int total = 0;
            for (int i = 0; i < peoplearr.Length; i++)
            {
                string[] updownnum = peoplearr[i].Split(':');
                total = total + Convert.ToInt16(updownnum[0]) - Convert.ToInt16(updownnum[1]);
                linesitepeople.Add(total);
            }
            string[] carlinearr = carline.Line.Split('→');
            string[] cutcarlinearr = cutcarlinestr.Split('→');
            string addnum = "";//用來存放需要對人數增加的站點
            for (int i = 0; i < cutcarlinearr.Length; i++)
            {
                for (int j = 0; j < carlinearr.Length; j++)
                {
                    if (cutcarlinearr[i] == carlinearr[j])
                    {
                        addnum += j;
                        if (i != cutcarlinearr.Length - 1)
                        {
                            addnum += "#";
                        }
                        break;
                    }

                }
            }
            string[] addnumarr = addnum.Split('#');
            for (int i = 0; i < linesitepeople.Count; i++)
            {
                for (int j = 0; j < addnumarr.Length; j++)
                {
                    int num = Convert.ToInt16(addnumarr[j]);
                    if (i == num)
                    {
                        if (Convert.ToInt16(linesitepeople[i]) >= carline.MaxPassengers)
                        {
                            
                            return Content("Error!預約人數已滿，如要候補請點選確認，換班次請選取消");
                        }
                    }
                }

            }
            if (ModelState.IsValid)
            {


                _db.StudentReservation.Add(studentreservation);
                studentreservation.Create(_db, _db.StudentReservation);
                return Content("Sucess!");
            }

            return Content("Error!資料不完全");
        }//預約


        [HttpPost]
        public ActionResult AddAlternateReservation(StudentReservation studentreservation)
        {
          
           // Reservationreset(studentreservation.ReservationDate);
            string[] startstationarr = studentreservation.Startstation.Split(':');
            string[] endstationarr = studentreservation.Endstation.Split(':');
            string[] studentreservationCarTimearr = studentreservation.ReservationTime.Split('#');
        
            studentreservation.Startstation = startstationarr[1];
            studentreservation.Endstation = endstationarr[1];
            studentreservation.ReservationTime = studentreservationCarTimearr[1];
            studentreservation.ReservationCarNo = studentreservationCarTimearr[0];
            int CarLineId = Convert.ToInt16(studentreservation.ReservationCarLine);
            var carline = _db.CarLine.Find(CarLineId);


            var carlinesite = _db.CarLine.Where(x => x.ParentId == CarLineId).OrderBy(x => x.ListNum).ToList();
            string cutcarlinestr = "";
            int Q = Convert.ToInt16(startstationarr[0]);//起站的編號設為FOR的種子
            int t = Convert.ToInt16(endstationarr[0]);//迄站的編號設為FOR的結束
            for (int i = Q; i < t; i++)
            {
                cutcarlinestr += carlinesite[i].Subject + "→";
            }
         
            cutcarlinestr += "→";
            cutcarlinestr = cutcarlinestr.Replace("→→", "");
            studentreservation.CutCarLine = cutcarlinestr;


            
            if (ModelState.IsValid)
            {

                //studentreservation.ReservationStatus = ReservationState.候補;
                _db.StudentReservation.Add(studentreservation);
                studentreservation.Create(_db, _db.StudentReservation);
                return Content("Sucess!");
            }

            return Content("Error!資料不完全");
        }//候補


        [HttpPost]
        private void Reservationreset(DateTime date)
        {
            SendCar sendCar = _db.SendCars.FirstOrDefault(x => x.StartDate == date);
	        var sendCarchild = _db.SendCars.Where(x => x.ParentId == sendCar.Id).ToList();
            var studentReservation =
                _db.StudentReservation.Where(
                    x => x.ReservationDate == date && x.ReservationStatus != ReservationState.取消預約);

	        foreach (var item in sendCarchild)
            {
                string[] getsitecount = item.LineName.Line.Split('→');
                var timeschedule = _db.TimeSchedule.Where(x => x.CarLineId == item.CarLineId && x.ParentId != null).ToList();
                string carlineid = item.CarLineId + "";
                string people = "";
                for (int j = 0; j < getsitecount.Length; j++)
                {
                    people = people + "0:0";
                    if (j != getsitecount.Length - 1)
                    {
                        people += "、";
                    }


                }
                
             
                foreach (var items in timeschedule)
                {

                    ArrayList addcolumn = new ArrayList();
                    ArrayList cutcolumn = new ArrayList();
                    for (int i = 0; i < getsitecount.Length; i++)
                    {
                        addcolumn.Add(0);
                        cutcolumn.Add(0);
                    }

                    foreach (var reservationitem in studentReservation.Where(x => x.ReservationCarNo == items.CarNo && x.ReservationCarLine == carlineid && x.ReservationStatus != ReservationState.候補))
                    {
                            for (int j = 0; j < getsitecount.Length; j++)
                            {
                                if (reservationitem.Startstation == getsitecount[j])
                                {
                                    addcolumn[j] = Convert.ToInt16(addcolumn[j]) + 1;

                                    break;
                                }

                            }
                        
                        for (int j = 0; j < getsitecount.Length; j++)
                        {
                            if (reservationitem.Endstation == getsitecount[j])
                            {
                                cutcolumn[j] = Convert.ToInt16(cutcolumn[j]) + 1;

                                break;
                            }

                        }
                        people = "";
                        for (int i = 0; i < addcolumn.Count; i++)
                        {
                            people += addcolumn[i] + ":" + cutcolumn[i];
                            if (i != addcolumn.Count - 1)
                            {
                                people += "、";
                            }
                        }

                    }
                   
                    var sendCargrandchild =
                        _db.SendCars.FirstOrDefault(
                            x => x.GrandParentId == sendCar.Id && x.ParentId == item.Id && x.TimeScheduleId == items.Id);
                    if (!string.IsNullOrEmpty(people))
                    {
                        sendCargrandchild.People = people;
                       
                        people = "";
                    }
                    else
                    {
                        for (int j = 0; j < getsitecount.Length; j++)
                        {
                            people = people + "0:0";
                            if (j != getsitecount.Length - 1)
                            {
                                people += "、";
                            }


                        }
                        sendCargrandchild.People = people;
                       
                    }


                        _db.SaveChanges();
                  }

	        }     
        }//計算預約每一站的人數(不含候補因為拿所有候補去比對能否變成正選)

        [HttpPost]
        public JsonResult GetCarline(DateTime date)
        {
            if (date != null)
            {

                var sendCars = _db.SendCars.FirstOrDefault(x => x.StartDate == date);
                var carline = _db.SendCars.Where(x => x.ParentId == sendCars.Id).Select(
                    c => new
                    {
                        CarLineId = c.CarLineId,
                        CarLineName = c.LineName.Subject

                    });
                return this.Json(carline.ToList());
            }
            return this.Json("Error");
        }//取得路線清單

        [HttpPost]
        public JsonResult GetSite(int? CarLineId)
        {


            if (CarLineId.HasValue)
            {
                var carline = _db.CarLine.Where(x => x.ParentId == CarLineId).OrderBy(x => x.ListNum).Select(
                    c => new
                    {
                        Listnum = c.ListNum - 1,
                        site = c.Subject
                    });
                return this.Json(carline.ToList());
            }
            return this.Json("Error");
        }//取得站點清單

        [HttpPost]
        public JsonResult GetTime(int? CarLineId, string Site)
        {
            if (!string.IsNullOrEmpty(Site) && CarLineId.HasValue)
            {
                var timeSchedule = _db.TimeSchedule.Where(x => x.CarLineId == CarLineId && x.Time != null).OrderBy(x => x.CarNo).ToList();
                string[] sitearr = Site.Split(':');
                int sitenum = Convert.ToInt16(sitearr[0]);
                List<CarTime> CarTimes = new List<CarTime>();

                foreach (var item in timeSchedule)
                {
                    string[] timearr = item.Time.Split('、');
                    CarTime carTime = new CarTime();
                    carTime.CarNo = item.CarNo;
                    carTime.Time = timearr[sitenum];
                    carTime.CarTimeStr = carTime.CarNo + "#" + carTime.Time;
                    CarTimes.Add(carTime);
                }
                return this.Json(CarTimes);
            }
            return this.Json("Error");
        }//取得時間選單

        [HttpPost]
        public JsonResult GetStudentReservation(string StudentId)
        {
            if (!string.IsNullOrEmpty(StudentId))
            {
                var studentReservationAll =
                    _db.StudentReservation.Where(x => x.StudentId == StudentId).OrderBy(x => x.ReservationDate).ToList();
                List<StudentReservationDetails> studentReservationDetails = new List<StudentReservationDetails>();

                foreach (var item in studentReservationAll)
                {

                    StudentReservationDetails studentReservation = new StudentReservationDetails();
                    studentReservation.Id = item.Id;
                    studentReservation.Student = item.Student;
                    studentReservation.StudentId = item.StudentId;
                    studentReservation.StudentCardId = item.StudentCardId;
                    int carlineid = Convert.ToInt16(item.ReservationCarLine);
                    var carline = _db.CarLine.Find(carlineid);
                    studentReservation.ReservationCarLine = carline.Subject;
                    studentReservation.Startstation = item.Startstation;
                    studentReservation.Endstation = item.Endstation;
                    studentReservation.ReservationTime = item.ReservationTime;
                    studentReservation.ReservationCarNo = item.ReservationCarNo;
                    studentReservation.ReservationStatus = item.ReservationStatus.ToString();
                    studentReservation.ReservationDate = item.ReservationDate.ToString("yyyy/MM/dd");
                    studentReservationDetails.Add(studentReservation);
                }


                return this.Json(studentReservationDetails);
            }
            return this.Json("Error");
        }//取得學生預約清單

        [HttpPost]
        public ActionResult CancelStudentReservation(int id)
        {
            StudentReservation studentreservation = _db.StudentReservation.Find(id);
            if (studentreservation.ReservationDate < DateTime.Today.Date)
            {
                return Content("Error!無法取消已消逝時間的預約");
            }
            if (studentreservation.ReservationStatus == ReservationState.預約成功)
            {
                return Content("Error!一旦預約成功則無法取消預約");
            }

            if (ModelState.IsValid)
            {
                studentreservation.ReservationStatus = ReservationState.取消預約;
                studentreservation.Update();
                //ChangeAlternateReservation(id);
                return Content("Sucess!");
            }


            return Content("Error");
        }//取消預約

        private void ChangeAlternateReservation(int id)
        {
            StudentReservation studentreservation = _db.StudentReservation.Find(id);
            Reservationreset(studentreservation.ReservationDate);
            var allstudentReservation =
                _db.StudentReservation.Where(
                    x =>
                        x.ReservationStatus == ReservationState.候補 &&
                        x.ReservationDate == studentreservation.ReservationDate &&
                        x.ReservationCarNo == studentreservation.ReservationCarNo &&
                        x.ReservationCarLine == studentreservation.ReservationCarLine).ToList();

            


        }



        [HttpPost]
        public ActionResult GetSucessFailureReservation(DateTime date)
        {

            JObject jObject = new JObject();

        
            JObject jLine = new JObject();
            JObject jFLine = new JObject();
            jObject.Add(new JProperty("日期", date.ToString("yyyy/MM/dd")));
            JObject jSucess = new JObject();
            var reservationEndGrand = _db.ReservationEnd.FirstOrDefault(x => x.StartDate == date);
            var reservationEndParents = _db.ReservationEnd.Where(x => x.ParentId == reservationEndGrand.Id).ToList();
            foreach (var item in reservationEndParents)
            {
                JObject jCarNo = new JObject();
                JObject jFCarNo = new JObject();
                var reservationEnds = _db.ReservationEnd.Where(x => x.GrandParentId == reservationEndGrand.Id && x.ParentId == item.Id).ToList();
                foreach (var items in reservationEnds)
                {
                    var carlinename = _db.CarLine.FirstOrDefault(x => x.Line == item.Line && x.Subject == item.LineName);
                    string carlineid = carlinename.Id + "";
                    string CarNoNum = items.CarNo+ "";
                    var studentReservationsucess =
                        _db.StudentReservation.Where(
                            x =>
                                x.ReservationDate == date && x.ReservationCarLine == carlineid &&
                                x.ReservationCarNo == CarNoNum&&x.ReservationStatus==ReservationState.預約成功).ToList();
                  
                  
                    JArray jstudentArray = new JArray();
                    int i = 0;
                    foreach (var student in studentReservationsucess)
                    {
                        i++;
                        JObject jstudent = new JObject();
                        jstudent.Add(new JProperty("姓名" ,student.Student));
                        jstudent.Add(new JProperty("學號", student.StudentId));
                        jstudent.Add(new JProperty("時間", student.ReservationTime));
                        jstudent.Add(new JProperty("起點", student.Startstation));
                        jstudent.Add(new JProperty("迄點", student.Endstation));
                  
                        jstudentArray.Add(jstudent);
                    }

                   
                    jCarNo.Add(new JProperty("班次" + items.CarNo, jstudentArray));


                 
                    var studentReservationFailure =
                       _db.StudentReservation.Where(
                           x =>
                               x.ReservationDate == date && x.ReservationCarLine == carlineid &&
                               x.ReservationCarNo == CarNoNum && x.ReservationStatus == ReservationState.候補).ToList();

                  
                    JArray jFstudentArray = new JArray();
                    int k = 0;
                    foreach (var student in studentReservationFailure)
                    {
                        k++;
                        JObject jstudent = new JObject();
                        jstudent.Add(new JProperty("姓名", student.Student));
                        jstudent.Add(new JProperty("學號", student.StudentId));
                        jstudent.Add(new JProperty("時間", student.ReservationTime));
                        jstudent.Add(new JProperty("起點", student.Startstation));
                        jstudent.Add(new JProperty("迄點", student.Endstation));
                       
                        jFstudentArray.Add(jstudent);
                    }





                    jFCarNo.Add(new JProperty("班次" + items.CarNo, jFstudentArray));

                }
                jLine.Add(new JProperty(item.LineName, jCarNo));
                jFLine.Add(new JProperty(item.LineName, jFCarNo));
            }
            JObject jFailure = new JObject();
            jSucess.Add(new JProperty("路線", jLine));
            jFailure.Add(new JProperty("路線", jFLine));
            jObject.Add(new JProperty("預約成功", jSucess));
            jObject.Add(new JProperty("候補", jFailure));

























            //JObject jHurt = new JObject();
            //jHurt.Add(new JProperty("hk", 1));
            //jHurt.Add(new JProperty("測試1", 2));
            //jObject.Add(new JProperty("路線", jHurt));

            string jsonContent;
            jsonContent = JsonConvert.SerializeObject(jObject, Formatting.Indented);

            return new ContentResult { Content = jsonContent, ContentType = "application/json" };
        }//取得預約成功失敗的推播資料

        [HttpPost]
        public ActionResult GetDistance_Google()
        {
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            string reply = wc.DownloadString("https://maps.googleapis.com/maps/api/distancematrix/json?origins=22.604929,120.300297&destinations=22.527509,120.345269&language=zh-TW&key=AIzaSyDG-qGExh_6vQBstQD9i1IFtlpQ1HdyxNI");
            JObject jObject = JObject.Parse(reply);
            JToken distance = jObject["rows"].First["elements"].First["distance"]["value"];
            JToken duration = jObject["rows"].First["elements"].First["duration"]["text"];
            string distancestr = distance.ToString();
            string durationstr = duration.ToString();
            durationstr = durationstr.Replace("分", "");
            double km = Math.Round(Convert.ToDouble(distancestr) / 1000, 1);
            int minute = Convert.ToInt16(durationstr);




            return Content(reply);
        }//算距離API


        public ActionResult Match(int id)
        {
            SendCar sendCar = _db.SendCars.Find(id);
            Deleteolddata(sendCar.StartDate.Value);
            ReservationEnd reservationEnd = new ReservationEnd();
            reservationEnd.StartDate = sendCar.StartDate;
            _db.ReservationEnd.Add(reservationEnd);
            _db.SaveChanges();
            var reservationEndGrandParents = _db.ReservationEnd.FirstOrDefault(x => x.StartDate == sendCar.StartDate);
            int reservationEndGrandParentsId = Convert.ToInt16(reservationEndGrandParents.Id);

            var sendcarsparents = _db.SendCars.Where(x => x.ParentId == id).ToList();

            foreach (var item in sendcarsparents)
            {
                reservationEnd = new ReservationEnd();
                reservationEnd.ParentId = reservationEndGrandParentsId;
                reservationEnd.LineName = item.LineName.Subject;
                reservationEnd.Line = item.LineName.Line;
                _db.ReservationEnd.Add(reservationEnd);
                _db.SaveChanges();
                var reservationEndParents = _db.ReservationEnd.FirstOrDefault(x => x.ParentId == reservationEndGrandParentsId && x.LineName == item.LineName.Subject);
                int reservationEndParentsId = Convert.ToInt16(reservationEndParents.Id);


                var sendcarschild = _db.SendCars.Where(x => x.ParentId == item.Id).ToList();
                foreach (var items in sendcarschild)
                {



                    reservationEnd = new ReservationEnd();
                    reservationEnd.ParentId = reservationEndParentsId;
                    reservationEnd.GrandParentId = reservationEndGrandParentsId;
                    int carno = Convert.ToInt16(items.CarNoTime.CarNo);
                    reservationEnd.CarNo = carno;
                    string carlineid = item.CarLineId + "";


                    var studentReservation = _db.StudentReservation.Where(x => x.ReservationDate == sendCar.StartDate && x.ReservationCarLine == carlineid && x.ReservationCarNo == items.CarNoTime.CarNo && x.ReservationStatus == ReservationState.預約成功);
                    string people = "";
                    string Timestr = "";
                    if (studentReservation != null)
                    {
                        var timeschedule = _db.TimeSchedule.FirstOrDefault(x => x.CarLineId == item.CarLineId && x.ParentId != null && x.CarNo == items.CarNoTime.CarNo);
                        string[] getsiteTime = items.CarNoTime.Time.Split('、');
                        string[] getsitecount = item.LineName.Line.Split('→');

                        ArrayList addcolumn = new ArrayList();
                        ArrayList cutcolumn = new ArrayList();
                        for (int k = 0; k < getsitecount.Length; k++)
                        {
                            addcolumn.Add(0);
                            cutcolumn.Add(0);
                        }


                        foreach (var studentReservationitem in studentReservation)
                        {
                            for (int j = 0; j < getsitecount.Length; j++)
                            {
                                if (studentReservationitem.Startstation == getsitecount[j])
                                {
                                    addcolumn[j] = Convert.ToInt16(addcolumn[j]) + 1;

                                    break;
                                }

                            }
                            for (int j = 0; j < getsitecount.Length; j++)
                            {
                                if (studentReservationitem.Endstation == getsitecount[j])
                                {
                                    cutcolumn[j] = Convert.ToInt16(cutcolumn[j]) + 1;

                                    break;
                                }

                            }

                            people = "";
                            Timestr = "";
                            for (int p = 0; p < addcolumn.Count; p++)
                            {
                                people += addcolumn[p] + ":" + cutcolumn[p];
                             
                                if (p != addcolumn.Count - 1)
                                {
                                    people += "、";

                                }




                            }


                        }
                        if (string.IsNullOrEmpty(people))
                        {
                            for (int j = 0; j < getsitecount.Length; j++)
                            {
                                people = people + "0:0";
                                if (j != getsitecount.Length - 1)
                                {
                                    people += "、";
                                }



                            }

                        }
                        reservationEnd.People = people;


                        string[] peoplearr = people.Split('、');
                        Boolean countYN = false;
                        int notimecount = 0;
                        for (int j = 0; j < peoplearr.Length; j++)
                        {
                            if (j == 0)
                            {
                                Timestr += getsiteTime[j];
                            }
                            else
                            {
                                if (countYN)
                                {
                                    if (peoplearr[j] == "0:0")
                                    {
                                        if (j == peoplearr.Length - 1)
                                        {
                                            _db = new BackendContext();
                                            string carsitebeforestr = getsitecount[j - 1 - notimecount];
                                            CarSite carsitebefore = _db.CarSite.FirstOrDefault(x => x.Subject == carsitebeforestr);
                                            _db = new BackendContext();
                                            carsitebeforestr = getsitecount[j];
                                            CarSite carsitenow = _db.CarSite.FirstOrDefault(x => x.Subject == carsitebeforestr);
                                            WebClient wc = new WebClient();
                                            wc.Encoding = Encoding.UTF8;
                                            string reply = wc.DownloadString("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + carsitebefore.Lat + "," + carsitebefore.Lon + "&destinations=" + carsitenow.Lat + "," + carsitenow.Lon + "&language=zh-TW&key=AIzaSyDG-qGExh_6vQBstQD9i1IFtlpQ1HdyxNI");
                                            JObject jObject = JObject.Parse(reply);
                                            JToken duration = jObject["rows"].First["elements"].First["duration"]["value"];
                                            double durationstr = Convert.ToDouble(duration.ToString())/60;
                                            durationstr = Math.Round(durationstr);
                                            int minute = Convert.ToInt16(durationstr);
                                            string[] getbeforetime = Timestr.Split('、');
                                            string beforetime = "";
                                            for (int k = 0; k < getbeforetime.Length; k++)
                                            {
                                                if (!string.IsNullOrEmpty(getbeforetime[k]))
                                                {
                                                    beforetime = getbeforetime[k];
                                                }
                                            }
                                            DateTime thissiteTime = Convert.ToDateTime(beforetime);
                                            thissiteTime = thissiteTime.AddMinutes(minute);
                                            Timestr += thissiteTime.Hour + ":" + thissiteTime.Minute + "";
                                        }
                                        else
                                        {
                                            Timestr += "";
                                            notimecount++;
                                        }
                                    }
                                    else
                                    {

                                        _db = new BackendContext();
                                        string carsitebeforestr = getsitecount[j - 1 - notimecount];
                                        CarSite carsitebefore = _db.CarSite.FirstOrDefault(x => x.Subject == carsitebeforestr);
                                        _db = new BackendContext();
                                        carsitebeforestr = getsitecount[j];
                                        CarSite carsitenow = _db.CarSite.FirstOrDefault(x => x.Subject == carsitebeforestr);
                                        WebClient wc = new WebClient();
                                        wc.Encoding = Encoding.UTF8;
                                        string reply = wc.DownloadString("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + carsitebefore.Lat + "," + carsitebefore.Lon + "&destinations=" + carsitenow.Lat + "," + carsitenow.Lon + "&language=zh-TW&key=AIzaSyDG-qGExh_6vQBstQD9i1IFtlpQ1HdyxNI");
                                        JObject jObject = JObject.Parse(reply);
                                        JToken duration = jObject["rows"].First["elements"].First["duration"]["text"];
                                        string durationstr = duration.ToString();
                                        durationstr = durationstr.Replace("分", "");
                                        int minute = Convert.ToInt16(durationstr);
                                        string[] getbeforetime = Timestr.Split('、');
                                        string beforetime = "";
                                        for (int k = 0; k < getbeforetime.Length; k++)
                                        {
                                            if (!string.IsNullOrEmpty(getbeforetime[k]))
                                            {
                                                beforetime = getbeforetime[k];
                                            }
                                        }
                                        DateTime thissiteTime = Convert.ToDateTime(beforetime);
                                        thissiteTime = thissiteTime.AddMinutes(minute);
                                        Timestr += thissiteTime.Hour + ":" + thissiteTime.Minute + "";

                                        if (notimecount != 0)
                                        {
                                            notimecount = 0;
                                        }

                                    }
                                }
                                else
                                {

                                    if (peoplearr[j] == "0:0")
                                    {
                                        if (j == peoplearr.Length - 1)
                                        {
                                            Timestr += getsiteTime[j];
                                        }
                                        else
                                        {
                                            countYN = true;
                                            Timestr += "";
                                            notimecount++;
                                        }

                                    }
                                    else
                                    {
                                        Timestr += getsiteTime[j];
                                    }
                                }

                            }

                            if (j != peoplearr.Length - 1)
                            {
                                Timestr += "、";
                            }


                        }










                        reservationEnd.CarTime = Timestr;

                        _db.ReservationEnd.Add(reservationEnd);
                        _db.SaveChanges();



                    }




                    var studentReservations =
                        _db.StudentReservation.Where(
                            x =>
                                x.ReservationDate == sendCar.StartDate && x.ReservationCarLine == carlineid &&
                                x.ReservationCarNo == items.CarNoTime.CarNo &&
                                x.ReservationStatus != ReservationState.取消預約);
                    int i = 0;//判定預約成功與否
                    foreach (var studentitem in studentReservations.OrderBy(x => x.Id))
                    {
                        i++;
                        if (i <= items.Passengers)
                        {
                            studentitem.ReservationStatus = ReservationState.預約成功;
                        }
                        else
                        {
                            studentitem.ReservationStatus = ReservationState.候補;
                        }
                        studentitem.Update();

                    }
















                }



            }






            return Content("");
        }//媒合

        private void Deleteolddata(DateTime Startdate)
        {
            ReservationEnd reservationEnd = _db.ReservationEnd.FirstOrDefault(x=>x.StartDate==Startdate);
            if (reservationEnd != null)
            {
                List<ReservationEnd> reservationEndchild = _db.ReservationEnd.Where(x => x.ParentId == reservationEnd.Id).ToList();
                List<ReservationEnd> reservationEndchildchild = _db.ReservationEnd.Where(x => x.GrandParentId == reservationEnd.Id).ToList();
                if (reservationEndchildchild.Count > 0)
                {
                    foreach (var item in reservationEndchildchild)
                    {
                        _db.ReservationEnd.Remove(item);
                    }
                    _db.SaveChanges();
                }
                if (reservationEndchild.Count > 0)
                {
                    foreach (var item in reservationEndchild)
                    {
                        _db.ReservationEnd.Remove(item);
                    }
                    _db.SaveChanges();
                }
                _db.ReservationEnd.Remove(reservationEnd);
                _db.SaveChanges();
            }
           


           
        }//重新媒合去刪除舊的媒合資料


        [Authorize]

        public ActionResult Index(int? page, FormCollection fc)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);


            var studentreservation = _db.StudentReservation.OrderByDescending(p => p.InitDate).AsQueryable();


            ViewBag.CarLine = _db.CarLine.Where(x => x.ParentId == null).ToList();
            if (hasViewData("SearchByReservationStatus"))
            {
                string ReservationStatus = getViewDateStr("SearchByReservationStatus");
                PtBusToSchool.Models.ReservationState searchByReservationStatus = (PtBusToSchool.Models.ReservationState)Enum.Parse(typeof(PtBusToSchool.Models.ReservationState), ReservationStatus, false);

                studentreservation = studentreservation.Where(w => w.ReservationStatus == searchByReservationStatus);
            }
            if (hasViewData("SearchByStartDate") && !hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate")).AddDays(1);
                studentreservation = studentreservation.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);
            }
            if (!hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);

                studentreservation = studentreservation.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);
            }
            if (hasViewData("SearchByStartDate") && hasViewData("SearchByEndDate"))
            {
                DateTime startDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                DateTime endDate = Convert.ToDateTime(getViewDateStr("SearchByEndDate")).AddDays(1);
                studentreservation = studentreservation.Where(w => w.InitDate >= startDate && w.InitDate <= endDate);
            }


            return View(studentreservation.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /admin/StudentReservations/Details/5
        [Authorize]
        public ActionResult Details(int id = 0)
        {
            StudentReservation studentreservation = _db.StudentReservation.Find(id);
            if (studentreservation == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(studentreservation);
        }



        //
        // POST: /admin/StudentReservations/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StudentReservation studentreservation = _db.StudentReservation.Find(id);
            _db.StudentReservation.Remove(studentreservation);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }





        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
