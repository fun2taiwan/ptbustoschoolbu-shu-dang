namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delSubjectAndCarLineInTimetable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Timetables", "Subject");
            DropColumn("dbo.Timetables", "CarLine");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Timetables", "CarLine", c => c.String());
            AddColumn("dbo.Timetables", "Subject", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
