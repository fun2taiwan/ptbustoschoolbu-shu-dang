namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarStyleInCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Cars", "CarStyles", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Cars", "CarStyles");
        }
    }
}
