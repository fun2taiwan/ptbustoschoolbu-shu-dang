namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addReservationDateInStudentReservation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.StudentReservations", "ReservationDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.StudentReservations", "ReservationDate");
        }
    }
}
