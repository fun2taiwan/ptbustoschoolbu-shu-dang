﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Newtonsoft.Json;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;


namespace PtBusToSchool.Areas.admin.Controllers
{
    public class HomeController : Controller
    {
        private BackendContext db = new BackendContext();
        //
        // GET: /Admin/Home/

        [PermissionFilters]
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [PermissionFilters]
        [Authorize]
        public ActionResult MyIndex()
        {

            return View();
        }

        /// <summary>
        /// 由內網管理後台登入的入口
        /// carl
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Login(string code)
        {
            BackendContext db = new BackendContext();
            if (!string.IsNullOrEmpty(code))
            {

                //carl 改成md5的編碼方式
                string userAccout = Utility.DecodingByMD5(code);

                Member member = db.Members.FirstOrDefault(d => d.Account == userAccout);
                if (member != null)
                {
                    Utility.GetPerssionGlobal(member);
                    string userData = JsonConvert.SerializeObject(member);
                    Utility.SetAuthenTicket(userData, member.Account);
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.message = "登入失敗!";
                return View();
            }
            ViewBag.message = "登入失敗!";
            return View();

        }

        private Member ValidateUser(string userName, string password)
        {
            Member member = db.Members.SingleOrDefault(o => o.Account == userName);
            if (member == null)
            {
                return null;
            }
            string saltPassword = Utility.GenerateHashWithSalt(password, member.PasswordSalt);
            return saltPassword == member.Password ? member : null;
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();

            return Redirect(FormsAuthentication.LoginUrl);


        }


        public ActionResult Error()
        {
            return View();
        }


    }
}
