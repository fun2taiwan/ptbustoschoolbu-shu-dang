namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addStudentReservation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StudentReservations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Student = c.String(nullable: false, maxLength: 50),
                        StudentId = c.String(),
                        StudentCardId = c.String(),
                        ReservationCarLine = c.String(),
                        Startstation = c.String(),
                        Endstation = c.String(),
                        ReservationTime = c.String(),
                        ReservationCarNo = c.String(),
                        ReservationStatus = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.StudentReservations");
        }
    }
}
