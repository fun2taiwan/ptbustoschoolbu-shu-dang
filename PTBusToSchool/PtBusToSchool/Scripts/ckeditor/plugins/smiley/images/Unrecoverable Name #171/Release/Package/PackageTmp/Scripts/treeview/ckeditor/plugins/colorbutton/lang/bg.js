ing the same semantics as specified.
            </summary>
            <param name = "format">The formatting string. </param>
            <param name = "arg0">The object to write into the formatted string. </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.WriteLine(System.String,System.Object,System.Object)">
            <summary>
                Writes out a formatted string, followed by a line terminator, using the same semantics as specified.
            </summary>
            <param name = "format">The formatting string to use. </param>
            <param name = "arg0">The first object to write into the formatted string. </param>
            <param name = "arg1">The second object to write into the formatted string. </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.WriteLine(System.String,System.Object[])">
            <summary>
                Writes out a formatted string, followed by a line terminator, using the same semantics as specified.
            </summary>
        