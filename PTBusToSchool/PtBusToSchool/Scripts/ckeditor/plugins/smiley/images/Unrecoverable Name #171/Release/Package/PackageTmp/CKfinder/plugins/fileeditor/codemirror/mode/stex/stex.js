﻿<?xml version="1.0" encoding="utf-8"?>
<doc>
  <assembly>
    <name>System.Web.Http</name>
  </assembly>
  <members>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.Exception)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> that represents an exception.</summary>
      <returns>The request must be associated with an <see cref="T:System.Web.Http.HttpConfiguration" /> instance.An <see cref="T:System.Net.Http.HttpResponseMessage" /> whose content is a serialized representation of an  <see cref="T:System.Web.Http.HttpError" /> instance.</returns>
      <param name="request">The HTTP request.</param>
      <param name="statusCode">The status code of the response.</param>
      <param name="exception">The exception.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.String)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> that represents an error message.</summary>
      <returns>The request must be associated with an <see cref="T:System.Web.Http.HttpConfiguration" /> instance.An <see cref="T:System.Net.Http.HttpResponseMessage" /> whose content is a serialized representation of an <see cref="T:System.Web.Http.HttpError" /> instance.</returns>
      <param name="request">The HTTP request.</param>
      <param name="statusCode">The status code of the response.</param>
      <param name="message">The error message.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.String,System.Exception)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> that represents an exception with an error message.</summary>
      <returns>The request must be associated with an <see cref="T:System.Web.Http.HttpConfiguration" /> instance.An <see cref="T:System.Net.Http.HttpResponseMessage" /> whose content is a serialized representation of an <see cref="T:System.Web.Http.HttpError" /> instance.</returns>
      <param name="request">The HTTP request.</param>
      <param name="statusCode">The status code of the response.</param>
      <param name="message">The error message.</param>
      <param name="exception">The exception.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.Web.Http.HttpError)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> that represents an error.</summary>
      <returns>The request must be associated with an <see cref="T:System.Web.Http.HttpConfiguration" /> instance.An <see cref="T:System.Net.Http.HttpResponseMessage" /> whose content is a serialized representation of an <see cref="T:System.Web.Http.HttpError" /> instance.</returns>
      <param name="request">The HTTP request.</param>
      <param name="statusCode">The status code of the response.</param>
      <param name="error">The HTTP error.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateErrorResponse(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,System.Web.Http.ModelBinding.ModelStateDictionary)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> that represents an error in the model state.</summary>
      <returns>The request must be associated with an <see cref="T:System.Web.Http.HttpConfiguration" /> instance.An <see cref="T:System.Net.Http.HttpResponseMessage" /> whose content is a serialized representation of an <see cref="T:System.Web.Http.HttpError" /> instance.</returns>
      <param name="request">The HTTP request.</param>
      <param name="statusCode">The status code of the response.</param>
      <param name="modelState">The model state.</param>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateResponse``1(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,``0)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <typeparam name="T">The type of the HTTP response message.</typeparam>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateResponse``1(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,``0,System.Net.Http.Formatting.MediaTypeFormatter)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <param name="formatter">The media type formatter.</param>
      <typeparam name="T">The type of the HTTP response message.</typeparam>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateResponse``1(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,``0,System.Net.Http.Formatting.MediaTypeFormatter,System.Net.Http.Headers.MediaTypeHeaderValue)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <param name="formatter">The media type formatter.</param>
      <param name="mediaType">The media type header value.</param>
      <typeparam name="T">The type of the HTTP response message.</typeparam>
    </member>
    <member name="M:System.Net.Http.HttpRequestMessageExtensions.CreateResponse``1(System.Net.Http.HttpRequestMessage,System.Net.HttpStatusCode,``0,System.Net.Http.Formatting.MediaTypeFormatter,System.String)">
      <summary>Creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>An initialized <see cref="T:System.Net.Http.HttpResponseMessage" /> wired up to the associated <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
      <param name="request">The HTTP request message which led to this response message.</param>
      <param name="statusCode">The HTTP response status code.</param>
      <param name="value">The content of the HTTP response message.</param>
      <param name="formatter">The media type formatter.</param>
      <param name="mediaType">The media 