namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTimetableForeignKeyCarLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timetables", "CarLineId", c => c.Int());
            AddForeignKey("dbo.Timetables", "CarLineId", "dbo.CarLines", "Id");
            CreateIndex("dbo.Timetables", "CarLineId");
            DropColumn("dbo.Timetables", "CarLine");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Timetables", "CarLine", c => c.String());
            DropIndex("dbo.Timetables", new[] { "CarLineId" });
            DropForeignKey("dbo.Timetables", "CarLineId", "dbo.CarLines");
            DropColumn("dbo.Timetables", "CarLineId");
        }
    }
}
