﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class ReservationController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var reservation = _db.Reservation.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                reservation = reservation.Where(w => w.Subject.Contains(searchBySubject)); 
            } 
 
            if (hasViewData("SearchByReservationSorF")) 
            { 
            string ReservationSorF = getViewDateStr("SearchByReservationSorF");             
             PtBusToSchool.Models.ReservationState searchByReservationSorF= (PtBusToSchool.Models.ReservationState)Enum.Parse(typeof(PtBusToSchool.Models.ReservationState), ReservationSorF, false); 
             
                reservation = reservation.Where(w => w.ReservationSorF == searchByReservationSorF); 
            } 
 

//            ViewBag.Subject = Subject;
            return View(reservation.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /admin/Reservation/Details/5

        public ActionResult Details(int id = 0)
        {
            Reservation reservation = _db.Reservation.Find(id);
            if (reservation == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(reservation);
        }
        [HttpPost]
        public JsonResult GetStartAndEndStation(string CarlineId)
        {
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();

            if (!string.IsNullOrWhiteSpace(CarlineId))
            {
                int Id = Convert.ToInt16(CarlineId);
                var startstation = _db.Timetable.Find(Id);

                string[] stationarr = startstation.CarLine.Split('→');
                for (int i = 0; i < stationarr.Length; i++)
                {
                    items.Add(new KeyValuePair<string, string>(
                             i + ":" + stationarr[i],
                             string.Format("{0}", stationarr[i])));

                }


            }
            return this.Json(items);
            //return this.Json("ddd");
        }
        [HttpPost]
        public JsonResult GetCarTime(string CarlineId, string TimeNum)
        {
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();

            if (!string.IsNullOrWhiteSpace(CarlineId))
            {
                int Id = Convert.ToInt16(CarlineId);
                var carNo = _db.Timetable.Where(x=>x.ParentId==Id);
                int Timearrnum = Convert.ToInt16(TimeNum.Substring(0,TimeNum.IndexOf(":")));
                
                foreach (var item in carNo)
                {
                    string[] timearr = item.Time.Split('、');
                    items.Add(new KeyValuePair<string, string>(
                             item.CarNo.ToString() + "#" + timearr[Timearrnum],
                              string.Format("{0}", timearr[Timearrnum])));
                    
                }
            


            }
            return this.Json(items);
        }
        //private IEnumerable<Timetable> GetStartAndEnd(string CarlineId)
        //{
        //    int parentId = Convert.ToInt16(CarlineId);
        //    var query = _db.Timetable.Where(x => x.ParentId == parentId).OrderBy(x => x.InitDate);
        //    return query.ToList();

        //}

        //
        // GET: /admin/Reservation/Create

        public ActionResult Create()
        {
            ViewBag.ReservationCarLine = new SelectList(_db.Timetable.Where(x => x.ParentId == null), "Id", "Subject");
            return View();
        }

        //
        // POST: /admin/Reservation/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Reservation reservation )
        {
            
            string[] startstationarr = reservation.Startstation.Split(':');
            string[] endstationarr = reservation.Endstation.Split(':');
            string[] reservationCarTimearr = reservation.ReservationCarTime.Split('#');
            if (Convert.ToInt16(startstationarr[0]) > Convert.ToInt16(endstationarr[0]))
            {
                ViewBag.Carline = new SelectList(_db.Timetable.Where(x => x.ParentId == null), "Id", "Subject");
                return View(reservation);
            }
            reservation.Startstation = startstationarr[1];
            reservation.Endstation = endstationarr[1];
            reservation.ReservationCarTime = reservationCarTimearr[1];
            reservation.ReservationCarNo = reservationCarTimearr[0];
            int CarLineId = Convert.ToInt16(reservation.ReservationCarLine);
            var carline = _db.Timetable.Find(CarLineId);
            //reservation.ReservationCarLine = carline.Subject;
            if (ModelState.IsValid)
            {







                _db.Reservation.Add(reservation);
                reservation.Create(_db, _db.Reservation);
               return RedirectToAction("Index");
            }

            return View(reservation);
        }

        //
        // GET: /admin/Reservation/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Reservation reservation = _db.Reservation.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        //
        // POST: /admin/Reservation/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Reservation reservation)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(reservation).State = EntityState.Modified;
                reservation.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(reservation);
        }

        //
        // GET: /admin/Reservation/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Reservation reservation = _db.Reservation.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            return View(reservation);
        }

        //
        // POST: /admin/Reservation/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservation reservation = _db.Reservation.Find(id);
            _db.Reservation.Remove(reservation);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
