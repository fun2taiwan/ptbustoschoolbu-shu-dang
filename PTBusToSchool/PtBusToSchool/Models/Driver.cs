﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class Driver:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "駕駛人姓名")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [Display(Name = "員工編號")]
        public string StaffNo { get; set; }


        public virtual ICollection<Car> CarAndDriver { get; set; }
    }
}