    </member>
    <member name="P:System.Web.Http.HttpConfiguration.Formatters">
      <summary>Gets the media-type formatters for this instance.</summary>
      <returns>A collection of <see cref="T:System.Net.Http.Formatting.MediaTypeFormatter" /> objects.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.IncludeErrorDetailPolicy">
      <summary>Gets or sets a value indicating whether error details should be included in error messages.</summary>
      <returns>The <see cref="T:System.Web.Http.IncludeErrorDetailPolicy" /> value that indicates that error detail policy.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.Initializer">
      <summary> Gets or sets the action that will perform final initialization of the <see cref="T:System.Web.Http.HttpConfiguration" /> instance before it is used to process requests. </summary>
      <returns>The action that will perform final initialization of the <see cref="T:System.Web.Http.HttpConfiguration" /> instance.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.MessageHandlers">
      <summary>Gets an ordered list of <see cref="T:System.Net.Http.DelegatingHandler" /> instances to be invoked as an <see cref="T:System.Net.Http.HttpRequestMessage" /> travels up the stack and an <see cref="T:System.Net.Http.HttpResponseMessage" /> travels down in stack in return. </summary>
      <returns>The message handler collection.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.ParameterBindingRules">
      <summary>The collection of rules for how parameters should be bound.</summary>
      <returns>A collection of functions that can produce a parameter binding for a given parameter.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.Properties">
      <summary>Gets the properties associated with this instance.</summary>
      <returns>The <see cref="T:System.Collections.Concurrent.ConcurrentDictionary`2" />that contains the properties.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.Routes">
      <summary>Gets the <see cref="T:System.Web.Http.HttpRouteCollection" /> associated with this <see cref="T:System.Web.Http.HttpConfiguration" /> instance.</summary>
      <returns>The <see cref="T:System.Web.Http.HttpRouteCollection" />.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.Services">
      <summary>Gets the container of default services associated with this instance.</summary>
      <returns>The <see cref="T:System.Web.Http.Controllers.ServicesContainer" /> that contains the default services for this instance.</returns>
    </member>
    <member name="P:System.Web.Http.HttpConfiguration.VirtualPathRoot">
      <summary>Gets the root virtual path.</summary>
      <returns>The root virtual path.</returns>
    </member>
    <member name="T:System.Web.Http.HttpConfigurationExtensions">
      <summary>Contains extension methods for the <see cref="T:System.Web.Http.HttpConfiguration" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.HttpConfigurationExtensions.BindParameter(System.Web.Http.HttpConfiguration,System.Type,System.Web.Http.ModelBinding.IModelBinder)">
      <summary> Register that the given parameter type on an Action is to be bound using the model binder. </summary>
      <param name="configuration">configuration to be updated.</param>
      <param name="type">parameter type that binder is applied to</param>
      <param name="binder">a model binder</param>
    </member>
    <member name="T:System.Web.Http.HttpDeleteAttribute">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.HttpDeleteAttribute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpDeleteAttribute" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.HttpDeleteAttribute.HttpMethods">
      <summary>Gets a collection of HTTP methods.</summary>
      <returns>A collection of HTTP methods.</returns>
    </member>
    <member name="T:System.Web.Http.HttpError">
      <summary> Defines a serializable container for arbitrary error information. </summary>
    </member>
    <member name="M:System.Web.Http.HttpError.#ctor">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.HttpError" /> class. </summary>
    </member>
    <member name="M:System.Web.Http.HttpError.#ctor(System.Exception,System.Boolean)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.HttpError" /> class for exception. </summary>
      <param name="exception">The exception to use for error information.</param>
      <param name="includeErrorDetail">  true to include the exception information in the error; false otherwise</param>
    </member>
    <member name="M:System.Web.Http.HttpError.#ctor(System.String)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.HttpError" /> class containing error message message. </summary>
      <param name="message">The error message to associate with this instance.</param>
    </member>
    <member name="M:System.Web.Http.HttpError.#ctor(System.Web.Http.ModelBinding.ModelStateDictionary,System.Boolean)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.HttpError" /> class for modelState. </summary>
      <param name="modelState">The invalid model state to use for error information.</param>
      <param name="includeErrorDetail">  true to include exception messages in the error; false otherwise</param>
    </member>
    <member name="P:System.Web.Http.HttpError.Message">
      <summary> The error message associated with this instance. </summary>
    </member>
    <member name="M:System.Web.Http.HttpError.System#Xml#Serialization#IXmlSerial