ta.Entity.Migrations.Model.AddColumnOperation">
            <summary>
                Represents a column being added to a table.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Migrations.Model.MigrationOperation">
            <summary>
                Represents an operation to modify a database schema.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Model.MigrationOperation.#ctor(System.Object)">
            <summary>
                Initializes a new instance of the MigrationOperation class.
            </summary>
            <param name = "anonymousArguments">
             
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.MigrationOperation.AnonymousA