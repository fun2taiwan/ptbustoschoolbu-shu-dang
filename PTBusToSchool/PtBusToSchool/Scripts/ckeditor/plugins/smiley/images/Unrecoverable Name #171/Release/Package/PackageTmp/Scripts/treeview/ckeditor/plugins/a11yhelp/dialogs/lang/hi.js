d, or the source query if NoTracking is not supported.</returns>
        </member>
        <member name="M:System.Data.Entity.DbExtensions.AsNoTracking(System.Linq.IQueryable)">
            <summary>
                Returns a new query where the entities returned will not be cached in the <see cref="T:System.Data.Entity.DbContext"/>
                or <see cref="T:System.Data.Objects.ObjectContext"/>.  This method works by calling the AsNoTracking method of the
                underlying query object.  If the underlying query object does not have a AsNoTracking method,
                then calling this method will have no affect.
            </summary>
            <param name="source">The source query.</param>
            <returns>A new query with NoTracking applied, or the source query if NoTracking is not supported.</returns>
        </member>
        <member name="M:System.Data.Entity.DbExtensions.CommonAsNoTracking``1(``0)">
            <summary>
                Common code for generic and non-generic AsNoTracking.
            </summary>
        </member>
        <member name="M:System.Data.Entity.DbExtensions.Load(System.Linq.IQueryable)">
            <summary>
                Enumerates the query such that for server queries such as those of <see cref="T:System.Data.Entity.DbSet`1"/>, <see cref="T:System.Data.Objects.ObjectSet`1"/>,
                <see cref="T:System.Data.Objects.ObjectQuery`1"/>, and others the results of the query will be loaded into the associated <see cref="T:System.Data.Entity.DbContext"/>,
                <see cref="T:System.Data.Objects.ObjectContext"/> or other cache on the client.
                This is equivalent to calling ToList and then throwing away the list without the overhead of actually creating the list.
            </summary>
            <param name="source">The source query.</param>
        </member>
        <member name="M:System.Data.Entity.DbExtensions.ToBindingList``1(System.Collections.ObjectModel.ObservableCollection{``0})">
            <summary>
                Returns an <see cref="T:System.ComponentModel.BindingList`1"/> implementation that stays in sync with the given <see cref="T:System.Collections.ObjectModel.ObservableCollection`1"/>.
            </summary>
            <typeparam name="T">The element type.</typeparam>
            <param name="source">The collection that the binding list will stay in sync with.</param>
            <returns>The binding list.</returns>
        </member>
        <member name="T:System.Data.Entity.DbModelBuilder">
            <summary>
                DbModelBuilder is used to map CLR classes to a database schema.
                This code centric approach to building an Entity Data Model (EDM) model is known as 'Code First'.
            </summary>
            <remarks>
                DbModelBuilder is typically used to configure a model by overriding <see cref="M:System.Data.Entity.DbContext.OnModelCreating(System.Data.Entity.DbModelBuilder)"/>. 
                You can also use DbModelBuilder independently of DbContext to build a model and then construct a 
                <see cref="T:System.Data.Entity.DbContext"/> or <see cref="T:System.Data.Objects.ObjectContext"/>.
                The recommended approach, however, is to use OnModelCreating in <see cref="T:System.Data.Entity.DbContext"/> as
                the workflow is more intuitive and takes care of common tasks, such as caching the created model.
            
                Types that form your model are registered with DbModelBuilder and optional configuration can be
                performed by applying data annotations to your classes and/or using the fluent style DbModelBuilder
                API. 
            
                When the Build method is called a set of conventions are run to discover the initial model.
                These conventions will automatically discover aspects of the model, such a