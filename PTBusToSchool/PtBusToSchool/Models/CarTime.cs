﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PtBusToSchool.Models
{
    public class CarTime
    {
        [Display(Name = "班次")]
        public string CarNo { get; set; }

        [Display(Name = "時間")]
        public string Time { get; set; }

        [Display(Name = "回傳值")]
        public string CarTimeStr { get; set; }
    }
}