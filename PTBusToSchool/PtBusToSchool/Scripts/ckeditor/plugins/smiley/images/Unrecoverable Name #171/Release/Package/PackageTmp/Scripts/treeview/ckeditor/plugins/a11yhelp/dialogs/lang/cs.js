er>
    <member name="M:System.Web.Http.Routing.HttpRoute.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRoute" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.#ctor(System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRoute" /> class.</summary>
      <param name="routeTemplate">The route template.</param>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.#ctor(System.String,System.Web.Http.Routing.HttpRouteValueDictionary)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRoute" /> class.</summary>
      <param name="routeTemplate">The route template.</param>
      <param name="defaults">The default values for the route parameters.</param>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.#ctor(System.String,System.Web.Http.Routing.HttpRouteValueDictionary,System.Web.Http.Routing.HttpRouteValueDictionary)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRoute" /> class.</summary>
      <param name="routeTemplate">The route template.</param>
      <param name="defaults">The default values for the route parameters.</param>
      <param name="constraints">The constraints for the route parameters.</param>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.#ctor(System.String,System.Web.Http.Routing.HttpRouteValueDictionary,System.Web.Http.Routing.HttpRouteValueDictionary,System.Web.Http.Routing.HttpRouteValueDictionary)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRoute" /> class.</summary>
      <param name="routeTemplate">The route template.</param>
      <param name="defaults">The default values for the route parameters.</param>
      <param name="constraints">The constraints for the route parameters.</param>
      <param name="dataTokens">Any additional tokens for the route parameters.</param>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.#ctor(System.String,System.Web.Http.Routing.HttpRouteValueDictionary,System.Web.Http.Routing.HttpRouteValueDictionary,System.Web.Http.Routing.HttpRouteValueDictionary,System.Net.Http.HttpMessageHandler)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Routing.HttpRoute" /> class.</summary>
      <param name="routeTemplate">The route template.</param>
      <param name="defaults">The default values for the route parameters.</param>
      <param name="constraints">The constraints for the route parameters.</param>
      <param name="dataTokens">Any additional tokens for the route parameters.</param>
      <param name="handler">The message handler that will be the recipient of the request.</param>
    </member>
    <member name="P:System.Web.Http.Routing.HttpRoute.Constraints">
      <summary>Gets the constraints for the route parameters.</summary>
      <returns>The constraints for the route parameters.</returns>
    </member>
    <member name="P:System.Web.Http.Routing.HttpRoute.DataTokens">
      <summary>Gets any additional data tokens not used directly to determine whether a route matches an incoming <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>Any additional data tokens not used directly to determine whether a route matches an incoming <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
    </member>
    <member name="P:System.Web.Http.Routing.HttpRoute.Defaults">
      <summary>Gets the default values for route parameters if not provided by the incoming <see cref="T:System.Net.Http.HttpRequestMessage" />.</summary>
      <returns>The default values for route parameters if not provided by the incoming <see cref="T:System.Net.Http.HttpRequestMessage" />.</returns>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.GetRouteData(System.String,System.Net.Http.HttpRequestMessage)">
      <summary>Determines whether this route is a match for the incoming request by looking up the <see cref="T:System.Web.Http.Routing.HttpRouteData" /> for the route.</summary>
      <returns>The <see cref="T:System.Web.Http.Routing.HttpRouteData" /> for a route if matches; otherwise null.</returns>
      <param name="virtualPathRoot">The virtual path root.</param>
      <param name="request">The HTTP request.</param>
    </member>
    <member name="M:System.Web.Http.Routing.HttpRoute.GetVirtualPath(System.Net.Http.HttpRequestMessage,System.Collections.Generic.IDictionary{System.String,System.Object})">
      <summary>Attempts to generate a URI that represents the values passed in based on current values from the <see cref="T:System.Web.Htt