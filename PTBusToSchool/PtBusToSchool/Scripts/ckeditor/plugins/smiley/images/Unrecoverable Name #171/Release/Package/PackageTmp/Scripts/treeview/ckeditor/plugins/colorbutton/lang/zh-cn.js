y>
                Gets or sets the currently assigned name.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmDataModelType.IsAbstract">
            <summary>
                Gets a value indicating whether this type is abstract.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Edm.EdmDataModelType.BaseType">
            <summary>
                Gets the optional base type of this type.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.EdmStructuralMember">
            <summary>
                EdmStructuralMember is the base for all types that represent members of structural items in the Entity Data Model (EDM) metadata construction and modification API.
        