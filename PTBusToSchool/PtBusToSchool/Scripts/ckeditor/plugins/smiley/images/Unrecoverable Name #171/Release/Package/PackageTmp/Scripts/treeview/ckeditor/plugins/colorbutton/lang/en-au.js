ram>
            <param name = "anonymousArguments">
                Additional arguments that may be processed by providers. 
                Use anonymous type syntax to specify arguments e.g. 'new { SampleArgument = "MyValue" }'.
            </param>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.SqlOperation.Sql">
            <summary>
                Gets the SQL to be executed.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Model.SqlOperation.SuppressTransaction">
            <summary>
                Gets or sets a value indicating whether this statement should be performed outside of
                the transaction scope that is used to make the migration process transactional.
                If set to true, this operation will not be rolled back if the migration process fails.
            