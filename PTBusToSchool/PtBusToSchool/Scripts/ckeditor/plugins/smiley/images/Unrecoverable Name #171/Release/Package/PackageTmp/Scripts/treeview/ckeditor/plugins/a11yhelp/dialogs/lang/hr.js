Filters.HttpActionExecutedContext.Response">
      <summary>Gets or sets the <see cref="T:System.Net.Http.HttpResponseMessage" /> for the context.</summary>
      <returns>The <see cref="T:System.Net.Http.HttpResponseMessage" /> for the context.</returns>
    </member>
    <member name="T:System.Web.Http.Filters.HttpFilterCollection">
      <summary>Represents a collection of HTTP filters.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Filters.HttpFilterCollection" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.Add(System.Web.Http.Filters.IFilter)">
      <summary>Adds an item at the end of the collection.</summary>
      <param name="filter">The item to add to the collection.</param>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.Clear">
      <summary>Removes all item in the collection.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.Contains(System.Web.Http.Filters.IFilter)">
      <summary>Determines whether the collection contains the specified item.</summary>
      <returns>true if the collection contains the specified item; otherwise, false.</returns>
      <param name="filter">The item to check.</param>
    </member>
    <member name="P:System.Web.Http.Filters.HttpFilterCollection.Count">
      <summary>Gets the number of elements in the collection.</summary>
      <returns>The number of elements in the collection.</returns>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.GetEnumerator">
      <summary>Gets an enumerator that iterates through the collection.</summary>
      <returns>An enumerator object that can be used to iterate through the collection.</returns>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.Remove(System.Web.Http.Filters.IFilter)">
      <summary>Removes the specified item from the collection.</summary>
      <param name="filter">The item to remove in the collection.</param>
    </member>
    <member name="M:System.Web.Http.Filters.HttpFilterCollection.System#Collections#IEnumerable#GetEnumerator">
      <summary>Gets an enumerator that iterates through the collection.</summary>
      <returns>An enumerator object that can be used to iterate through the collection.</returns>
    </member>
    <member name="T:System.Web.Http.Filters.IActionFilter">
      <summary>Defines the methods that are used in an action filter.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.IActionFilter.ExecuteActionFilterAsync(System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken,System.Func{System.Threading.Tasks.Task{System.Net.Http.HttpResponseMessage}})">
      <summary>Executes the filter action asynchronously.</summary>
      <returns>The newly created task for this operation.</returns>
      <param name="actionContext">The action context.</param>
      <param name="cancellationToken">The cancellation token assigned for this task.</param>
      <param name="continuation">The delegate function to continue after the action method is invoked.</param>
    </member>
    <member name="T:System.Web.Http.Filters.IAuthorizationFilter">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.Filters.IAuthorizationFilter.ExecuteAuthorizationFilterAsync(System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken,System.Func{System.Threading.Tasks.Task{System.Net.Http.HttpResponseMessage}})">
      <summary>Executes the authorization filter to synchronize.</summary>
      <returns>The authorization filter to synchronize.</returns>
      <param name="actionContext">The action context.</param>
      <param name="cancellationToken">The cancellation token associated with the filter.</param>
      <param name="continuation">The continuation.</param>
    </member>
    <m