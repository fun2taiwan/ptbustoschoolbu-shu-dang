// <auto-generated />
namespace PtBusToSchool.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class delTimetableForeignKeyCarLine : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(delTimetableForeignKeyCarLine));
        
        string IMigrationMetadata.Id
        {
            get { return "201706150208052_delTimetableForeignKeyCarLine"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
