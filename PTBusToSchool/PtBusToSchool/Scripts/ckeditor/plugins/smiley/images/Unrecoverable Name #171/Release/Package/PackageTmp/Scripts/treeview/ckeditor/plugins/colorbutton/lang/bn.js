ntity.Migrations.Utilities.IndentedTextWriter.WriteLine(System.String)">
            <summary>
                Writes the specified string, followed by a line terminator, to the text stream.
            </summary>
            <param name = "s">The string to write. </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.WriteLine">
            <summary>
                Writes a line terminator.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.WriteLine(System.Boolean)">
            <summary>
                Writes the text representation of a Boolean, followed by a line terminator, to the text stream.
            </summary>
            <param name = "value">The Boolean to write. </param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Utilities.IndentedTextWriter.WriteLine(System.C