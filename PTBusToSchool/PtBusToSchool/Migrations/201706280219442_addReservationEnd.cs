namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addReservationEnd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReservationEnds",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(),
                        GrandParentId = c.Int(),
                        StartDate = c.DateTime(),
                        LineName = c.String(),
                        CarNo = c.Int(nullable: false),
                        CarTime = c.String(),
                        People = c.String(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ReservationEnds", t => t.ParentId)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ReservationEnds", new[] { "ParentId" });
            DropForeignKey("dbo.ReservationEnds", "ParentId", "dbo.ReservationEnds");
            DropTable("dbo.ReservationEnds");
        }
    }
}
