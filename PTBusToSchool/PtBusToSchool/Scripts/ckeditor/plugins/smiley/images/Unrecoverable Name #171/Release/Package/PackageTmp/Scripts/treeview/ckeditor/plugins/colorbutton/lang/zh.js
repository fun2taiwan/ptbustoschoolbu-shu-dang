<?xml version="1.0"?>
<doc>
    <assembly>
        <name>EntityFramework</name>
    </assembly>
    <members>
        <member name="T:System.Data.Entity.Edm.EdmDataModelType">
            <summary>
                The base for all all Entity Data Model (EDM) types that represent a type from the EDM type system.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.EdmNamespaceItem">
            <summary>
                Represents an item in an Entity Data Model (EDM) <see cref="T:System.Data.Entity.Edm.EdmNamespace"/> .
            </summary>
        </member>
        <member name="T:System.Data.Entity.Edm.EdmQualifiedNameMetadataItem">
            <summary>
                The base for all all Entity Data Model (EDM) item types that with a Name property that represents a qualified (can be dotted) name.
            </summary>
        