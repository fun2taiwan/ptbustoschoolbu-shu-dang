﻿using System;
using System.Data.Entity;
using System.Web;

namespace PtBusToSchool.Models
{
    public class DatabaseInitializer : CreateDatabaseIfNotExists<BackendContext>
    {
        protected override void Seed(BackendContext context)
        {
            var unit = new Unit { Subject = "屏東客運", Alias = "屏客", Poster = "admin", InitDate = DateTime.Now };
            var member = new Member { Account = "admin", Password = "/oIZh6cwrV5BIca+i8zTXNbS2aR89H+7az9pdWFMDvo=", PasswordSalt = "NqYBB/o=", Name = "總管理者", Gender = GenderType.男, Email = "admin@gmail.com", JobTitle = "總管理者", MyUnit = unit, Permission = "A,A01,A02",Poster = "admin",InitDate = DateTime.Now};
            context.Units.Add(unit);
            context.Members.Add(member);
            context.SaveChanges();

            //執行預存程序
            string createMemberProcedure =System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Config/CreateMember.sql")) ;
            context.Database.ExecuteSqlCommand(createMemberProcedure);
        }
    }
}