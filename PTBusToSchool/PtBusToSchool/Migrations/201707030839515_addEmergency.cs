namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addEmergency : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Emergencies",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IncidentDate = c.DateTime(nullable: false),
                        Line = c.String(),
                        CarNo = c.String(),
                        Momo = c.String(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Emergencies");
        }
    }
}
