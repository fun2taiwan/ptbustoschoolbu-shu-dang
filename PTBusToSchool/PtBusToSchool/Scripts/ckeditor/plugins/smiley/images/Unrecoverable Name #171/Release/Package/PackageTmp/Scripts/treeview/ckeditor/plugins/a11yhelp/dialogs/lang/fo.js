. Finally
                any configuration that was performed using the DbModelBuilder API is applied. 
            
                Configuration done via the DbModelBuilder API takes precedence over data annotations which 
                in turn take precedence over the default conventions.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.#ctor">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.DbModelBuilder"/> class. 
            
                The process of discovering the initial model will use the set of conventions included 
                in the most recent version of the Entity Framework installed on your machine.
            </summary>
            <remarks>
                Upgrading to newer versions of the Entity Framework may cause breaking changes 
                in your application because new conventions may cause the initial model to be 
                configured differently. There is an alternate constructor that allows a specific 
                version of conventions to be specified.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.#ctor(System.Data.Entity.DbModelBuilderVersion)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.DbModelBuilder"/> class that will use 
                a specific set of conventions to discover the initial model.
            </summary>
            <param name="modelBuilderVersion">The version of conventions to be used.</param>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.Ignore``1">
            <summary>
                Excludes a type from the model. This is used to remove types from the model that were added 
                by convention during initial model discovery.
            </summary>
            <typeparam name = "T">The type to be excluded.</typeparam>
            <returns>The same DbModelBuilder instance so that multiple calls can be chained.</returns>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.Ignore(System.Collections.Generic.IEnumerable{System.Type})">
            <summary>
                Excludes a type(s) from the model. This is used to remove types from the model that were added 
                by convention during initial model discovery.
            </summary>
            <param name = "types">The types to be excluded from the model.</param>
            <returns>The same DbModelBuilder instance so that multiple calls can be chained.</returns>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.Entity``1">
            <summary>
                Registers an entity type as part of the model and returns an object that can be used to
                configure the entity. This method can be called multiple times for the same entity to
                perform multiple lines of configuration.
            </summary>
            <typeparam name = "TEntityType">The type to be registered or configured.</typeparam>
            <returns>The configuration object for the specified entity type.</returns>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.Entity(System.Type)">
            <summary>
                Registers a type as an entity in the model and returns an object that can be used to
                configure the entity. This method can be called multiple times for the same type to
                perform multiple lines of configuration.
            </summary>
            <param name = "entityType">The type to be registered or configured.</param>
            <returns>The configuration object for the specified entity type.</returns>
        </member>
        <member name="M:System.Data.Entity.DbModelBuilder.ComplexType``1">
            <summary>