﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class CarLine:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        //ForeignKey
        [Display(Name = "上層編號")]
        public int? ParentId { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentId")]
        public virtual CarLine ParentCarLine { get; set; }


        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "路線名稱")]
        public string Subject { get; set; }

        [Display(Name = "路線")]
        public string Line { get; set; }

        
        [Display(Name = "層級")]
        [NotMapped]
        public int Level
        {
            get
            {
                int level = 1;
                CarLine parentClass = this.ParentCarLine;
                while (parentClass != null)
                {
                    level++;
                    parentClass = parentClass.ParentCarLine;
                }

                return level;
            }

        }

        [Required(ErrorMessage = "排序必填")]
        [Display(Name = "排序")]
        public int ListNum { get; set; }

          [Required(ErrorMessage = "{0}必填")]
        [Display(Name = "每站最大載客數")]
        public int? MaxPassengers { get; set; }

          
          [Display(Name = "是否有時刻表")]
          public BooleanType IsHaveTimeTable { get; set; }

        public virtual ICollection<CarLine> ChildCarLine { get; set; }
        public virtual ICollection<TimeSchedule> TimeSchedules { get; set; }
    }
}