﻿<?xml version="1.0" encoding="utf-8"?>
<doc>
  <assembly>
    <name>System.Web.Http.WebHost</name>
  </assembly>
  <members>
    <member name="T:System.Web.Http.GlobalConfiguration">
      <summary> Provides a global <see cref="T:System.Web.Http.HttpConfiguration" /> for ASP.NET applications. </summary>
    </member>
    <member name="P:System.Web.Http.GlobalConfiguration.Configuration"></member>
    <member name="P:System.Web.Http.GlobalConfiguration.DefaultHandler">
      <summary> Gets the default message handler that will be called for all requests. </summary>
    </member>
    <member name="T:System.Web.Http.RouteCollectionExtensions">
      <summary> Extension methods for <see cref="T:System.Web.Routing.RouteCollection" /></summary>
    </member>
    <member name="M:System.Web.Http.RouteCollectionExtensions.MapHttpRoute(System.Web.Routing.RouteCollection,System.String,System.String)">
      <summary>Maps the specified route template.</summary>
      <returns>A reference to the mapped route.</returns>
      <param name="routes">A collection of routes for the application.</param>
      <param name="name">The name of the route to map.</param>
      <param name="routeTemplate">The route template for the route.</param>
    </member>
    <member name="M:System.Web.Http.RouteCollectionExtensions.MapHttpRoute(System.Web.Routing.RouteCollection,System.String,System.String,System.Object)">
      <summary>Maps the specified route template and sets default route.</summary>
      <returns>A reference to the mapped route.</returns>
      <param name="routes">A collection of routes for the application.</param>
      <param name="name">The name of the route to map.</param>
      <param name="routeTemplate">The route template for the route.</param>
      <param name="defaults">An object that contains default route values.</param>
    </member>
    <member name="M:System.Web.Http.RouteCollectionExtensions.MapHttpRoute(System.Web.Routing.Route