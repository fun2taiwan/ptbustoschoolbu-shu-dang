strategy">The strategy.</param>
            <value>The database creation strategy.</value>
        </member>
        <member name="M:System.Data.Entity.Database.SetInitializerInternal``1(System.Data.Entity.IDatabaseInitializer{``0},System.Boolean)">
            <summary>
                Internal version of SetInitializer that allows the strategy to be locked such that it cannot be replaced
                by another call to SetInitializer.  This allows strategies set in the app.config to win over strategies set
                in code.
            </summary>
            <typeparam name = "TContext">The type of the context.</typeparam>
            <param name = "strategy">The strategy.</param>
            <param name = "lockStrategy">if set to <c>true</c> then the strategy is locked.</param>
        </member>
        <member name="M:System.Data.Entity.Database.Initialize(System.Boolean)">
            <summary>
                Runs the the registered <see cref="T:System.Data.Entity.IDatabaseInitializer`1"/> on this context.
            
                If "force" is set to true, then the initializer is run regardless of whether or not it
                has been run before.  This can be useful if a database is deleted while an app is running
                and needs to be reinitialized.
            
                If "force" is set to false, then the initializer is only run if it has not already been
                run for this context, model, and connection in this app domain. This method is typically
                used when it is necessary to ensure that the database has been created and seeded
                before starting some operation where doing so lazily will cause issues, such as when the
                operation is part of a transaction.
            </summary>
            <param name="force">if set to <c>true</c> the initializer is run even if it has already been run.</param>
        </member>
        <member name="M:System.Data.Entity.Database.CompatibleWithModel(System.Boolean)">
            <summary>
            Checks whether or not the database is compatible with the the current Code First model.
            </summary>
            <remarks>
            Model compatibility currently uses the following rules.
            
            If the context was created using either the Model First or Database First approach then the
            model is assumed to be compatible with the database and this method returns true.
            
            For Code First the model is considered compatible if the model is stored in the database
            in the Migrations history table and that model has no differences from the current model as
            determined by Migrations model differ.
            
            If the model is not stored in the database but an EF 4.1/4.2 model hash is found instead,
            then this is used to check for compatibility.
            </remarks>
            <param name = "throwIfNoMetadata">
            If set to <c>true</c> then an exception will be thrown if no model metadata is found in
            the database. If set to <c>false</c> then this method will return <c>true</c> if metadata
            is not found.</param>
            <returns>
            True if the model hash in the context and the database match; false otherwise.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Database.Create">
            <summary>
                Creates a new database on the database server for the model defined in the backing context.
                Note that calling this method before the database initialization strategy has run will disable
                executing that strategy.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Database.CreateIfNotExists">
            <summary>
                Creates a new database on the database server for the model defined in the backing context, but only
     