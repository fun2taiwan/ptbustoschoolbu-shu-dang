namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarLineNewRule : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CarLines", "ParentId", c => c.Int());
            AddForeignKey("dbo.CarLines", "ParentId", "dbo.CarLines", "Id");
            CreateIndex("dbo.CarLines", "ParentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CarLines", new[] { "ParentId" });
            DropForeignKey("dbo.CarLines", "ParentId", "dbo.CarLines");
            DropColumn("dbo.CarLines", "ParentId");
        }
    }
}
