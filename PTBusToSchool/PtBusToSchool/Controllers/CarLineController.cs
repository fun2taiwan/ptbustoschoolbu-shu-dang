﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class CarLineController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 2000;
        //




        public ActionResult Index(int? page, FormCollection fc, int? id)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
            var carline = _db.CarLine.Include(p => p.ParentCarLine);
            carline = (id == null || id == 0) ? carline.Where(m => m.ParentId == null) : carline.Where(m => m.ParentId == id);

            if (id.HasValue && id != 0)
            {
                var path = _db.CarLine.Find(id);
                ViewBag.Path = "→" + path.Subject;
            }

            ViewBag.ParentId = id;
            if (hasViewData("SearchByParentId"))
            {
                int searchByParentId = getViewDateInt("SearchByParentId");
                carline = carline.Where(w => w.ParentId == searchByParentId);
            }
            if (hasViewData("SearchBySubject"))
            {
                string searchBySubject = getViewDateStr("SearchBySubject");
                carline = carline.Where(w => w.Subject.Contains(searchBySubject));
            }


            //            ViewBag.Subject = Subject;
            return View(carline.OrderBy(p => p.ListNum).ToPagedList(currentPageIndex, DefaultPageSize));



        }


        //[HttpPost]
        //public ActionResult Index(int? page, int? id)
        //{


        //    int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
        //    var carline = _db.CarLine.Include(p => p.ParentCarLine);



        //    carline = (id == null || id == 0) ? carline.Where(m => m.ParentId == null) : carline.Where(m => m.ParentId == id);



        //    carline = carline.OrderBy(p => p.ListNum);
        //    //ViewBag.ParentId = new SelectList(db.ProductClasses.OrderBy(p=>p.ListNum), "Id", "Subject");
        //    ViewBag.ParentId = id;
        //    return View(carline.OrderBy(p => p.ListNum).ToPagedList(currentPageIndex, DefaultPageSize));
        //}


        [HttpPost]
        public ActionResult Sort(string sortData)
        {
            if (!string.IsNullOrEmpty(sortData))
            {
                string[] tempDatas = sortData.TrimEnd(',').Split(',');
                string carlinestr = "";
                int? parentId = null;
                foreach (string tempData in tempDatas)
                {
                    string[] itemDatas = tempData.Split(':');
                    CarLine carline = _db.CarLine.Find(Convert.ToInt16(itemDatas[0]));
                    carline.ListNum = Convert.ToInt16(itemDatas[1]);
                    parentId = carline.ParentId;
                    carlinestr = carlinestr + carline.Subject + "→";
                    //_db.Entry(publish).State = EntityState.Modified;
                    _db.SaveChanges();

                }


                CarLine carlineParent = _db.CarLine.Find(parentId);
                carlinestr += "→.";
                carlinestr = carlinestr.Replace("→→.", "");
                carlinestr = carlinestr.Replace("→.", "");
                carlineParent.Line = carlinestr;
                _db.SaveChanges();
            }
            return RedirectToAction("Index");
        }


        //
        // GET: /admin/CarLine/Details/5

        public ActionResult Details(int id = 0)
        {
            CarLine carline = _db.CarLine.Find(id);
            if (carline == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(carline);
        }

        //
        // GET: /admin/CarLine/Create

        public ActionResult Create()
        {
            ViewBag.ParentId = new SelectList(_db.CarLine.OrderBy(p => p.ListNum), "Id", "Subject");
            ViewBag.CarSite = _db.CarSite.OrderBy(p => p.InitDate).ToList();

            return View();
        }

        //
        // POST: /admin/CarLine/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CarLine carline, string[] carSiteArray)
        {
            ModelState.Remove("ParentId");
            carline.IsHaveTimeTable=BooleanType.否;
            if (carSiteArray == null)
            {

                if (ModelState.IsValid)
                {

                    _db.CarLine.Add(carline);
                    int maxListNum = 0;
                    if ((_db.CarLine.Any()))
                    {
                        maxListNum = _db.CarLine.Max(x => x.ListNum);
                    }
                    carline.ListNum = maxListNum + 1;
                    carline.Create(_db, _db.CarLine);
                    return RedirectToAction("Index", new { Id = carline.ParentId });
                }
            }
            else
            {
                if (ModelState.IsValid)
                {

                    _db.CarLine.Add(carline);
                    int maxListNum = 0;
                    if ((_db.CarLine.Any()))
                    {
                        maxListNum = _db.CarLine.Max(x => x.ListNum);
                    }
                    carline.ListNum = maxListNum + 1;
                    carline.Create(_db, _db.CarLine);
                    var getcarlineId =
                        _db.CarLine.FirstOrDefault(x => x.ListNum == carline.ListNum);
                    for (int i = 0; i < carSiteArray.Length; i++)
                    {

                        CarLine carlineChild = new CarLine();
                        carlineChild.Subject = carSiteArray[i];
                        carlineChild.ParentId = getcarlineId.Id;
                        _db.CarLine.Add(carlineChild);

                        maxListNum = _db.CarLine.Max(x => x.ListNum);
                        carlineChild.ListNum = maxListNum + 1;
                        carlineChild.MaxPassengers = 0;
                        _db.SaveChanges();
                        //carline.Create(_db, _db.CarLine);
                    }
                    var carlineinsert = _db.CarLine.Where(x => x.ParentId == getcarlineId.Id).OrderBy(x => x.ListNum);
                    string carlinestr = "";
                    foreach (var items in carlineinsert)
                    {
                        carlinestr = carlinestr + items.Subject + "→";
                    }
                    carlinestr += "→.";
                    carlinestr = carlinestr.Replace("→→.", "");
                    carlinestr = carlinestr.Replace("→.", "");

                    carline.Line = carlinestr;


                    _db.SaveChanges();
                    return RedirectToAction("Index", new { Id = getcarlineId.Id });
                }

            }


            ViewBag.ParentId = new SelectList(_db.CarLine.OrderBy(p => p.ListNum), "Id", "Subject", carline.ParentId);
            return View(carline);
        }

        //
        // GET: /admin/CarLine/Edit/5

        public ActionResult Edit(int id = 0)
        {
            CarLine carline = _db.CarLine.Find(id);
            if (carline == null)
            {
                return HttpNotFound();
            }
            ViewBag.ParentId = new SelectList(_db.CarLine.OrderBy(p => p.ListNum), "Id", "Subject", carline.ParentId);
            ViewBag.CarLineChild = _db.CarLine.Where(x => x.ParentId == id).ToList();
            ViewBag.CarSite = _db.CarSite.OrderBy(x => x.InitDate).ToList();

            return View(carline);
        }

        //
        // POST: /admin/CarLine/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(CarLine carline, string[] carSiteArray)
        {

            if (ModelState.IsValid)
            {


                if (carSiteArray == null)
                {
                    var carlinechild = _db.CarLine.Where(x => x.ParentId == carline.Id);
                    if (carlinechild.Any())
                    {
                        foreach (var item in carlinechild)
                        {
                            _db.CarLine.Remove(item);
                        }
                        carline.Line = null;
                        carline.Update();
                        _db.SaveChanges();
                    }
                    else
                    {

                        carline.Update();
                    }
                }
                else
                {
                    var carlinechild = _db.CarLine.Where(x => x.ParentId == carline.Id);
                    string carsite = "";
                    if (carlinechild.Any())
                    {
                        for (int i = 0; i < carSiteArray.Length; i++)
                        {
                            carsite += carSiteArray[i] + "→";
                        }
              
                    carsite += "→.";
                    carsite = carsite.Replace("→→.", "");
                    carsite = carsite.Replace("→.", "");
                    int baselength = carsite.Length;
                    foreach (var items in carlinechild)
                    {
                        carsite = carsite.Replace(items.Subject, "");
                        if (baselength != carsite.Length)
                        {
                            baselength = carsite.Length;
                        }
                        else
                        {
                            _db.CarLine.Remove(items);
                        }
                    }
                    string[] carsitearr = carsite.Split('→');
                    int maxListNum = carlinechild.Max(x => x.ListNum);
                    int maxListNumaddnum = 0;
                    foreach (var item in carsitearr)
                    {
                        
                        if (!string.IsNullOrEmpty(item))
                        {
                            maxListNumaddnum++;
                            CarLine newcarlineChild = new CarLine();
                            newcarlineChild.Subject = item;
                            newcarlineChild.ParentId = carline.Id;
                            newcarlineChild.ListNum = maxListNum + maxListNumaddnum;
                            newcarlineChild.MaxPassengers = 0;
                            _db.CarLine.Add(newcarlineChild);
                           
                        }
                    }


                    _db.SaveChanges();



                        carlinechild = _db.CarLine.Where(x => x.ParentId == carline.Id).OrderBy(x=>x.ListNum);
                        int listnum = 1;
                        foreach (var item in carlinechild)
                        {
                            item.ListNum = listnum;
                            listnum++;
                            
                        }
                        _db.SaveChanges();

                        carlinechild = _db.CarLine.Where(x => x.ParentId == carline.Id).OrderBy(x => x.ListNum);
                        string carlinestr = "";
                        foreach (var item in carlinechild)
                        {
                            carlinestr = carlinestr + item.Subject + "→";
                        }
                        carlinestr += "→.";
                        carlinestr = carlinestr.Replace("→→.", "");
                        carlinestr = carlinestr.Replace("→.", "");
                        carline.Line = carlinestr;
                        carline.Update();


                    }
                    else
                    {
                        for (int i = 0; i < carSiteArray.Length; i++)
                        {

                            CarLine carlineChild = new CarLine();
                            carlineChild.Subject = carSiteArray[i];
                            carlineChild.ParentId = carline.Id;
                            carlineChild.ListNum = i + 1;
                            carlineChild.MaxPassengers = 0;
                           _db.CarLine.Add(carlineChild);
                               
                           
                        }
                        _db.SaveChanges();

                        var changecarline = _db.CarLine.Where(x => x.ParentId == carline.Id).OrderBy(x => x.ListNum);
                        string carlinestr = "";
                        foreach (var item in changecarline)
                        {
                            carlinestr = carlinestr + item.Subject + "→";
                        }
                        carlinestr += "→.";
                        carlinestr = carlinestr.Replace("→→.", "");
                        carlinestr = carlinestr.Replace("→.", "");
                        carline.Line = carlinestr;
                        carline.Update();



                    }



                }























                //_db.Entry(carline).State = EntityState.Modified;

                return RedirectToAction("Index", new { Id = carline.ParentId });
            }
            ViewBag.ParentId = new SelectList(_db.CarLine.OrderBy(p => p.ListNum), "Id", "Subject", carline.ParentId);
            return View(carline);
        }

        //
        // GET: /admin/CarLine/Delete/5

        public ActionResult Delete(int id = 0)
        {
            CarLine carline = _db.CarLine.Find(id);
            if (carline == null)
            {
                return HttpNotFound();
            }
            return View(carline);
        }

        //
        // POST: /admin/CarLine/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CarLine carline = _db.CarLine.Find(id);

            var parentId = carline.ParentId;

            var carlinechild = _db.CarLine.Where(x => x.ParentId == id);
            foreach (var item in carlinechild)
            {
                _db.CarLine.Remove(item);
            }
            _db.CarLine.Remove(carline);

            _db.SaveChanges();
            return RedirectToAction("Index", new { Id = parentId });
        }

        public ActionResult up()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult up(HttpPostedFileBase UpImageFiles)
        {
            //取得副檔名
            string extension = UpImageFiles.FileName.Split('.')[UpImageFiles.FileName.Split('.').Length - 1];
            //新檔案名稱
            string fileName = UpImageFiles.FileName;
            string savedName = Path.Combine(Server.MapPath("~/FileUpload/"), fileName);
            UpImageFiles.SaveAs(savedName);

            string strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " + Request.PhysicalApplicationPath + "FileUpload\\" + fileName + ";Extended Properties='Excel 12.0;HDR=NO; IMEX=1'";//連結字串中的HDR=YES，代表略過第一欄資料
            OleDbConnection oledb_con = new OleDbConnection(strCon);
            oledb_con.Open();
            OleDbCommand oledb_com = new OleDbCommand(" SELECT * FROM [工作表1$] ", oledb_con);
            OleDbDataReader oledb_dr = oledb_com.ExecuteReader();

            int i = 0;
          
            while (oledb_dr.Read())
            {
                i++;
                var rrr = oledb_dr.FieldCount;
                for (int j = 0; j < oledb_dr.FieldCount; j++)
                {
                    string dd = oledb_dr[j].ToString();
                    
                }

                //string dd = oledb_dr[0].ToString();
                //dd = Convert.ToDateTime(oledb_dr[1]).ToString("HH:mm");
                //dd = Convert.ToDateTime(oledb_dr[2]).ToString("HH:mm");
                //dd = Convert.ToDateTime(oledb_dr[3]).ToString("HH:mm");
                //dd = Convert.ToDateTime(oledb_dr[4]).ToString("HH:mm");
                //dd = Convert.ToDateTime(oledb_dr[5]).ToString("HH:mm"); 

            }
            oledb_dr.Close();
            oledb_con.Close();



            return View();
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
