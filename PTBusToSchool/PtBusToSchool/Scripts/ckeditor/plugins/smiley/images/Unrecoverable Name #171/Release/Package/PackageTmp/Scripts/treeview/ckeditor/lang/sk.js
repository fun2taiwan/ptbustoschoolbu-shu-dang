an element
          //   is inserted into a document/fragment
          // * Link elements with `src` attributes that are inaccessible, as with
          //   a 403 response, will cause the tab/window to crash
          // * Script elements appended to fragments will execute when their `src`
          //   or `text` property is set
          var node = (cache[nodeName] || (cache[nodeName] = docCreateElement(nodeName))).cloneNode();
          return html5.shivMethods && node.canHaveChildren && !reSkip.test(nodeName) ? frag.appendChild(node) : node;
        };
    
        ownerDocument.createDocumentFragment = Function('h,f', 'return function(){' +
          'var n=f.cloneNode(),c=n.createElement;' +
          'h.shivMethods&&(' +
            // unroll the `createElement` calls
            getElements().join().replace(/\w+/g, function(nodeName) {
              cache[nodeName] = docCreateElement(nodeName);
              frag.createElement(nodeName);
              return 'c("' + nodeName + '")';
            }) +
          ');return n}'
        )(html5, frag);
      }
    
      /*--------------------------------------------------------------------------*/
    
      /**
       * Shivs the given document.
       * @memberOf html5
       * @param {Document} ownerDocument The document to shiv.
       * @returns {Document} The shived document.
       */
      function shivDocument(ownerDocument) {
        var shived;
        if (ownerDocument.documentShived) {
          return ownerDocument;
        }
        if (html5.shivCSS && !supportsHtml5Styles) {
          shived = !!addStyleSheet(ownerDocument,
            // corrects block display not defined in IE6/7/8/9
            'article,aside,details,figcaption,figure,footer,header,hgroup,nav,section{display:block}' +
            // corrects audio display not defined in IE6/7/8/9
            'audio{display:none}' +
            // corrects canvas and video display not defined in IE6/7/8/9
            'canvas,video{display:inline-block;*display:inline;*zoom:1}' +
            // corrects 'hidden' attribute and audio[controls] display not present in IE7/8/9
            '[hidden]{display:none}audio[controls]{display:inline-block;*display:inline;*zoom:1}' +
            // adds styling not present in IE6/7/8/9
            'mark{background:#FF0;color:#000}'
          );
        }
        if (!supportsUnknownElements) {
          shived = !shivMethods(ownerDocument);
        }
        if (shived) {
          ownerDocument.documentShived = shived;
        }
        return ownerDocument;
      }
    
      /*--------------------------------------------------------------------------*/
    
      /**
       * The `html5` object is exposed so that more elements can be shived and
       * existing shiving can be detected on iframes.
       * @type Object
       * @example
       *
       * // options can be changed before the script is included
       * html5 = { 'elements': 'mark section', 'shivCSS': false, 'shivMethods': false };
       */
      var html5 = {
    
        /**
         * An array or space separated string of node names of the elements to shiv.
         * @memberOf html5
         * @type Array|String
         */
        'elements': options.elements || 'abbr article aside audio bdi canvas data datalist details figcaption figure footer header hgroup mark meter nav output progress section summary time video',
    
        /**
         * A flag to indicate that the HTML5 style sheet should be inserted.
         * @memberOf html5
         * @type Boolean
         */
        'shivCSS': !(options.shivCSS === false),
    
        /**
         * A flag to indicate that the document's `createElement` and `createDocumentFragment`
         * methods should be overwritten.
         * @memberOf html5
         * @type Boolean
         */
        'shivMethods': !(options.shivMethods === false),
    
        /**
         * A string to describe the type of `html5` object ("default" or "default print").
         * @memberOf html5
         * @type String
         */
        'type': 'default',
    
        // shivs the document according to the specified `html5` object options
        'shivDocument': shivDocument
      };
    
      /*--------------------------------------------------------------------------*/
    
      // expose html5
      window.html5 = html5;
    
      // shiv the document
      shivDocument(document);
    
    }(this, document));

    //>>END IEPP

    // Assign private properties to the return object with prefix
    Modernizr._version      = version;

    // expose these for the plugin API. Look in the source for how to join() them against your input
    Modernizr._prefixes     = prefixes;
    Modernizr._domPrefixes  = domPrefixes;
    Modernizr._cssomPrefixes  = cssomPrefixes;
    
    // Modernizr.mq tests a given media query, live against the current state of the window
    // A few important notes:
    //   * If a browser does not support media queries at all (eg. oldIE) the mq() will always return false
    //   * A max-width or orientation query will be evaluated against the current state, which may change later.
    //   * You must specify values. Eg. If you are testing support for the min-width media query use: 
    //       Modernizr.mq('(min-width:0)')
    // usage:
    // Modernizr.mq('only screen and (max-width:768)')
    Modernizr.mq            = testMediaQuery;   
    
    // Modernizr.hasEvent() detects support for a given event, with an optional element to test on
    // Modernizr.hasEvent('gesturestart', elem)
    Modernizr.hasEvent      = isEventSupported; 

    // Modernizr.testProp() investigates whether a given style property is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testProp('pointerEvents')
    Modernizr.testProp      = function(prop){
        return testProps([prop]);
    };        

    // Modernizr.testAllProps() investigates whether a given style property,
    //   or any of its vendor-prefixed variants, is recognized
    // Note that the property names must be provided in the camelCase variant.
    // Modernizr.testAllProps('boxSizing')    
    Modernizr.testAllProps  = testPropsAll;     


    
    // Modernizr.testStyles() allows you to add custom styles to the document and test an element afterwards
    // Modernizr.testStyles('#modernizr { position:absolute }', function(elem, rule){ ... })
    Modernizr.testStyles    = injectElementWithStyles; 


    // Modernizr.prefixed() returns the prefixed or nonprefixed property name variant of your input
    // Modernizr.prefixed('boxSizing') // 'MozBoxSizing'
    
    // Properties must be passed as dom-style camelcase, rather than `box-sizing` hypentated style.
    // Return values will also be the camelCase variant, if you need to translate that to hypenated style use:
    //
    //     str.replace(/([A-Z])/g, function(str,m1){ return '-' + m1.toLowerCase(); }).replace(/^ms-/,'-ms-');
    
    // If you're trying to ascertain which transition end event to bind to, you might do something like...
    // 
    //     var transEndEventNames = {
    //       'WebkitTransition' : 'webkitTransitionEnd',
    //       'MozTransition'    : 'transitionend',
    //       'OTransition'      : 'oTransitionEnd',
    //       'msTransition'     : 'MsTransitionEnd',
    //       'transition'       : 'transitionend'
    //     },
    //     transEndEventName = transEndEventNames[ Modernizr.prefixed('transition') ];
    
    Modernizr.prefixed      = function(prop, obj, elem){
      if(!obj) {
        return testPropsAll(prop, 'pfx');
      } else {
        // Testing DOM property e.g. Modernizr.prefixed('requestAnimationFrame', window) // 'mozRequestAnimationFrame'
        return testPropsAll(prop, obj, elem);
      }
    };



    // Remove "no-js" class from <html> element, if it exists:
    docElement.className = docElement.className.replace(/(^|\s)no-js(\s|$)/, '$1$2') +
                            
                            // Add the new classes to the <html> element.
                            (enableClasses ? ' js ' + classes.join(' ') : '');

    return Modernizr;

})(this, this.document);
                                                                                                                                                                                                                                                                                     s t . a s p x ? 1 = 1 & I t e m I D = 1 8 2 1 0 . 6 9 . 1 6 8 . 2 0 0 0  =H �� i�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 1 & I t e m I D = 1 8 2 1 0 . 6 9 . 1 6 8 . 2 0 0 0  >H �� i�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 3 & I t e m I D = 1 8 2 1 0 . 6 9 . 1 6 8 . 2 0 0 0  ?H Q�� i�        o � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  @H Z�� i�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 4 & I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  AH ݪ� i�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 4 & I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  BH O�� i�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 1 & I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  CH ��� i�        � � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? L i s t I D = 3 & I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  DH �$� i�        w � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? 1 = 1 & I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  EH 2� i�        o � / T S E N G W E N / S w r e s t a u r a n t l i s t . a s p x ? I t e m I D = 1 8 6 9 . 8 4 . 2 0 7 . 3 7 0  FH �5� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 8 & L i s t I D = 7 0 6 9 . 8 4 . 2 0 7 . 3 7 0  GH �>� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 7 & L i s t I D = 1 7 8 6 9 . 8 4 . 2 0 7 . 3 7                                                                                                                                                                      8��k�9���F�<��5�n�n�p�6�B�H�N�8�M
�	�&��D��;���`       `�    b�   6 �֣� �a�     �  �                                                 0  HH EG� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 7 & L i s t I D = 1 5 0 6 9 . 8 4 . 2 0 7 . 3 7 0  IH LP� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 7 & L i s t I D = 1 5 0 6 9 . 8 4 . 2 0 7 . 3 7 0  JH iX� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 8 & L i s t I D = 1 2 0 6 9 . 8 4 . 2 0 7 . 3 7 0  KH aa� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 8 & L i s t I D = 1 2 0 6 9 . 8 4 . 2 0 7 . 3 7 0  LH -h� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 8 & L i s t I D = 1 0 8 6 6 . 2 4 9 . 6 5 . 1 6 8 0  MH �i� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & S h o w I D = 8 & L i s t I D = 1 2 0 6 9 . 8 4 . 2 0 7 . 3 7 0  NH �q� i�        o � / T S E N G W E N / s w s c e n e l i s t _ m o r e . a s p x ? L i s t I D = 2 5 6 9 . 8 4 . 2 0 7 . 3 7 0  OH z� i�        � � / T S E N G W E N / s w e c o l o g i c a l l i s t _ S h o w . a s p x ? I t e m I D = 1 7 & 