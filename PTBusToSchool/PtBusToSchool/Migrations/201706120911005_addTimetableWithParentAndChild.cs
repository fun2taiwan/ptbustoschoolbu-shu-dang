namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTimetableWithParentAndChild : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Timetables", "ParentId", c => c.Int());
            AddForeignKey("dbo.Timetables", "ParentId", "dbo.Timetables", "Id");
            CreateIndex("dbo.Timetables", "ParentId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Timetables", new[] { "ParentId" });
            DropForeignKey("dbo.Timetables", "ParentId", "dbo.Timetables");
            DropColumn("dbo.Timetables", "ParentId");
        }
    }
}
