 representing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Decimal)">
            <summary>
                Generates code to specify the default value for a <see cref="T:System.Decimal"/> column.
            </summary>
            <param name="defaultValue">The value to be used as the default.</param>
            <returns>Code representing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Guid)">
            <summary>
                Generates code to specify the default value for a <see cref="T:System.Guid"/> column.
            </summary>
            <param name="defaultValue">The value to be used as the default.</param>
            <returns>Code representing the default value.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.VisualBasicMigrationCodeGenerator.Generate(System.Int64)">
            <summary>
                Generates code 