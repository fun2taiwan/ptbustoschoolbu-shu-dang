 methods/properties such that uses of the object
            can be mocked.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Internal.LegacyDatabaseInitializerConfig">
            <summary>
                Encapsulates information read from the application config file that specifies a database initializer
                and allows that initializer to be dynamically applied.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Internal.LegacyDatabaseInitializerConfig.#ctor(System.String,System.String)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Internal.LegacyDatabaseInitializerConfig"/> class.
            </summary>
            <param name="configKey">The key from the entry in the config file.</param>
            <param name="configValue">The value