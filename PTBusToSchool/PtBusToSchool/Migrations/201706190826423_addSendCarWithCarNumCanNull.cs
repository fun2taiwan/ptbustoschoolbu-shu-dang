namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSendCarWithCarNumCanNull : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SendCars", "CarNum", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SendCars", "CarNum", c => c.Int(nullable: false));
        }
    }
}
