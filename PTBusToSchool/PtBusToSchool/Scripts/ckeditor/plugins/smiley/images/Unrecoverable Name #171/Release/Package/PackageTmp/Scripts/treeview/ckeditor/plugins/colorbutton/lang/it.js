<summary>
                Gets the type of the exception that was thrown.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.Design.ToolingException.InnerStackTrace">
            <summary>
                Gets the stack trace of the exception that was thrown.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Migrations.Design.ToolingFacade">
            <summary>
                Helper class that is used by design time tools to run migrations related  
                commands that need to interact with an application that is being edited
                in Visual Studio.
            
                Because the application is being edited the assemblies need to
                be loaded in a separate AppDomain to ensure the latest version
                is always loaded.
            
                The App/Web.config file from the startup project is