﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class StudentReservation:BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "學生姓名")]
        public string Student { get; set; }

        [Display(Name = "學號")]
        public string StudentId { get; set; }

        [Display(Name = "學生證卡號")]
        public string StudentCardId { get; set; }

        [Display(Name = "預約路線")]
        public string ReservationCarLine { get; set; }

        [Display(Name = "起站")]
        public string Startstation { get; set; }

        [Display(Name = "迄站")]
        public string Endstation { get; set; }

        [Display(Name = "預約日期")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime ReservationDate { get; set; }

        [Display(Name = "預約時間")]
        public string ReservationTime { get; set; }

        [Display(Name = "預約班次")]
        public string ReservationCarNo { get; set; }

        [Display(Name = "媒合狀態")]
        public ReservationState ReservationStatus { get; set; }

        [Display(Name = "扣除的站點")]
        public string CutCarLine { get; set; }



    }
}