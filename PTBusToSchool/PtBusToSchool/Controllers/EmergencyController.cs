﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class EmergencyController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //

        


        public ActionResult Index(int? page, FormCollection fc )
        {
			//記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            

            var emergency = _db.Emergency.OrderByDescending(p => p.InitDate).AsQueryable();
            if (hasViewData("SearchBySubject")) 
            { 
            string searchBySubject = getViewDateStr("SearchBySubject");             
                emergency = emergency.Where(w => w.Subject.Contains(searchBySubject)); 
            }
            if (hasViewData("SearchByStartDate"))
            {
                DateTime Date = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));

                emergency = emergency.Where(w => w.IncidentDate == Date);
            }

//            ViewBag.Subject = Subject;
            return View(emergency.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }



        

        //
        // GET: /admin/Emergency/Details/5

        public ActionResult Details(int id = 0)
        {
            Emergency emergency = _db.Emergency.Find(id);
            if (emergency == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(emergency);
        }

        //
        // GET: /admin/Emergency/Create

        public ActionResult Create()
        {

            return View();
        }


        public JsonResult GetCarNo(int LineId)
        {
           
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();


            var sendcar = _db.SendCars.Where(x => x.ParentId == LineId).ToList();


            if (sendcar.Any())
            {
                foreach (var item in sendcar)
                {
                    string[] timearr = item.CarNoTime.Time.Split('、');

                    items.Add(new KeyValuePair<string, string>(
                        item.Id.ToString(),
                        string.Format("{0}", item.CarNoTime.CarNo + "(" + timearr[0] + ")")));
                }
            }

            return this.Json(items);
        }



        public JsonResult GetCarLine(DateTime datetext)
        {
           
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();


            var sendcar = _db.SendCars.FirstOrDefault(x => x.StartDate == datetext.Date);
            var carline = _db.SendCars.Where(x => x.ParentId == sendcar.Id).ToList();

            if (carline.Any())
            {
                foreach (var item in carline)
                {
                    items.Add(new KeyValuePair<string, string>(
                        item.Id.ToString(),
                        string.Format("{0}", item.LineName.Subject+"("+item.LineName.Line+")")));
                }
            }

            return this.Json(items);
        }

        //
        // POST: /admin/Emergency/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(Emergency emergency )
        {

            int Carlineid = Convert.ToInt16(emergency.Line);
            int CarNo = Convert.ToInt16(emergency.CarNo);
            var carline = _db.SendCars.Find(Carlineid);
            var carNo = _db.SendCars.Find(CarNo);


            emergency.Line = carline.LineName.Subject + "(" + carline.LineName.Line + ")";

            string[] timearr = carNo.CarNoTime.Time.Split('、');
            emergency.CarNo = carNo.CarNoTime.CarNo + "(" + timearr[0] + ")";





            if (ModelState.IsValid)
            {

                _db.Emergency.Add(emergency);
                emergency.Create(_db,_db.Emergency);
                return RedirectToAction("Index");
            }

            return View(emergency);
        }

        //
        // GET: /admin/Emergency/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Emergency emergency = _db.Emergency.Find(id);
            if (emergency == null)
            {
                return HttpNotFound();
            }
            return View(emergency);
        }

        //
        // POST: /admin/Emergency/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
         
        public ActionResult Edit(Emergency emergency)
        {
            if (ModelState.IsValid)
            {

               //_db.Entry(emergency).State = EntityState.Modified;
                emergency.Update();
                return RedirectToAction("Index",new{Page=-1});
            }
            return View(emergency);
        }

        //
        // GET: /admin/Emergency/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Emergency emergency = _db.Emergency.Find(id);
            if (emergency == null)
            {
                return HttpNotFound();
            }
            return View(emergency);
        }

        //
        // POST: /admin/Emergency/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Emergency emergency = _db.Emergency.Find(id);
            _db.Emergency.Remove(emergency);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
