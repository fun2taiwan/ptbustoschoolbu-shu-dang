﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public abstract class BackendBase
    {

        [MaxLength(20)]
        [Display(Name = "發佈者")]
        [JsonIgnore]
        public string Poster { get; set; }

        [Display(Name = "發佈時間")]
        [JsonIgnore]
        public DateTime? InitDate { get; set; }

        [MaxLength(20)]
        [Display(Name = "更新者")]
        [JsonIgnore]
        public string Updater { get; set; }



        [Display(Name = "最後更新時間")]
        [JsonIgnore]
        public DateTime? UpdateDate { get; set; }


      

        /// <summary>
        /// 新增
        /// </summary>
        public void Create(BackendContext db, System.Data.Entity.DbSet dbSet)
        {
            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);
            if (this.InitDate == null)
            {
                this.InitDate = NOWTime;
            }
            if (this.Poster == null)
            {
                this.Poster = HttpContext.Current.User.Identity.Name;
            }

            dbSet.Add(this);
            db.SaveChanges();
        }

        /// <summary>
        /// 更新
        /// </summary>
        public void Update()
        {
            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);

            BackendContext db = new BackendContext();
            this.UpdateDate =NOWTime;
            this.Updater = System.Web.HttpContext.Current.User.Identity.Name;
            db.Entry(this).State = EntityState.Modified;
            
            db.SaveChanges();
        }
        public void Update(BackendContext db, System.Data.Entity.DbSet dbSet)
        {
            TimeZoneInfo TPZone = TimeZoneInfo.FindSystemTimeZoneById("Taipei Standard Time");
            DateTime NOWTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TPZone);

            this.UpdateDate = NOWTime;
            this.Updater = System.Web.HttpContext.Current.User.Identity.Name;
            db.Entry(this).State = EntityState.Modified;

            
            db.SaveChanges();
            //try
            //{

            //}
            //catch (System.Data.Entity.Validation.DbEntityValidationException ex)
            //{

            //    throw ex;
            //}

        }

        /// <summary>
        /// 刪除
        /// </summary>
        public void Delete(BackendContext db, System.Data.Entity.DbSet dbSet)
        {
            dbSet.Remove(this);
            db.Entry(this).State = EntityState.Deleted;
            
            db.SaveChanges();
        }
    }
}