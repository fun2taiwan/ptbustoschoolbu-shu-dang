﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Net.Mail;
using System.Web.DataAccess;


namespace PtBusToSchool.Models
{
    public class BackendContext : DbContext
    {
        // 您可以將自訂程式碼新增到這個檔案。變更不會遭到覆寫。
        // 
        // 如果您要 Entity Framework 每次在您變更模型結構描述時
        // 自動卸除再重新產生資料庫，請將下列
        // 程式碼新增到 Global.asax 檔案的 Application_Start 方法中。
        // 注意: 這將隨著每次模型變更而損毀並重新建立您的資料庫。
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<CypdGlobalModels.Models.UnitContext>());

        public BackendContext()
            : base("name=DefaultConnection")
        {
        }
      
        //權限
        public DbSet<Unit> Units { get; set; } //單位
        public DbSet<Member> Members { get; set; }//管理者
        public DbSet<Role> Roles { get; set; } //角色

        public DbSet<CarSite> CarSite { get; set; } //站點
        public DbSet<Car> Car { get; set; } //車子
        public DbSet<CarLine> CarLine { get; set; }

        public DbSet<Driver> Drivers { get; set; } //路線
        public DbSet<Timetable> Timetable { get; set; } //時刻表
        public DbSet<TimeSchedule> TimeSchedule { get; set; } //時刻表
        public DbSet<Reservation> Reservation { get; set; } //預約
        public DbSet<StudentReservation> StudentReservation { get; set; } //預約
        public DbSet<SendCar> SendCars { get; set; } //預約派車
        public DbSet<ReservationEnd> ReservationEnd { get; set; } //預約完成後產生的虛擬時刻表
        public DbSet<Emergency> Emergency { get; set; } //突發狀況推播資訊
    }
}
