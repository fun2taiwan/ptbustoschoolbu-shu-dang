namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarAndCarLine : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarLines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 50),
                        Line = c.String(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Subject = c.String(nullable: false, maxLength: 50),
                        CarLine = c.String(),
                        Passengers = c.Int(),
                        CarStatus = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CarLineCarSites",
                c => new
                    {
                        CarLine_Id = c.Int(nullable: false),
                        CarSite_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CarLine_Id, t.CarSite_Id })
                .ForeignKey("dbo.CarLines", t => t.CarLine_Id, cascadeDelete: true)
                .ForeignKey("dbo.CarSites", t => t.CarSite_Id, cascadeDelete: true)
                .Index(t => t.CarLine_Id)
                .Index(t => t.CarSite_Id);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.CarLineCarSites", new[] { "CarSite_Id" });
            DropIndex("dbo.CarLineCarSites", new[] { "CarLine_Id" });
            DropForeignKey("dbo.CarLineCarSites", "CarSite_Id", "dbo.CarSites");
            DropForeignKey("dbo.CarLineCarSites", "CarLine_Id", "dbo.CarLines");
            DropTable("dbo.CarLineCarSites");
            DropTable("dbo.Cars");
            DropTable("dbo.CarLines");
        }
    }
}
