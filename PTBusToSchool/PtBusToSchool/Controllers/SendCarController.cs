﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
	[PermissionFilters]
    [Authorize]
    public class SendCarController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




	    public ActionResult Index(int? page, FormCollection fc, int? id,DateTime? date)
	    {
             var sendCarcount = _db.SendCars.Where(x => x.StartDate >= DateTime.Today);
	       
             if (sendCarcount.Count()!=14)
	        {
                DateTime dt = DateTime.Now.Date;
	            for (int i = 0; i < 14; i++)
	            {
                    initcarinfo(dt.AddDays(i));
	            }
             
	        }
	        if (id != null&& id!=0)
	        {
                SendCar sendCarlevel = _db.SendCars.Find(id);
                if (sendCarlevel.Level == 2)
                {
                    Reservationreset(sendCarlevel.ParentSendCar.StartDate);
                }
	        }
	   
	        //記住搜尋條件
	        if (id.HasValue && id != 0)
	        {
	        }
	        else
	        {
	        
	        GetCatcheRoutes(page, fc);
	    }

	    //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);

	        Session["Id"] = null;
            var sendcars = _db.SendCars.Include(s => s.ParentSendCar).Include(s => s.LineName).Include(s => s.CarNoTime).OrderByDescending(p => p.InitDate).AsQueryable();

            ViewBag.CarLineId = new SelectList(_db.CarLine.Where(p=>p.ParentId==null).OrderBy(p=>p.InitDate), "Id", "Subject");
            ViewBag.StartDate = new SelectList(_db.SendCars.Where(p => p.ParentId == null).OrderBy(p => p.InitDate), "Id", "StartDate");
            if (id.HasValue && id != 0)
            {
                SendCar sendCar = _db.SendCars.Find(id);
                if (sendCar.Level == 1)
                {
                    ViewBag.linename = id;
                    sendcars = sendcars.Where(x => x.ParentId == id);
                    ViewBag.Path = "→" + sendCar.StartDate.GetValueOrDefault().Date.ToString("yyyy/MM/dd");
                    
                }
                if (sendCar.Level == 2)
                {
                    var carline = _db.CarLine.Find(sendCar.CarLineId);
                    //ViewBag.grandparentid = sendCar.ParentId;
                    ViewBag.carnotime = carline.Line;
                    sendcars = sendcars.Where(x => x.GrandParentId == sendCar.ParentId&&x.ParentId==id);
                    ViewBag.Path = "→" + sendCar.ParentSendCar.StartDate.GetValueOrDefault().Date.ToString("yyyy/MM/dd")+"→" + sendCar.LineName.Subject;
                }
            }
            else
            {
                sendcars = sendcars.Where(x => x.ParentId == null);

            }
         
            if (hasViewData("SearchByStartDate"))
            {
               
                DateTime searchByStartDate = Convert.ToDateTime(getViewDateStr("SearchByStartDate"));
                sendcars = sendcars.Where(w => w.StartDate == searchByStartDate);
            } 
          


            return View(sendcars.OrderByDescending(p => p.StartDate).ToPagedList(currentPageIndex, DefaultPageSize));
           
        }

        private void Reservationreset(DateTime? date)
	    {
            SendCar sendCar = _db.SendCars.FirstOrDefault(x => x.StartDate == date);
	        var sendCarchild = _db.SendCars.Where(x => x.ParentId == sendCar.Id).ToList();
            var studentReservation =
                _db.StudentReservation.Where(
                    x => x.ReservationDate == date && x.ReservationStatus != ReservationState.取消預約);

	        foreach (var item in sendCarchild)
            {
                string[] getsitecount = item.LineName.Line.Split('→');
                var timeschedule = _db.TimeSchedule.Where(x => x.CarLineId == item.CarLineId && x.ParentId != null).ToList();
                string carlineid = item.CarLineId + "";
                string people = "";
                for (int j = 0; j < getsitecount.Length; j++)
                {
                    people = people + "0:0";
                    if (j != getsitecount.Length - 1)
                    {
                        people += "、";
                    }


                }
                
             
                foreach (var items in timeschedule)
                {

                    ArrayList addcolumn = new ArrayList();
                    ArrayList cutcolumn = new ArrayList();
                    for (int i = 0; i < getsitecount.Length; i++)
                    {
                        addcolumn.Add(0);
                        cutcolumn.Add(0);
                    }

                    foreach (var reservationitem in studentReservation.Where(x => x.ReservationCarNo == items.CarNo && x.ReservationCarLine == carlineid && x.ReservationStatus != ReservationState.取消預約))
                    {
                            for (int j = 0; j < getsitecount.Length; j++)
                            {
                                if (reservationitem.Startstation == getsitecount[j])
                                {
                                    addcolumn[j] = Convert.ToInt16(addcolumn[j]) + 1;

                                    break;
                                }

                            }
                        
                        for (int j = 0; j < getsitecount.Length; j++)
                        {
                            if (reservationitem.Endstation == getsitecount[j])
                            {
                                cutcolumn[j] = Convert.ToInt16(cutcolumn[j]) + 1;

                                break;
                            }

                        }
                        people = "";
                        for (int i = 0; i < addcolumn.Count; i++)
                        {
                            people += addcolumn[i] + ":" + cutcolumn[i];
                            if (i != addcolumn.Count - 1)
                            {
                                people += "、";
                            }
                        }

                    }
                   
                    var sendCargrandchild =
                        _db.SendCars.FirstOrDefault(
                            x => x.GrandParentId == sendCar.Id && x.ParentId == item.Id && x.TimeScheduleId == items.Id);
                    if (!string.IsNullOrEmpty(people))
                    {
                        sendCargrandchild.People = people;
                       
                        people = "";
                    }
                    else
                    {
                        for (int j = 0; j < getsitecount.Length; j++)
                        {
                            people = people + "0:0";
                            if (j != getsitecount.Length - 1)
                            {
                                people += "、";
                            }


                        }
                        sendCargrandchild.People = people;
                       
                    }





                    //addcolumn.Clear();
                    // cutcolumn.Clear();
                    //for (int i = 0; i < getsitecount.Length; i++)
                    //{
                    //    addcolumn.Add(0);
                    //    cutcolumn.Add(0);
                    //}

                    //foreach (var reservationitem in studentReservation.Where(x => x.ReservationCarNo == items.CarNo && x.ReservationCarLine == carlineid && x.ReservationStatus == ReservationState.候補))
                    //{
                    //    for (int j = 0; j < getsitecount.Length; j++)
                    //    {
                    //        if (reservationitem.Startstation == getsitecount[j])
                    //        {
                    //            addcolumn[j] = Convert.ToInt16(addcolumn[j]) + 1;

                    //            break;
                    //        }

                    //    }

                    //    for (int j = 0; j < getsitecount.Length; j++)
                    //    {
                    //        if (reservationitem.Endstation == getsitecount[j])
                    //        {
                    //            cutcolumn[j] = Convert.ToInt16(cutcolumn[j]) + 1;

                    //            break;
                    //        }

                    //    }
                    //    people = "";
                    //    for (int i = 0; i < addcolumn.Count; i++)
                    //    {
                    //        people += addcolumn[i] + ":" + cutcolumn[i];
                    //        if (i != addcolumn.Count - 1)
                    //        {
                    //            people += "、";
                    //        }
                    //    }

                    //}

                 
                    //if (!string.IsNullOrEmpty(people))
                    //{
                    //    sendCargrandchild.NoReservationPeople = people;
                    //    _db.SaveChanges();
                    //    people = "";
                    //}
                    //else
                    //{
                    //    for (int j = 0; j < getsitecount.Length; j++)
                    //    {
                    //        people = people + "0:0";
                    //        if (j != getsitecount.Length - 1)
                    //        {
                    //            people += "、";
                    //        }


                    //    }
                    //    sendCargrandchild.NoReservationPeople = people;
                        _db.SaveChanges();
                    //}
























                 
                }

	        }
	    }

	    public ActionResult Arrangeshifts(int? id)
	    {
            var sendcars = _db.SendCars.Where(x=>x.Id==id||x.ParentId==id||x.GrandParentId==id).ToList();
            return View(sendcars);
	    }

	    private void initcarinfo(DateTime today)
	    {
            SendCar sendCar = _db.SendCars.FirstOrDefault(x => x.StartDate == today);
	        if (sendCar == null)
	        {

           
                  
	                SendCar sendCarnew = new SendCar();
                    sendCarnew.StartDate = today;
	                _db.SendCars.Add(sendCarnew);
	                _db.SaveChanges();

                    var sendCargrandpa = _db.SendCars.FirstOrDefault(x => x.StartDate == today.Date);
	                int grandpaid = Convert.ToInt16(sendCargrandpa.Id);
	                var carline = _db.CarLine.Where(x => x.ParentId == null).ToList();
	                foreach (var item in carline)
	                {
	                    sendCarnew = new SendCar();
	                    sendCarnew.ParentId = grandpaid;
	                    sendCarnew.CarLineId = item.Id;

	                    _db.SendCars.Add(sendCarnew);
	                    _db.SaveChanges();
	                    var sendCarpa = _db.SendCars.FirstOrDefault(x => x.CarLineId == item.Id && x.ParentId == grandpaid);
	                    int parentid = Convert.ToInt16(sendCarpa.Id);
	                    var timeschedule =
	                        _db.TimeSchedule.Where(x => x.CarLineId == item.Id && x.ParentId != null).ToList();
	                    string[] getsitecount = item.Line.Split('→');
	                    string people = "";
                        var studentReservation = _db.StudentReservation.Where(x => x.ReservationDate == today.Date);

	                    for (int i = 0; i < getsitecount.Length; i++)
	                    {

	                        people += "0:0";
	                        if (i != getsitecount.Length - 1)
	                        {
	                            people += "、";
	                        }
	                    }
	                    foreach (var items in timeschedule)
	                    {
	                        sendCarnew = new SendCar();
	                        sendCarnew.ParentId = parentid;
	                        sendCarnew.TimeScheduleId = items.Id;
	                        sendCarnew.GrandParentId = grandpaid;
	                        string carlineid = items.CarLineId + "";

	                        ArrayList addcolumn = new ArrayList();
	                        ArrayList cutcolumn = new ArrayList();
	                        for (int i = 0; i < getsitecount.Length; i++)
	                        {
	                            addcolumn.Add(0);
	                            cutcolumn.Add(0);
	                        }
	                        foreach (var reservationitem in studentReservation.Where(x => x.ReservationCarNo == items.CarNo && x.ReservationCarLine == carlineid&&x.ReservationStatus!=ReservationState.取消預約))
	                        {

	                            for (int j = 0; j < getsitecount.Length; j++)
	                            {
	                                if (reservationitem.Startstation == getsitecount[j])
	                                {
	                                    addcolumn[j] = Convert.ToInt16(addcolumn[j]) + 1;

	                                    break;
	                                }

	                            }

	                            for (int j = 0; j < getsitecount.Length; j++)
	                            {
	                                if (reservationitem.Endstation == getsitecount[j])
	                                {
	                                    cutcolumn[j] = Convert.ToInt16(cutcolumn[j]) + 1;

	                                    break;
	                                }

	                            }

	                        }
	                        people = "";
	                        for (int i = 0; i < addcolumn.Count; i++)
	                        {
	                            people += addcolumn[i] + ":" + cutcolumn[i];
	                            if (i != addcolumn.Count - 1)
	                            {
	                                people += "、";
	                            }
	                        }
	                        sendCarnew.People = people;
                            //addcolumn.Clear();
                            //cutcolumn.Clear();
                            //for (int i = 0; i < getsitecount.Length; i++)
                            //{
                            //    addcolumn.Add(0);
                            //    cutcolumn.Add(0);
                            //}
                            //foreach (var reservationitem in studentReservation.Where(x => x.ReservationCarNo == items.CarNo && x.ReservationCarLine == carlineid && x.ReservationStatus == ReservationState.候補))
                            //{

                            //    for (int j = 0; j < getsitecount.Length; j++)
                            //    {
                            //        if (reservationitem.Startstation == getsitecount[j])
                            //        {
                            //            addcolumn[j] = Convert.ToInt16(addcolumn[j]) + 1;

                            //            break;
                            //        }

                            //    }

                            //    for (int j = 0; j < getsitecount.Length; j++)
                            //    {
                            //        if (reservationitem.Endstation == getsitecount[j])
                            //        {
                            //            cutcolumn[j] = Convert.ToInt16(cutcolumn[j]) + 1;

                            //            break;
                            //        }

                            //    }

                            //}
                            //people = "";
                            //for (int i = 0; i < addcolumn.Count; i++)
                            //{
                            //    people += addcolumn[i] + ":" + cutcolumn[i];
                            //    if (i != addcolumn.Count - 1)
                            //    {
                            //        people += "、";
                            //    }
                            //}
                            //sendCarnew.NoReservationPeople = people;
	                        _db.SendCars.Add(sendCarnew);
	                    }
	                    _db.SaveChanges();


	                }

	            
	        }
	        else
	        {
	            var sendcarold = _db.SendCars.Where(x => x.ParentId == sendCar.Id).ToList();
	            var carline = _db.CarLine.Where(x => x.ParentId == null).ToList();

	            SendCar sendCarnew = new SendCar();
	            foreach (var item in carline)
	            {
	                Boolean addYorN = true;
	                foreach (var items in sendcarold)
	                {
	                    if (item.Id == items.CarLineId)
	                    {

	                        addYorN = false;
	                    }

	                }
	                if (addYorN)
	                {

	                    sendCarnew = new SendCar();
	                    sendCarnew.ParentId = sendCar.Id;
	                    sendCarnew.CarLineId = item.Id;

	                    _db.SendCars.Add(sendCarnew);
	                    _db.SaveChanges();

	                    var sendCarpa = _db.SendCars.FirstOrDefault(x => x.CarLineId == item.Id && x.ParentId == sendCar.Id);
	                    int parentid = Convert.ToInt16(sendCarpa.Id);
	                    var timeschedule =
	                        _db.TimeSchedule.Where(x => x.CarLineId == item.Id && x.ParentId != null).ToList();
	                    string[] getsitecount = item.Line.Split('→');
	                    string people = "";
	                    for (int i = 0; i < getsitecount.Length; i++)
	                    {
	                        people += "0:0";
	                        if (i != getsitecount.Length - 1)
	                        {
	                            people += "、";
	                        }
	                    }
	                    foreach (var items in timeschedule)
	                    {
	                        sendCarnew = new SendCar();
	                        sendCarnew.ParentId = parentid;
	                        sendCarnew.TimeScheduleId = items.Id;
	                        sendCarnew.People = people;
                            sendCarnew.NoReservationPeople = people;
	                        sendCarnew.GrandParentId = sendCar.Id;
	                        _db.SendCars.Add(sendCarnew);
	                    }
	                    _db.SaveChanges();



	                }
	            }


	            foreach (var item in sendcarold)
	            {
	                string ss = item.CarLineId + "";
	                var alltimeschedule =
	                    _db.TimeSchedule.Where(x => x.ParentId != null && x.CarLineId == item.CarLineId).ToList();
	                string[] timearr = new string[] {};
	                string[] peoplearr = new string[] {};
	                foreach (var items in alltimeschedule)
	                {
	                    Boolean addYorN = true;
	                    Boolean editYorN = false;
	                    foreach (var itemchild in item.ChildSendCar)
	                    {
	                        if (items.Id == itemchild.TimeScheduleId)
	                        {
	                            addYorN = false;
	                        }
	                        timearr = items.LineName.Line.Split('→');
	                        peoplearr = itemchild.People.Split('、');
	                        if (timearr.Length != peoplearr.Length)
	                        {
	                            editYorN = true;
	                        }
	                    }
	                    if (addYorN)
	                    {

	                        sendCarnew = new SendCar();
	                        sendCarnew.ParentId = item.Id;
	                        sendCarnew.GrandParentId = sendCar.Id;
	                        sendCarnew.TimeScheduleId = items.Id;

	                        string[] getsitecount = item.LineName.Line.Split('→');
	                        string people = "";
	                        for (int i = 0; i < getsitecount.Length; i++)
	                        {
	                            people += "0:0";
	                            if (i != getsitecount.Length - 1)
	                            {
	                                people += "、";
	                            }
	                        }
	                        sendCarnew.People = people;
                          
	                        _db.SendCars.Add(sendCarnew);
	                        _db.SaveChanges();
	                    }
	                    if (editYorN)
	                    {

	                        string people = "";



	                        foreach (var itemedit in item.ChildSendCar)
	                        {
	                            int addcount = timearr.Length - peoplearr.Length;
	                            people = itemedit.People;
                                 
	                            for (int i = 0; i < addcount; i++)
	                            {
	                                people += "、0:0";
	                            }
	                            itemedit.People = people;
                          
	                            _db.SaveChanges();
	                        }

	                    }


	                }

	            }
	        }

	    }
        public JsonResult GetCar(CarStyle CarStyle)
        {
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();


            var cars = _db.Car.Where(x => x.CarStyles == CarStyle).ToList();
            if (cars.Count() > 0)
            {
                foreach (var item in cars)
                {
                    items.Add(new KeyValuePair<string, string>(
                        item.Id.ToString(),
                        string.Format("{0}", item.Subject)));
                }
            }
            
            return this.Json(items);
        }

        public ActionResult AddCar(int CarId, int editId)
        {

            List<int> CarIdCount = (List<int>)Session["Id"];
            if (CarIdCount ==null)
            {

                CarIdCount = new List<int>();
                CarIdCount.Add(CarId);
            }
            else
            {
                Boolean YN = false;
                foreach (var item in CarIdCount)
                {
                    if (item == CarId)
                    {
                        YN = true;
                    }
                }
                if (!YN)
                {
                    CarIdCount.Add(CarId);
                }
            }

            Session["Id"] = CarIdCount;
            //return RedirectToAction("Edit", new { id = editId });
            return Content("");
        }

	    //
        // GET: /admin/SendCar/Details/5

        public ActionResult Details(int id = 0)
        {
            SendCar sendcar = _db.SendCars.Find(id);
            if (sendcar == null)
            {
                //return HttpNotFound();
				 return View();
            }
            return View(sendcar);
        }

        //
        // GET: /admin/SendCar/Create

        public ActionResult Create()
        {
            ViewBag.ParentId = new SelectList(_db.SendCars.OrderBy(p=>p.InitDate), "Id", "People");
            ViewBag.CarLineId = new SelectList(_db.CarLine.OrderBy(p=>p.ListNum), "Id", "Subject");
            ViewBag.TimeScheduleId = new SelectList(_db.TimeSchedule.OrderBy(p => p.InitDate), "Id", "CarNo");
            return View();
        }

        //
        // POST: /admin/SendCar/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        
        public ActionResult Create(SendCar sendcar )
        {
            if (ModelState.IsValid)
            {

                _db.SendCars.Add(sendcar);
                sendcar.Create(_db,_db.SendCars);
                return RedirectToAction("Index");
            }

            ViewBag.ParentId = new SelectList(_db.SendCars.OrderBy(p => p.InitDate), "Id", "People", sendcar.ParentId);
            ViewBag.CarLineId = new SelectList(_db.CarLine.OrderBy(p=>p.ListNum), "Id", "Subject", sendcar.CarLineId);
            ViewBag.TimeScheduleId = new SelectList(_db.TimeSchedule.OrderBy(p => p.InitDate), "Id", "CarNo", sendcar.TimeScheduleId);
            return View(sendcar);
        }

        //
        // GET: /admin/SendCar/Edit/5

        public ActionResult Edit(int id = 0)
        {
            SendCar sendcar = _db.SendCars.Find(id);
            if (sendcar == null)
            {
                return HttpNotFound();
            }
            ViewBag.CarStyle = new SelectList(Enum.GetValues(typeof(PtBusToSchool.Models.CarStyle)));
            
            if (Session["Id"] != null)
            {
                List<int> allCarslist = (List<int>) Session["Id"];

                List<Car> Allcars = new List<Car>();
                foreach (var item in allCarslist)
                {
                    var cars = _db.Car.Find(item);
                    Allcars.Add(cars);
                }
                ViewBag.AllCars = Allcars;

            }
            else
            {
                if (sendcar.Cars != null)
                {
                    string[] cararr = sendcar.Cars.Split('、');
                    List<Car> Allcars = new List<Car>();
                    List<int> allCarslist = new List<int>();
                    foreach (var item in cararr)
                    {
                        var cars = _db.Car.FirstOrDefault(x => x.Subject == item);
                        Allcars.Add(cars);
                        allCarslist.Add(cars.Id);

                    }

                    Session["Id"] = allCarslist;

                    ViewBag.AllCars = Allcars;
                }
             





            }

            return View(sendcar);
        }

        //
        // POST: /admin/SendCar/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(SendCar sendcar, string[] AllCars)
        {
            if (ModelState.IsValid)
            {
                if (AllCars.Length == 0)
                {
                    sendcar.CarNum = 0;
                }
                else
                {
                    sendcar.CarNum = AllCars.Length;
                    string cars = "";
                    string drivers = "";
                    int passengers = 0; 
                    for (int i = 0; i < AllCars.Length; i++)
                    {
                        cars += AllCars[i];
                        string carid = AllCars[i];
                        var car = _db.Car.FirstOrDefault(x => x.Subject == carid);
                        passengers += Convert.ToInt16(car.SeatAmount + car.StandAmount);
                        drivers += car.CarAndDriver.Subject;
                        if (i != AllCars.Length - 1)
                        {
                            cars += "、";
                            drivers += "、";
                        }
                    }
                    sendcar.Cars = cars;
                    sendcar.Drivers = drivers;
                    sendcar.Passengers = passengers;
                }
                //_db.Entry(sendcar).State = EntityState.Modified;
                sendcar.Update();

                return RedirectToAction("Index",new{Page=-1});
            }
            ViewBag.Cars = _db.Car.OrderBy(p => p.CarStyles).ToList();
            return View(sendcar);
        }

        //
        // GET: /admin/SendCar/Delete/5

        public ActionResult Delete(int id = 0)
        {
            SendCar sendcar = _db.SendCars.Find(id);
            if (sendcar == null)
            {
                return HttpNotFound();
            }
            return View(sendcar);
        }

        //
        // POST: /admin/SendCar/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            List<SendCar> sendCarschild = _db.SendCars.Where(x => x.GrandParentId == id).ToList();
            foreach (var item in sendCarschild)
            {
                _db.SendCars.Remove(item);
            }
            _db.SaveChanges();
            sendCarschild = _db.SendCars.Where(x => x.ParentId == id).ToList();
            foreach (var item in sendCarschild)
            {
                _db.SendCars.Remove(item);
            }
            _db.SaveChanges();
            SendCar sendcar = _db.SendCars.Find(id);
            _db.SendCars.Remove(sendcar);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
