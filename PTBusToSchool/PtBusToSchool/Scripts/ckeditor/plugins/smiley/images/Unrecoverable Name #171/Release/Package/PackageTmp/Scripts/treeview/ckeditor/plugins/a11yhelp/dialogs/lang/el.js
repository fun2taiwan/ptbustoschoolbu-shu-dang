this object.</summary>
      <returns>The <see cref="T:System.Web.Http.Validation.ModelValidationNode" /> for this object.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.CompositeModelBinder">
      <summary>Represents an <see cref="T:System.Web.Http.ModelBinding.IModelBinder" /> that delegates to one of a collection of <see cref="T:System.Web.Http.ModelBinding.ModelBinderProvider" /> instances.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CompositeModelBinder.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.ModelBinding.IModelBinder})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.CompositeModelBinder" /> class.</summary>
      <param name="binders">An enumeration of binders.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CompositeModelBinder.#ctor(System.Web.Http.ModelBinding.IModelBinder[])">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.CompositeModelBinder" /> class.</summary>
      <param name="binders">An array of binders.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CompositeModelBinder.BindModel(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Indicates whether the specified model is binded.</summary>
      <returns>true if the model is binded; otherwise, false.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider">
      <summary>Represents the class for composite model binder providers.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider.#ctor(System.Collections.Generic.IEnumerable{System.Web.Http.ModelBinding.ModelBinderProvider})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider" /> class.</summary>
      <param name="providers">A collection of <see cref="T:System.Web.Http.ModelBinding.ModelBinderProvider" /></param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Gets the binder for the model.</summary>
      <returns>The binder for the model.</returns>
      <param name="configuration">The binder configuration.</param>
      <param name="modelType">The type of the model.</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.Binders.CompositeModelBinderProvider.Providers">
      <summary>Gets the providers for the composite model binder.</summary>
      <returns>The collection of providers.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.DictionaryModelBinder`2">
      <summary>Maps a browser request to a dictionary data object.</summary>
      <typeparam name="TKey">The type of the key.</typeparam>
      <typeparam name="TValue">The type of the value.</typeparam>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.DictionaryModelBinder`2.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.DictionaryModelBinder`2" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.DictionaryModelBinder`2.CreateOrReplaceCollection(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext,System.Collections.Generic.IList{System.Collections.Generic.KeyValuePair{`0,`1}})">
      <summary>Converts the collection to a dictionary.</summary>
      <returns>true in all cases.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
      <param name="newCollection">The new collection.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.DictionaryModelBinderProvider">
      <summary>Provides a model binder for a dictionary.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.DictionaryModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.DictionaryModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.DictionaryModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Retrieves the associated model binder.</summary>
      <returns>The associated model binder.</returns>
      <param name="configuration">The configuration to use.</param>
      <param name="modelType">The type of model.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinder`2">
      <summary>Maps a browser request to a key/value pair data object.</summary>
      <typeparam name="TKey">The type of the key.</typeparam>
      <typeparam name="TValue">The type of the value.</typeparam>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinder`2.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinder`2" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinder`2.BindModel(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Binds the model by using the specified execution context and binding context.</summary>
      <returns>true if model binding is successful; otherwise, false.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinderProvider">
      <summary>Provides a model binder for a collection of key/value pairs.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.KeyValuePairModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Retrieves the associated mode