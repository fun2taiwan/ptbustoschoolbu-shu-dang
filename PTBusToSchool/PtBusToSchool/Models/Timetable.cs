﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class Timetable : BackendBase
    {
        [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

        [Display(Name = "班次")]
        public string CarNo { get; set; }

        [Required(ErrorMessage = "{0}必填")]
        [MaxLength(50)]
        [Display(Name = "路線名稱")]
        public string Subject { get; set; }

        [Display(Name = "行經站點")]
        public string CarLine { get; set; }

        [Display(Name = "時間")]
        public string Time { get; set; }

        [Display(Name = "檔案名稱")]
        public string FileName { get; set; }

        //ForeignKey
        [Display(Name = "上層編號")]
        public int? ParentId { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentId")]
        public virtual Timetable ParentCarLine { get; set; }

        [Display(Name = "層級")]
        [NotMapped]
        public int Level
        {
            get
            {
                int level = 1;
                Timetable parentClass = this.ParentCarLine;
                while (parentClass != null)
                {
                    level++;
                    parentClass = parentClass.ParentCarLine;
                }

                return level;
            }

        }

 

        public virtual ICollection<Timetable> ChildTimetable { get; set; }
    }
}