namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMaxPassengersInCarLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CarLines", "MaxPassengers", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CarLines", "MaxPassengers");
        }
    }
}
