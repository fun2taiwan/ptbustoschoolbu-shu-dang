.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelBindingContext.ValueProvider">
      <summary>Gets or sets the value provider.</summary>
      <returns>The value provider.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelError">
      <summary>Represents an error that occurs during model binding.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelError.#ctor(System.Exception)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelError" /> class by using the specified exception.</summary>
      <param name="exception">The exception.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelError.#ctor(System.Exception,System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelError" /> class by using the specified exception and error message.</summary>
      <param name="exception">The exception.</param>
      <param name="errorMessage">The error message</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelError.#ctor(System.String)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelError" /> class by using the specified error message.</summary>
      <param name="errorMessage">The error message</param>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelError.ErrorMessage">
      <summary>Gets or sets the error message.</summary>
      <returns>The error message.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelError.Exception">
      <summary>Gets or sets the exception object.</summary>
      <returns>The exception object.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelErrorCollection">
      <summary>Represents a collection of <see cref="T:System.Web.Http.ModelBinding.ModelError" /> instances.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelErrorCollection.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelErrorCollection" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelErrorCollection.Add(System.Exception)">
      <summary>Adds the specified Exception object to the model-error collection.</summary>
      <param name="exception">The exception.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelErrorCollection.Add(System.String)">
      <summary>Adds the specified error message to the model-error collection.</summary>
      <param name="errorMessage">The error message.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelState">
      <summary>Encapsulates the state of model binding to a property of an action-method argument, or to the argument itself.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelState.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelState" /> class.</summary>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelState.Errors">
      <summary>Gets a <see cref="T:System.Web.Http.ModelBinding.ModelErrorCollection" /> object that contains any errors that occurred during model binding.</summary>
      <returns>The model state errors.</returns>
    </member>
    <member name="P:System.Web.Http.ModelBinding.ModelState.Value">
      <summary>Gets a <see cref="T:System.Web.Http.ValueProviders.ValueProviderResult" /> object that encapsulates the value that was being bound during model binding.</summary>
      <returns>The model state value.</returns>
    </member>
    <member name="T:System.Web.Http.ModelBinding.ModelStateDictionary">
      <summary>Represents the state of an attempt to bind a posted form to an action method, which includes validation information.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelStateDictionary" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.#ctor(System.Web.Http.ModelBinding.ModelStateDictionary)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.ModelStateDictionary" /> class by using values that are copied from the specified model-state dictionary.</summary>
      <param name="dictionary">The dictionary.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.Add(System.Collections.Generic.KeyValuePair{System.String,System.Web.Http.ModelBinding.ModelState})">
      <summary>Adds the specified item to the model-state dictionary.</summary>
      <param name="item">The object to add to the model-state dictionary.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.Add(System.String,System.Web.Http.ModelBinding.ModelState)">
      <summary>Adds an element that has the specified key and value to the model-state dictionary.</summary>
      <param name="key">The key of the element to add.</param>
      <param name="value">The value of the element to add.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStateDictionary.AddModelError(System.String,System.Exception)">
      <summary>Adds the specified model error to the errors collection for the model-state dictionary that is associated with the specified key.</summary>
      <param name="key">The key.</param>
      <param name="exception">The exception.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.ModelStat