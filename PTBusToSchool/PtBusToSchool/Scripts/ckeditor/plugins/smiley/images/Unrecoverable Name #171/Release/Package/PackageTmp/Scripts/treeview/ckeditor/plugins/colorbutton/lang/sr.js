       Provides access to features of the context that deal with change tracking of entities.
            </summary>
            <value>An object used to access features that deal with change tracking.</value>
        </member>
        <member name="P:System.Data.Entity.DbContext.Configuration">
            <summary>
                Provides access to configuration options for the context.
            </summary>
            <value>An object used to access configuration options.</value>
        </member>
        <member name="P:System.Data.Entity.DbContext.InternalContext">
            <summary>
                Provides access to the underlying InternalContext for other parts of the internal design.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Internal.AppConfig">
            <summary>
            A simple representation of an app.config or web.config file.
          