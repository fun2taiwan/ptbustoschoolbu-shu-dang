﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace PtBusToSchool.Models
{
    public class ReservationEnd:BackendBase
    {
          [Key]
        [Display(Name = "編號")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [JsonProperty]
        public int Id { get; set; }

          //ForeignKey
        [Display(Name = "上層編號")]
        public int? ParentId { get; set; }

        [JsonIgnore]
        [ForeignKey("ParentId")]
        public virtual ReservationEnd ParentSendCar { get; set; }

        [Display(Name = "上上層")]
        public int? GrandParentId { get; set; }


          [Display(Name = "發車日期")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime? StartDate { get; set; }


      
        [Display(Name = "路線名稱")]
        public string  LineName { get; set; }

        [Display(Name = "路線")]
        public string Line { get; set; }

        [Display(Name = "班次")]
        public int CarNo { get; set; }

        [Display(Name = "站到站時間")]
        public string CarTime { get; set; }

        [Display(Name = "每站預約成功人數")]
        public string People { get; set; }


        [Display(Name = "層級")]
        [NotMapped]
        public int Level
        {
            get
            {
                int level = 1;
                ReservationEnd parentClass = this.ParentSendCar;
                while (parentClass != null)
                {
                    level++;
                    parentClass = parentClass.ParentSendCar;
                }

                return level;
            }

        }



        public virtual ICollection<ReservationEnd> ChildReservationEnd{ get; set; }
    }
    
}