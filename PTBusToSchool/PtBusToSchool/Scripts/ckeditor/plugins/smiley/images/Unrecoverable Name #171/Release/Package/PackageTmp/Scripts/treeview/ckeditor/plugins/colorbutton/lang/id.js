se information.
            </summary>
        </member>
        <member name="T:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger">
            <summary>
                Base class for loggers that can be used for the migrations process.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger.Info(System.String)">
            <summary>
                Logs an informational message.
            </summary>
            <param name = "message">The message to be logged.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Infrastructure.MigrationsLogger.Warning(System.String)">
            <summary>
                Logs a warning that the user should be made aware of.
            </summary>
            <param name = "message">The message to be logged.</param>
       