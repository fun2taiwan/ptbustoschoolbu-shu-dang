namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delTimetableWithCarNo : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Timetables", "CarNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Timetables", "CarNo", c => c.String());
        }
    }
}
