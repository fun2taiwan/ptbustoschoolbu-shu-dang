namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addSendCar : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SendCars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ParentId = c.Int(),
                        StartDate = c.DateTime(),
                        CarLineId = c.Int(),
                        TimeScheduleId = c.Int(),
                        People = c.String(),
                        CarNum = c.Int(nullable: false),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.SendCars", t => t.ParentId)
                .ForeignKey("dbo.CarLines", t => t.CarLineId)
                .ForeignKey("dbo.TimeSchedules", t => t.TimeScheduleId)
                .Index(t => t.ParentId)
                .Index(t => t.CarLineId)
                .Index(t => t.TimeScheduleId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.SendCars", new[] { "TimeScheduleId" });
            DropIndex("dbo.SendCars", new[] { "CarLineId" });
            DropIndex("dbo.SendCars", new[] { "ParentId" });
            DropForeignKey("dbo.SendCars", "TimeScheduleId", "dbo.TimeSchedules");
            DropForeignKey("dbo.SendCars", "CarLineId", "dbo.CarLines");
            DropForeignKey("dbo.SendCars", "ParentId", "dbo.SendCars");
            DropTable("dbo.SendCars");
        }
    }
}
