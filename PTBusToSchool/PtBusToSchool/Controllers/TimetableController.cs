﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using MvcPaging;
using System.Web.Mvc;
using ClosedXML.Excel;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using PtBusToSchool.Filters;
using PtBusToSchool.Models;

namespace PtBusToSchool.Areas.admin.Controllers
{
    [PermissionFilters]
    [Authorize]
    public class TimetableController : _BaseController
    {
        private BackendContext _db = new BackendContext();
        private const int DefaultPageSize = 15;
        //




        public ActionResult Index(int? page, FormCollection fc, int? id)
        {
            //記住搜尋條件
            GetCatcheRoutes(page, fc);

            //取得正確的頁面
            int currentPageIndex = getCurrentPage(page, fc);
            var timetable = _db.Timetable.OrderByDescending(p => p.InitDate).AsQueryable();
            if (id.HasValue && id != 0)
            {
                var timetablegetcarlineid = _db.Timetable.Find(id);
                int lineid = Convert.ToInt16(timetablegetcarlineid.CarLine);
                ViewBag.AllLine = _db.CarLine.Find(lineid);
                var path = _db.Timetable.Find(id);
                ViewBag.Path = "→" + path.Subject;
                timetable = _db.Timetable.Where(x => x.ParentId == id);

            }


            if (hasViewData("SearchBySubject"))
            {
                string searchBySubject = getViewDateStr("SearchBySubject");
                timetable = timetable.Where(w => w.Subject.Contains(searchBySubject));
            }


            //            ViewBag.Subject = Subject;
            return View(timetable.OrderByDescending(p => p.InitDate).ToPagedList(currentPageIndex, DefaultPageSize));

        }





        //
        // GET: /admin/Timetable/Details/5

        public ActionResult Details(int id = 0)
        {
            Timetable timetable = _db.Timetable.Find(id);
            if (timetable == null)
            {
                //return HttpNotFound();
                return View();
            }
            return View(timetable);
        }

        //
        // GET: /admin/Timetable/Create

        public ActionResult Create()
        {
            ViewBag.Subject = new SelectList(_db.CarLine.Where(x => x.ParentId == null).OrderBy(p => p.ListNum), "Id", "Subject");

            return View();
        }

        //
        // POST: /admin/Timetable/Create

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Create(Timetable timetable, HttpPostedFileBase UpFiles, String updateTorF)
        {
            int carlineid = Convert.ToInt16(timetable.Subject);
            var carline = _db.CarLine.Find(carlineid);
            timetable.CarLine = timetable.Subject;
            timetable.Subject = carline.Subject;
            string carlineidthanuse = carlineid + "";
            if (ModelState.IsValid)
            {
                //取得副檔名
                string extension = UpFiles.FileName.Split('.')[UpFiles.FileName.Split('.').Length - 1];
                //新檔案名稱
                if (!extension.Equals("xlsx") && !extension.Equals("xls"))
                {
                    ViewBag.Message = "您上傳的檔案非EXCEL格式";
                    return View(timetable);
                }
                string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, extension);
                string savedName = Path.Combine(Server.MapPath("~/FileUpload/"), fileName);
                UpFiles.SaveAs(savedName);
                string strCon = "";
                if (updateTorF == "on")
                {
                    strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " +
                                   Request.PhysicalApplicationPath + "FileUpload\\" + fileName +
                                   ";Extended Properties='Excel 12.0;HDR=YES; IMEX=1'"; //連結字串中的HDR=YES，代表略過第一欄資料
                }
                else
                {
                    strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " + Request.PhysicalApplicationPath + "FileUpload\\" + fileName + ";Extended Properties='Excel 12.0;HDR=NO; IMEX=1'";//連結字串中的HDR=YES，代表略過第一欄資料
                }

                OleDbConnection oledb_con = new OleDbConnection(strCon);
                oledb_con.Open();
                OleDbCommand oledb_com = new OleDbCommand(" SELECT * FROM [工作表1$] ", oledb_con);
                OleDbDataReader oledb_dr = oledb_com.ExecuteReader();

                int i = 0;
                string carlinestr = "";
                int parentId = 0;
                if (updateTorF == "on")
                {
                    while (oledb_dr.Read())
                    {
                        i++;

                        if (i == 1)
                        {

                         
                            
                          
                            timetable.FileName = fileName;
                            //timetable.CarLine = carlinestr;
                            _db.Timetable.Add(timetable);
                            timetable.Create(_db, _db.Timetable);

                          
                            _db.SaveChanges();

                           
                            var parenttimetable =
                                _db.Timetable.FirstOrDefault(
                                    x => x.CarLine == carlineidthanuse && x.FileName == fileName && x.Subject == timetable.Subject);
                            parentId = parenttimetable.Id;



                        }
                       
                            string cartime = "";
                            string carNo = "";
                            for (int j = 0; j < oledb_dr.FieldCount; j++)
                            {
                                if (j == 0)
                                {
                                    carNo = oledb_dr[j].ToString();
                                }
                                else
                                {
                                    cartime += Convert.ToDateTime(oledb_dr[j]).ToString("HH:mm") + "、";
                                }

                            }

                            cartime += "、.";
                            cartime = cartime.Replace("、、.", "");
                            cartime = cartime.Replace("、.", "");
                            Timetable timetablechild = new Timetable();
                            timetablechild.FileName = fileName;
                            timetablechild.CarNo = carNo;
                            timetablechild.ParentId = parentId;
                            timetablechild.Time = cartime;
                            timetablechild.Subject = timetable.Subject;
                            _db.Timetable.Add(timetablechild);
                            _db.SaveChanges();
                        


                    }
                }
                else
                {
                    while (oledb_dr.Read())
                    {
                        i++;

                        if (i == 1)
                        {

                            for (int j = 1; j < oledb_dr.FieldCount; j++)
                            {
                                carlinestr = carlinestr + oledb_dr[j].ToString() + "→";
                                string countstr = oledb_dr[j].ToString();
                                var carsiteold = _db.CarSite.FirstOrDefault(x => x.Subject == countstr);
                                if (carsiteold == null)
                                {
                                    CarSite carSite = new CarSite();
                                    carSite.Subject = oledb_dr[j].ToString();
                                    carSite.Poster = timetable.Poster;
                                    carSite.InitDate = timetable.InitDate;
                                    _db.CarSite.Add(carSite);




                                }


                            }
                            carlinestr += "→.";
                            carlinestr = carlinestr.Replace("→→.", "");
                            carlinestr = carlinestr.Replace("→.", "");

                            timetable.FileName = fileName;
                            //timetable.CarLine = carlinestr;
                            _db.Timetable.Add(timetable);
                            timetable.Create(_db, _db.Timetable);

                            CarLine carLine = _db.CarLine.Find(carlineid);
                            carLine.Line = carlinestr;
                            var carlineallsite = _db.CarLine.Where(x => x.ParentId == carLine.Id);
                            foreach (var item in carlineallsite)
                            {
                                _db.CarLine.Remove(item);
                            }

                            _db.SaveChanges();

                            for (int j = 1; j < oledb_dr.FieldCount; j++)
                            {
                                CarLine carLinenewsite = new CarLine();
                                carLinenewsite.Subject = oledb_dr[j].ToString();
                                carLinenewsite.ParentId = carlineid;
                                carLinenewsite.ListNum = j;
                                _db.CarLine.Add(carLinenewsite);
                            }
                            _db.SaveChanges();
                            var parenttimetable =
                                _db.Timetable.FirstOrDefault(
                                    x => x.CarLine == carlineidthanuse && x.FileName == fileName && x.Subject == timetable.Subject);
                            parentId = parenttimetable.Id;



                        }
                        else
                        {
                            string cartime = "";
                            string carNo = "";
                            for (int j = 0; j < oledb_dr.FieldCount; j++)
                            {
                                if (j == 0)
                                {
                                    carNo = oledb_dr[j].ToString();
                                }
                                else
                                {
                                    cartime += Convert.ToDateTime(oledb_dr[j]).ToString("HH:mm") + "、";
                                }

                            }

                            cartime += "、.";
                            cartime = cartime.Replace("、、.", "");
                            cartime = cartime.Replace("、.", "");
                            Timetable timetablechild = new Timetable();
                            timetablechild.FileName = fileName;
                            timetablechild.CarNo = carNo;
                            timetablechild.ParentId = parentId;
                            timetablechild.Time = cartime;
                            timetablechild.Subject = timetable.Subject;
                            _db.Timetable.Add(timetablechild);
                            _db.SaveChanges();
                        }
                    }
                }
                oledb_dr.Close();
                oledb_con.Close();
                return RedirectToAction("Index");
            }

            return View(timetable);
        }

        //
        // GET: /admin/Timetable/Edit/5

        public ActionResult ReUpdate(int id = 0)
        {

            Timetable timetable = _db.Timetable.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            ViewBag.Subject = new SelectList(_db.CarLine.Where(x => x.ParentId == null).OrderBy(p => p.ListNum), "Id", "Subject", timetable.CarLine);
            return View(timetable);
        }


        public byte[] ExportExcel(IEnumerable<Timetable> list, Timetable timetable)
        {
            //建立Excel
            XLWorkbook workbook = new XLWorkbook();
            var sheet = workbook.Worksheets.Add("時刻表");
            int colIdx = 1;
            int carlineid = Convert.ToInt16(timetable.CarLine);
            var carLine = _db.CarLine.Find(carlineid);
            string[] carLinestr = carLine.Line.Split('→');
            string columnstr = "班次";
            for (int i = 0; i < carLinestr.Length; i++)
            {
                columnstr += "#" + carLinestr[i];
            }
            String[] columns = columnstr.Split('#');


            //String[] columns = { "訂單編號", "訂單日期", "購買人姓名", "購買人電話", "付款方式", "訂單處理狀態", "小計(不含運費)", "商城名稱", "供應商訂單處理狀態", "運費" };
            //String[] columns = { list.FirstOrDefault().Account,  list.FirstOrDefault().Name, list.FirstOrDefault().Gender, "E-Mail", "電話", "住址", "發佈者", "發佈時間", list.FirstOrDefault().Account};
            foreach (string colName in columns)
            {
                sheet.Cell(1, colIdx++).Value = colName;
            }
            //修改標題列Style
            //var header = sheet.Range("A1:J1");
            //header.Style.Fill.BackgroundColor = XLColor.Yellow;
            //header.Style.Font.FontColor = XLColor.Red;
            //header.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
          
            int rowIdx = 2;

            foreach (var item in list)
            {
                //if(item.)
                //sheet.Cell(rowIdx, 1).Style.NumberFormat.Format = "############################";
                sheet.Cell(rowIdx, 1).Value = "'" + item.CarNo;
                string[] timearr = item.Time.Split('、');
                int count = 2;
                for (int i = 0; i < timearr.Length; i++)
                {
                    sheet.Cell(rowIdx, count).Value = "'" + timearr[i];
                    count++;
                }



                rowIdx++;


            }






            ////第一欄隱藏
            //sheet.Column(1).Hide();
            ////自動伸縮欄寬
            //sheet.Column(2).AdjustToContents();
            //sheet.Column(2).Width += 2;
            //sheet.Column(3).Width = 50;
            ////寫入檔案-0
            //workbook.SaveAs(excelPath);
            for (int i = 1; i < columns.Length; i++)
            {
                sheet.Column(i).AdjustToContents();

            }

            byte[] bytes;
            using (var memoryStream = new MemoryStream())
            {
                workbook.SaveAs(memoryStream);
                bytes = memoryStream.ToArray();
            }
            return bytes;
        }



        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult ReUpdate(Timetable timetable, HttpPostedFileBase UpFiles, String updateTorF)
        {
            int carlineid = Convert.ToInt16(timetable.Subject);
            var carline = _db.CarLine.Find(carlineid);
            timetable.CarLine = timetable.Subject;
            timetable.Subject = carline.Subject;
            string carlineidthanuse = carlineid + "";
            if (Request["excel"] != null)
            {

                var timetableall = _db.Timetable.Where(x => x.ParentId == timetable.Id);
                Byte[] fileContent = ExportExcel(timetableall.ToList(), timetable);

             
                //string extension = timetable.FileName.Split('.')[timetable.FileName.Split('.').Length - 1]+"x";
                string filename = timetable.Subject + "時刻表.xlsx";
                return File(fileContent, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
                //return File(filepath, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", timetable.Subject + "時刻表." + extension);


        }
            
         
            if (ModelState.IsValid)
            {
                if (UpFiles == null)
                {
                    timetable.Update();
               


                    return RedirectToAction("Index", new { Page = -1 });
                }
                else
                {
                    //取得副檔名
                    string extension = UpFiles.FileName.Split('.')[UpFiles.FileName.Split('.').Length - 1];
                    //新檔案名稱
                    if (!extension.Equals("xlsx") && !extension.Equals("xls"))
                    {
                        ViewBag.Message = "您上傳的檔案非EXCEL格式";
                        return View(timetable);
                    }
                    List<Timetable> timetablechild = _db.Timetable.Where(x => x.ParentId == timetable.Id).ToList();
                    if (timetablechild.Count > 0)
                    {
                        foreach (var item in timetablechild)
                        {
                            _db.Timetable.Remove(item);
                        }
                        _db.SaveChanges();
                    }
                    Timetable beforeTimetable = _db.Timetable.Find(timetable.Id);
                    _db.Timetable.Remove(beforeTimetable);
                    _db.SaveChanges();



                    string fileName = String.Format("{0:yyyyMMddhhmmsss}.{1}", DateTime.Now, extension);
                    string savedName = Path.Combine(Server.MapPath("~/FileUpload/"), fileName);
                    UpFiles.SaveAs(savedName);
                    string strCon = "";
                    if (updateTorF == "on")
                    {
                        strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " +
                                       Request.PhysicalApplicationPath + "FileUpload\\" + fileName +
                                       ";Extended Properties='Excel 12.0;HDR=YES; IMEX=1'"; //連結字串中的HDR=YES，代表略過第一欄資料
                    }
                    else
                    {
                        strCon = " Provider = Microsoft.ACE.OLEDB.12.0 ; Data Source = " + Request.PhysicalApplicationPath + "FileUpload\\" + fileName + ";Extended Properties='Excel 12.0;HDR=NO; IMEX=1'";//連結字串中的HDR=YES，代表略過第一欄資料
                    }
                    OleDbConnection oledb_con = new OleDbConnection(strCon);
                    oledb_con.Open();
                    OleDbCommand oledb_com = new OleDbCommand(" SELECT * FROM [工作表1$] ", oledb_con);
                    OleDbDataReader oledb_dr = oledb_com.ExecuteReader();

                    int i = 0;
                    string carlinestr = "";
                    int parentId = 0;
                    if (updateTorF == "on")
                    {
                        while (oledb_dr.Read())
                        {
                            i++;

                            if (i == 1)
                            {
                                timetable.FileName = fileName;
                                timetable.CarLine = carlineidthanuse;
                                _db.Timetable.Add(timetable);
                                timetable.Create(_db, _db.Timetable);                             
                                _db.SaveChanges();
                                var parenttimetable =
                                    _db.Timetable.FirstOrDefault(
                                        x => x.CarLine == carlineidthanuse && x.FileName == fileName && x.Subject == timetable.Subject);
                                parentId = parenttimetable.Id;
                            }
                            
                                string cartime = "";
                                string carNo = "";
                                for (int j = 0; j < oledb_dr.FieldCount; j++)
                                {
                                    if (j == 0)
                                    {
                                        carNo = oledb_dr[j].ToString();
                                    }
                                    else
                                    {
                                        cartime += Convert.ToDateTime(oledb_dr[j]).ToString("HH:mm") + "、";
                                    }

                                }

                                cartime += "、.";
                                cartime = cartime.Replace("、、.", "");
                                cartime = cartime.Replace("、.", "");
                                Timetable timetablenewchild = new Timetable();
                                timetablenewchild.FileName = fileName;
                                timetablenewchild.CarNo = carNo;
                                timetablenewchild.ParentId = parentId;
                                timetablenewchild.Time = cartime;
                                timetablenewchild.Subject = timetable.Subject;
                                _db.Timetable.Add(timetablenewchild);
                                _db.SaveChanges();
                            


                        }
                    }
                    else
                    {
                        while (oledb_dr.Read())
                        {
                            i++;

                            if (i == 1)
                            {
                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    carlinestr = carlinestr + oledb_dr[j].ToString() + "→";
                                    string countstr = oledb_dr[j].ToString();
                                    var carsiteold = _db.CarSite.FirstOrDefault(x => x.Subject == countstr);
                                    if (carsiteold == null)
                                    {
                                        CarSite carSite = new CarSite();
                                        carSite.Subject = oledb_dr[j].ToString();
                                        carSite.Poster = timetable.Poster;
                                        carSite.InitDate = timetable.InitDate;
                                        _db.CarSite.Add(carSite);
                                    }



                                }
                                carlinestr += "→.";
                                carlinestr = carlinestr.Replace("→→.", "");
                                carlinestr = carlinestr.Replace("→.", "");

                                timetable.FileName = fileName;
                                timetable.CarLine = carlineidthanuse;
                                _db.Timetable.Add(timetable);
                                timetable.Create(_db, _db.Timetable);
                                CarLine carLine = _db.CarLine.Find(carlineid);
                                carLine.Line = carlinestr;
                                var carlineallsite = _db.CarLine.Where(x => x.ParentId == carLine.Id);
                                foreach (var item in carlineallsite)
                                {
                                    _db.CarLine.Remove(item);
                                }

                                _db.SaveChanges();
                                for (int j = 1; j < oledb_dr.FieldCount; j++)
                                {
                                    CarLine carLinenewsite = new CarLine();
                                    carLinenewsite.Subject = oledb_dr[j].ToString();
                                    carLinenewsite.ParentId = carlineid;
                                    carLinenewsite.ListNum = j;
                                    _db.CarLine.Add(carLinenewsite);
                                }
                                _db.SaveChanges();
                                var parenttimetable =
                                    _db.Timetable.FirstOrDefault(
                                        x => x.CarLine == carlineidthanuse && x.FileName == fileName && x.Subject == timetable.Subject);
                                parentId = parenttimetable.Id;



                            }
                            else
                            {
                                string cartime = "";
                                string carNo = "";
                                for (int j = 0; j < oledb_dr.FieldCount; j++)
                                {
                                    if (j == 0)
                                    {
                                        carNo = oledb_dr[j].ToString();
                                    }
                                    else
                                    {
                                        cartime += Convert.ToDateTime(oledb_dr[j]).ToString("HH:mm") + "、";
                                    }

                                }

                                cartime += "、.";
                                cartime = cartime.Replace("、、.", "");
                                cartime = cartime.Replace("、.", "");
                                Timetable timetablenewchild = new Timetable();
                                timetablenewchild.FileName = fileName;
                                timetablenewchild.CarNo = carNo;
                                timetablenewchild.ParentId = parentId;
                                timetablenewchild.Time = cartime;
                                timetablenewchild.Subject = timetable.Subject;
                                _db.Timetable.Add(timetablenewchild);
                                _db.SaveChanges();
                            }


                        }
                    }
                 
                    oledb_dr.Close();
                    oledb_con.Close();

                    return RedirectToAction("Index", new { Page = -1 });
                }









                //_db.Entry(timetable).State = EntityState.Modified;

            }
            return View(timetable);
        }

        public ActionResult Edit(int id = 0)
        {
            Timetable timetable = _db.Timetable.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            return View(timetable);
        }

        //
        // POST: /admin/Timetable/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Edit(Timetable timetable)
        {
            if (ModelState.IsValid)
            {

                //_db.Entry(timetable).State = EntityState.Modified;
                timetable.Update();
                return RedirectToAction("Index", new { Page = -1 });
            }
            return View(timetable);
        }

        //
        // GET: /admin/Timetable/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Timetable timetable = _db.Timetable.Find(id);
            if (timetable == null)
            {
                return HttpNotFound();
            }
            return View(timetable);
        }

        //
        // POST: /admin/Timetable/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            List<Timetable> timetablechild = _db.Timetable.Where(x => x.ParentId == id).ToList();
            if (timetablechild.Count > 0)
            {
                foreach (var item in timetablechild)
                {
                    _db.Timetable.Remove(item);
                }
                _db.SaveChanges();
            }

            Timetable timetable = _db.Timetable.Find(id);
            _db.Timetable.Remove(timetable);
            _db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            _db.Dispose();
            base.Dispose(disposing);
        }
    }

}
