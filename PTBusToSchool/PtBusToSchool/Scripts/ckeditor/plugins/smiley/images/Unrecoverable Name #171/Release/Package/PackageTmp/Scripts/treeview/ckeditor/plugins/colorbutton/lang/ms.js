d.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.DbMigrationsConfiguration.MigrationsNamespace">
            <summary>
                Gets or sets the namespace used for code-based migrations.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.DbMigrationsConfiguration.MigrationsDirectory">
            <summary>
                Gets or sets the sub-directory that code-based migrations are stored in.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.DbMigrationsConfiguration.CodeGenerator">
            <summary>
                Gets or sets the code generator to be used when scaffolding migrations.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.DbMigrationsConfiguration.MigrationsAssembly">
