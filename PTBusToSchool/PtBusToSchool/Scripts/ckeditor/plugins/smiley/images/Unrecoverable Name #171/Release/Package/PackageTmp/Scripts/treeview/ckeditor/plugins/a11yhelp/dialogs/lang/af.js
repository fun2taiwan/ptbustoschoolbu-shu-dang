a.Entity.Infrastructure.DbQuery`1.op_Implicit(System.Data.Entity.Infrastructure.DbQuery{`0})~System.Data.Entity.Infrastructure.DbQuery">
            <summary>
                Returns a new instance of the non-generic <see cref="T:System.Data.Entity.Infrastructure.DbQuery"/> class for this query.
            </summary>
            <returns>A non-generic version.</returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery`1.System#ComponentModel#IListSource#ContainsListCollection">
            <summary>
                Returns <c>false</c>.
            </summary>
            <returns><c>false</c>.</returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery`1.System#Linq#IQueryable#ElementType">
            <summary>
                The IQueryable element type.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery`1.System#Linq#IQueryable#Expression">
            <summary>
                The IQueryable LINQ Expression.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery`1.System#Linq#IQueryable#Provider">
            <summary>
                The IQueryable provider.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery`1.System#Data#Entity#Internal#Linq#IInternalQueryAdapter#InternalQuery">
            <summary>
                The internal query object that is backing this DbQuery
            </summary>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbQuery`1.InternalQuery">
            <summary>
                The internal query object that is backing this DbQuery
            </summary>
        </member>
        <member name="T:System.Data.Entity.IDbSet`1">
            <summary>
                An IDbSet represents the collection of all entities in the context, or that can be queried from the
                database, of a given type.  DbSet is a concrete implementation of IDbSet.
            </summary>
            <typeparam name = "TEntity">The type that defines the set.</typeparam>
        </member>
        <member name="M:System.Data.Entity.IDbSet`1.Find(System.Object[])">
            <summary>
                Finds an entity with the given primary key values.
                If an entity with the given primary key values exists in the context, then it is
                returned immediately without making a request to the store.  Otherwise, a request
                is made to the store for an entity with the given primary key values and this entity,
                if found, is attached to the context and returned.  If no entity is found in the
                context or the store, then null is returned.
            </summary>
            <remarks>
                The ordering of composite key values is as defined in the EDM, which is in turn as defined in
                the designer, by the Code First fluent API, or by the DataMember attribute.
            </remarks>
            <param name = "keyValues">The values of the primary key for the entity to be found.</param>
            <returns>The entity found, or null.</returns>
        </member>
        <member name="M:System.Data.Entity.IDbSet`1.Add(`0)">
            <summary>
                Adds the given entity to the context underlying the set in the Added state such that it will
                be inserted into the database when SaveChanges is called.
            </summary>
            <param name = "entity">The entity to add.</param>
            <returns>The entity.</returns>
            <remarks>
                Note that entities that are already in the context in some other state will have their state set
                to Added.  Add is a no-op if the entity is already in the context in the Added state.
            </remarks>
        </member>
      