:System.Data.Entity.Infrastructure.DbReferenceEntry`2.Name">
            <summary>
                Gets the property name.
            </summary>
            <value>The property name.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbReferenceEntry`2.CurrentValue">
            <summary>
                Gets or sets the current value of the navigation property.  The current value is
                the entity that the navigation property references.
            </summary>
            <value>The current value.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbReferenceEntry`2.IsLoaded">
            <summary>
                Gets a value indicating whether the entity has been loaded from the database.
            </summary>
            <value><c>true</c> if the entity is loaded; otherwise, <c>false</c>.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbReferenceEntry`2.InternalMemberEntry">
            <summary>
                Gets the underlying <see cref="T:System.Data.Entity.Internal.InternalReferenceEntry"/> as an <see cref="P:System.Data.Entity.Infrastructure.DbReferenceEntry`2.InternalMemberEntry"/>.
            </summary>
            <value>The internal member entry.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbReferenceEntry`2.EntityEntry">
            <summary>
                The <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry`1"/> to which this navigation property belongs.
            </summary>
            <value>An entry for the entity that owns this navigation property.</value>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbSqlQuery">
            <summary>
                Represents a SQL query for entities that is created from a <see cref="T:System.Data.Entity