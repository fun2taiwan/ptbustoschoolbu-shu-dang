namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delCarLineCarSitesICollection : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CarLineCarSites", "CarLine_Id", "dbo.CarLines");
            DropForeignKey("dbo.CarLineCarSites", "CarSite_Id", "dbo.CarSites");
            DropIndex("dbo.CarLineCarSites", new[] { "CarLine_Id" });
            DropIndex("dbo.CarLineCarSites", new[] { "CarSite_Id" });
            DropTable("dbo.CarLineCarSites");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.CarLineCarSites",
                c => new
                    {
                        CarLine_Id = c.Int(nullable: false),
                        CarSite_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CarLine_Id, t.CarSite_Id });
            
            CreateIndex("dbo.CarLineCarSites", "CarSite_Id");
            CreateIndex("dbo.CarLineCarSites", "CarLine_Id");
            AddForeignKey("dbo.CarLineCarSites", "CarSite_Id", "dbo.CarSites", "Id", cascadeDelete: true);
            AddForeignKey("dbo.CarLineCarSites", "CarLine_Id", "dbo.CarLines", "Id", cascadeDelete: true);
        }
    }
}
