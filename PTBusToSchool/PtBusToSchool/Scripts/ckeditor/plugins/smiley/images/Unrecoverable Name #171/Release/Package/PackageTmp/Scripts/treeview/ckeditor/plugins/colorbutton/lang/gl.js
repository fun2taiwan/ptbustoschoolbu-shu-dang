am name = "identifier">The identifier to be quoted.</param>
            <returns>The quoted identifier.</returns>
        </member>
        <member name="T:System.Data.Entity.Migrations.History.HistoryRow">
            <summary>
                This class is used by Code First Migrations to read and write migration history
                from the database. It is not intended to be used by other code and is only public
                so that it can be accessed by EF when running under partial trust. It may be
                changed or removed in the future.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.History.HistoryRow.MigrationId">
            <summary>
                Gets or sets the Id of the migration this row represents.
            </summary>
        </member>
        <member name="P:System.Data.Entity.Migrations.History.HistoryRow.CreatedOn">
            <summary>
     