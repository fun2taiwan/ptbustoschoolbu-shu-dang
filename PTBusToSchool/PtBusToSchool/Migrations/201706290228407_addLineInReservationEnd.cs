namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addLineInReservationEnd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReservationEnds", "Line", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReservationEnds", "Line");
        }
    }
}
