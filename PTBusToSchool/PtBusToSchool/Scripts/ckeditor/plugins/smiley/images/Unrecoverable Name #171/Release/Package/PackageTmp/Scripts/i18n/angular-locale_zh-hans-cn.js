tions.Generic.IDictionary{System.Object,System.Object})"/> method is overridden.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.Equals(System.Object)">
            <summary>
                Determines whether the specified <see cref="T:System.Object"/> is equal to this instance.
                Two <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> instances are considered equal if they are both entries for
                the same entity on the same <see cref="T:System.Data.Entity.DbContext"/>.
            </summary>
            <param name="obj">The <see cref="T:System.Object"/> to compare with this instance.</param>
            <returns>
                <c>true</c> if the specified <see cref="T:System.Object"/> is equal to this instance; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.Equals(System.Data.Entity.Infrastructure.DbEntityEntry)">
            <summary>
                Determines whether the specified <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> is equal to this instance.
                Two <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> instances are considered equal if they are both entries for
                the same entity on the same <see cref="T:System.Data.Entity.DbContext"/>.
            </summary>
            <param name="other">The <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> to compare with this instance.</param>
            <returns>
                <c>true</c> if the specified <see cref="T:System.Data.Entity.Infrastructure.DbEntityEntry"/> is equal to this instance; otherwise, <c>false</c>.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbEntityEntry.GetHashCode">
            <summary>
                Returns a hash code for this instance.
            </summary>
            <returns>
                A hash code for this instance, suitable for use in hashi