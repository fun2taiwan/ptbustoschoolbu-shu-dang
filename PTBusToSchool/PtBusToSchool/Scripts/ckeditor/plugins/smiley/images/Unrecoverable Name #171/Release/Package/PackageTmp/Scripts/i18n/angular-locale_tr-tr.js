urns>
                A <see cref="T:System.String"/> that represents this instance.
            </returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbSqlQuery.System#ComponentModel#IListSource#GetList">
            <summary>
                Throws an exception indicating that binding directly to a store query is not supported.
            </summary>
            <returns>
                Never returns; always throws.
            </returns>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbSqlQuery.InternalQuery">
            <summary>
                Gets the internal query.
            </summary>
            <value>The internal query.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbSqlQuery.System#ComponentModel#IListSource#ContainsListCollection">
            <summary>
                Returns <c>false</c>.
            </summary>
            <returns><c>false</c>.</returns>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbSqlQuery`1">
            <summary>
                Represents a SQL query for entities that is created from a <see cref="T:System.Data.Entity.DbContext"/> 
                and is executed using the connection from that context.
                Instances of this class are obtained from the <see cref="T:System.Data.Entity.DbSet`1"/> instance for the 
                entity type. The query is not executed when this object is created; it is executed
                each time it is enumerated, for example by using foreach.
                SQL queries for non-entities are created using the <see cref="P:System.Data.Entity.DbContext.Database"/>.
                See <see cref="T:System.Data.Entity.Infrastructure.DbSqlQuery"/> for a non-generic version of this class.
            </summary>
        </member>
        <member nam