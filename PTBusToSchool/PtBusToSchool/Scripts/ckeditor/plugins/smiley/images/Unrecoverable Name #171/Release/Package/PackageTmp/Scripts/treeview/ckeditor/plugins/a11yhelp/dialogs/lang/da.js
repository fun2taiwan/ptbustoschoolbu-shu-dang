.InternalSet">
            <summary>
                The underlying internal set.
            </summary>
        </member>
        <member name="M:System.Data.Entity.DbSet.#ctor">
            <summary>
                Internal constructor prevents external classes deriving from DbSet.
            </summary>
        </member>
        <member name="M:System.Data.Entity.DbSet.Find(System.Object[])">
            <summary>
                Finds an entity with the given primary key values.
                If an entity with the given primary key values exists in the context, then it is
                returned immediately without making a request to the store.  Otherwise, a request
                is made to the store for an entity with the given primary key values and this entity,
                if found, is attached to the context and returned.  If no entity is found in the
                context or the store, then null is returned.
            </summary>
            <remarks>
                The ordering of composite key values is as defined in the EDM, which is in turn as defined in
                the designer, by the Code First fluent API, or by the DataMember attribute.
            </remarks>
            <param name="keyValues">The values of the primary key for the entity to be found.</param>
            <returns>The entity found, or null.</returns>
            <exception cref="T:System.InvalidOperationException">Thrown if multiple entities exist in the context with the primary key values given.</exception>
            <exception cref="T:System.InvalidOperationException">Thrown if the type of entity is not part of the data model for this context.</exception>
            <exception cref="T:System.InvalidOperationException">Thrown if the types of the key values do not match the types of the key values for the entity type to be found.</exception>
            <exception cref="T:System.InvalidOperationException">Thrown if the context has been disposed.</exception>
        </member>
        <member name="M:System.Data.Entity.DbSet.Attach(System.Object)">
            <summary>
                Attaches the given entity to the context underlying the set.  That is, the entity is placed
                into the context in the Unchanged state, just as if it had been read from the database.
            </summary>
            <param name = "entity">The entity to attach.</param>
            <returns>The entity.</returns>
            <remarks>
                Attach is used to repopulate a context with an entity that is known to already exist in the database.
                SaveChanges will therefore not attempt to insert an attached entity into the database because
                it is assumed to already be there.
                Note that entities that are already in the context in some other state will have their state set
                to Unchanged.  Attach is a no-op if the entity is already in the context in the Unchanged state.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbSet.Add(System.Object)">
            <summary>
                Adds the given entity to the context underlying the set in the Added state such that it will
                be inserted into the database when SaveChanges is called.
            </summary>
            <param name = "entity">The entity to add.</param>
            <returns>The entity.</returns>
            <remarks>
                Note that entities that are already in the context in some other state will have their state set
                to Added.  Add is a no-op if the entity is already in the context in the Added state.
            </remarks>
        </member>
        <member name="M:System.Data.Entity.DbSet.Remove(System.Object)">
            <summary>
                Marks the given entity as Deleted such that it will be deleted from the database when SaveChanges
                is called.  Note that the entity must exist in the context in some