﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace PtBusToSchool.Models
{
    public enum GenderType
    {
        男,
        女,
        不指定
    }

    public enum TargetType
    {
        另開視窗,
        本地開啟
    }

   

    public enum BooleanType
    {
        是,
        否
    }

    public enum ProcessState
    {
        搶修中,
        已排除
    }

    public enum CarState
    {
        故障中,
        維修及保養,
        淘汰,
        正常
    }
    public enum CarStyle
    {
        大巴,
        中巴,
        小巴
    }

    public enum ReservationState
    {
        尚未媒合,
        取消預約,
        預約成功,
        預約失敗,
        候補
    }

   

}