ions.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.RenameColumnOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.RenameColumnOperation"/>.
            </summary>
            <param name="renameColumnOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.Generate(System.Data.Entity.Migrations.Model.SqlOperation,System.Data.Entity.Migrations.Utilities.IndentedTextWriter)">
            <summary>
                Generates code to perform a <see cref="T:System.Data.Entity.Migrations.Model.SqlOperation"/>.
            </summary>
            <param name="sqlOperation">The operation to generate code for.</param>
            <param name="writer">Text writer to add the generated code to.</param>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.ScrubName(System.String)">
            <summary>
                Removes any invalid characters from the name of an database artifact.
            </summary>
            <param name = "name">The name to be scrubbed.</param>
            <returns>The scrubbed name.</returns>
        </member>
        <member name="M:System.Data.Entity.Migrations.Design.CSharpMigrationCodeGenerator.TranslateColumnType(System.Data.Metadata.Edm.PrimitiveTypeKind)">
            <summary>
                Gets the type name to use for a column of the given data type.
            </summary>
            <param name = "primitiveTypeKind">The data type to translate.</param>
            <returns>The type name to use in the generated migration.<