namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAllPeopleInSendCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendCars", "AllPeople", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SendCars", "AllPeople");
        }
    }
}
