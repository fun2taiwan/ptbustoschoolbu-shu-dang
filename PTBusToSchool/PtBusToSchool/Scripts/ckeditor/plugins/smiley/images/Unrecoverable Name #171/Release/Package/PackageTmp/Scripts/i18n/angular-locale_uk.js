:System.Data.Entity.Infrastructure.DbPropertyValues.Item(System.String)">
            <summary>
                Gets or sets the value of the property with the specified property name.
                The value may be a nested instance of this class.
            </summary>
            <param name = "propertyName">The property name.</param>
            <value>The value of the property.</value>
        </member>
        <member name="P:System.Data.Entity.Infrastructure.DbPropertyValues.InternalPropertyValues">
            <summary>
                Gets the internal dictionary.
            </summary>
            <value>The internal dictionary.</value>
        </member>
        <member name="T:System.Data.Entity.Infrastructure.DbReferenceEntry">
            <summary>
                A non-generic version of the <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry`2"/> class.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry.Create(System.Data.Entity.Internal.InternalReferenceEntry)">
            <summary>
                Creates a <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry"/> from information in the given <see cref="T:System.Data.Entity.Internal.InternalReferenceEntry"/>.
                Use this method in preference to the constructor since it may potentially create a subclass depending on
                the type of member represented by the InternalCollectionEntry instance.
            </summary>
            <param name="internalReferenceEntry">The internal reference entry.</param>
            <returns>The new entry.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry.#ctor(System.Data.Entity.Internal.InternalReferenceEntry)">
            <summary>
                Initializes a new instance of the <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry"/> class.
            </summary>
            <param name="internalReferenceEntry">The internal entry.</param>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry.Load">
            <summary>
                Loads the entity from the database.
                Note that if the entity already exists in the context, then it will not overwritten with values from the database.
            </summary>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry.Query">
            <summary>
                Returns the query that would be used to load this entity from the database.
                The returned query can be modified using LINQ to perform filtering or operations in the database.
            </summary>
            <returns>A query for the entity.</returns>
        </member>
        <member name="M:System.Data.Entity.Infrastructure.DbReferenceEntry.Cast``2">
            <summary>
                Returns the equivalent generic <see cref="T:System.Data.Entity.Infrastructure.DbReferenceEntry`2"/> object.
            </summary>
            <typeparam name="TEntity">The type of entity on which the