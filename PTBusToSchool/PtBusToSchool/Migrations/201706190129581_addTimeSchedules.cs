namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTimeSchedules : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TimeSchedules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CarNo = c.String(),
                        CarLineId = c.Int(),
                        Time = c.String(),
                        FileName = c.String(),
                        ParentId = c.Int(),
                        Poster = c.String(maxLength: 20),
                        InitDate = c.DateTime(),
                        Updater = c.String(maxLength: 20),
                        UpdateDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarLines", t => t.CarLineId)
                .ForeignKey("dbo.TimeSchedules", t => t.ParentId)
                .Index(t => t.CarLineId)
                .Index(t => t.ParentId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TimeSchedules", new[] { "ParentId" });
            DropIndex("dbo.TimeSchedules", new[] { "CarLineId" });
            DropForeignKey("dbo.TimeSchedules", "ParentId", "dbo.TimeSchedules");
            DropForeignKey("dbo.TimeSchedules", "CarLineId", "dbo.CarLines");
            DropTable("dbo.TimeSchedules");
        }
    }
}
