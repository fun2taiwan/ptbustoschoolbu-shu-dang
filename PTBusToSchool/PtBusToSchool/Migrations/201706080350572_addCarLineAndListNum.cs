namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCarLineAndListNum : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CarLines", "ListNum", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CarLines", "ListNum");
        }
    }
}
