ollection of <see cref="T:System.Web.Http.ModelBinding.ModelBinderProvider" /> objects.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetModelMetadataProvider(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Metadata.ModelMetadataProvider" /> service.</summary>
      <returns>Returns an  <see cref="T:System.Web.Http.Metadata.ModelMetadataProvider" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetModelValidatorProviders(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Validation.ModelValidatorProvider" /> collection.</summary>
      <returns>Returns a collection of<see cref="T:System.Web.Http.Validation.ModelValidatorProvider" />objects.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetStructuredQueryBuilder(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Query.IStructuredQueryBuilder" /> service.</summary>
      <returns>Returns an<see cref="T:System.Web.Http.Query.IStructuredQueryBuilder" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetTraceManager(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Tracing.ITraceManager" /> service.</summary>
      <returns>Returns an<see cref="T:System.Web.Http.Tracing.ITraceManager" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetTraceWriter(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.Tracing.ITraceWriter" />service. </summary>
      <returns>Returns an<see cref="T:System.Web.Http.Tracing.ITraceWriter" />instance.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="M:System.Web.Http.ServicesExtensions.GetValueProviderFactories(System.Web.Http.Controllers.ServicesContainer)">
      <summary>Gets the <see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" /> collection.</summary>
      <returns>Returns  a colleciton of<see cref="T:System.Web.Http.ValueProviders.ValueProviderFactory" />objects.</returns>
      <param name="services">The services container.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.ApiControllerActionInvoker">
      <summary>Invokes the action methods of a controller.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ApiControllerActionInvoker.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ApiControllerActionInvoker" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ApiControllerActionInvoker.InvokeActionAsync(System.Web.Http.Controllers.HttpActionContext,System.Threading.CancellationToken)">
      <summary>Asynchronously invokes the specified action by using the specified controller context.</summary>
      <returns>The invoked action.</returns>
      <param name="actionContext">The controller context.</param>
      <param name="cancellationToken">The cancellation token.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.ApiControllerActionSelector">
      <summary>Represents a reflection based action selector.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ApiControllerActionSelector.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.Controllers.ApiControllerActionSelector" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.Controllers.ApiControllerActionSelector.GetActionMapping(System.Web.Http.Controllers.HttpControllerDescriptor)">
      <summary>Gets the action mappings for the <see cref="T:System.Web.Http.Controllers.ApiControllerActionSelector" />.</summary>
      <returns>The action mappings.</returns>
      <param name="controllerDescriptor">The information that describes a controller.</param>
    </member>
    <member name="M:System.Web.Http.Controllers.ApiControllerActionSelector.SelectAction(System.Web.Http.Controllers.HttpControllerContext)">
      <summary>Selects an action for the <see cref="T:System.Web.Http.Controllers.ApiControllerActionSelector" />.</summary>
      <returns>The selected action.</returns>
      <param name="controllerContext">The controller context.</param>
    </member>
    <member name="T:System.Web.Http.Controllers.ControllerServices">
      <summary> Represents a