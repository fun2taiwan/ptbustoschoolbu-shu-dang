the properties in the EdmEntityType defines ConcurrencyMode. Otherwise return false.
            </summary>
            <param name = "entityType"> </param>
            <returns> </returns>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.DataModelValidationHelper.AddMemberNameToHashSet(System.Data.Entity.Edm.EdmNamedMetadataItem,System.Collections.Generic.HashSet{System.String},System.Data.Entity.Edm.Validation.Internal.DataModelValidationContext,System.Func{System.String,System.String})">
            <summary>
                Add member name to the Hash set, raise an error if the name exists already.
            </summary>
            <param name = "item"> </param>
            <param name = "memberNameList"> </param>
            <param name = "context"> </param>
            <param name = "getErrorString"> </param>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.DataModelValidationHelper.HasContent(System.String)">
            <summary>
                If the string is null, empty, or only whitespace, return false, otherwise return true
            </summary>
            <param name = "stringToCheck"> </param>
            <returns> </returns>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.DataModelValidationHelper.CheckForInheritanceCycle``1(``0,System.Func{``0,``0})">
            <summary>
                Determine if a cycle exists in the type hierarchy: use two pointers to walk the chain, if one catches up with the other, we have a cycle.
            </summary>
            <returns> true if a cycle exists in the type hierarchy, false otherwise </returns>
        </member>
        <member name="T:System.Data.Entity.Edm.Validation.Internal.DataModelValidationRuleSet">
            <summary>
                RuleSet for DataModel Validation
            </summary>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.DataModelValidationRuleSet.GetRules(System.Data.Entity.Edm.Common.DataModelItem)">
            <summary>
                Get the related rules given certain DataModelItem
            </summary>
            <param name="itemToValidate"> The <see cref="T:System.Data.Entity.Edm.Common.DataModelItem"/> to validate </param>
            <returns> A collection of <see cref="T:System.Data.Entity.Edm.Validation.Internal.DataModelValidationRule"/> </returns>
        </member>
        <member name="T:System.Data.Entity.Edm.Validation.Internal.DataModelValidator">
            <summary>
                Data Model Validator
            </summary>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.DataModelValidator.Validate(System.Data.Entity.Edm.EdmModel,System.Boolean)">
            <summary>
                Validate the <see cref="N:System.Data.Entity.Edm.Validation.Internal.EdmModel"/> and all of its properties given certain version.
            </summary>
            <param name="root"> The root of the model to be validated </param>
            <param name="validateSyntax"> True to validate the syntax, otherwise false </param>
        </member>
        <member name="T:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelRuleSet">
            <summary>
                The RuleSet for EdmModel
            </summary>
        </member>
        <member name="M:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelRuleSet.CreateEdmModelRuleSet(System.Double,System.Boolean)">
            <summary>
                Get <see cref="T:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelRuleSet"/> based on version
            </summary>
            <param name="version"> a double value of version </param>
            <returns> <see cref="T:System.Data.Entity.Edm.Validation.Internal.EdmModel.EdmModelRuleSet"/> </returns>
        </member>
        <member nam