aram>
      <param name="defaults">An object that contains default route values.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollectionExtensions.MapHttpRoute(System.Web.Http.HttpRouteCollection,System.String,System.String,System.Object,System.Object)">
      <summary>Maps the specified route template and sets default route values and constraints.</summary>
      <returns>A reference to the mapped route.</returns>
      <param name="routes">A collection of routes for the application.</param>
      <param name="name">The name of the route to map.</param>
      <param name="routeTemplate">The route template for the route.</param>
      <param name="defaults">An object that contains default route values.</param>
      <param name="constraints">A set of expressions that constrain the values for routeTemplate.</param>
    </member>
    <member name="M:System.Web.Http.HttpRouteCollectionExtensions.MapHttpRoute(System.Web.Http.HttpRouteCollection,System.String,System.String,System.Object,System.Object,System.Net.Http.HttpMessageHandler)">
      <summary> Maps the specified route template and sets default route values, constraints, and end-point message handler. </summary>
      <returns>A reference to the mapped route.</returns>
      <param name="routes">A collection of routes for the application.</param>
      <param name="name">The name of the route to map.</param>
      <param name="routeTemplate">The route template for the route.</param>
      <param name="defaults">An object that contains default route values.</param>
      <param name="constraints">A set of expressions that constrain the values for routeTemplate.</param>
      <param name="handler">The handler to which the request will be dispatched.</param>
    </member>
    <member name="T:System.Web.Http.HttpServer">
      <summary> Defines an implementation of an <see cref="T:System.Net.Http.HttpMessageHandler" /> which dispatches an  incoming <see cref="T:System.Net.Http.HttpRequestMessage" /> and creates an <see cref="T:System.Net.Http.HttpResponseMessage" /> as a result. </summary>
    </member>
    <member name="M:System.Web.Http.HttpServer.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpServer" /> class, using the default configuration and dispatcher.</summary>
    </member>
    <member name="M:System.Web.Http.HttpServer.#ctor(System.Net.Http.HttpMessageHandler)">
      <summary> Initializes a new instance of the <see cref="T:System.Web.Http.HttpServer" /> class with a specified dispatcher. </summary>
      <param name="dispatcher">The HTTP dispatcher that will handle incoming requests.</param>
    </member>
    <member name="M:System.Web.Http.HttpServer.#ctor(System.Web.Http.HttpConfiguration)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpServer" /> class with a specified configuration.</summary>
      <param name="configuration">The <see cref="T:System.Web.Http.HttpConfiguration" /> used to configure this instance.</param>
    </member>
    <member name="M:System.Web.Http.HttpServer.#ctor(System.Web.Http.HttpConfiguration,System.Net.Http.HttpMessageHandler)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.HttpServer" /> class with a specified configuration and dispatcher.</summary>
      <param name="configuration">The <see cref="T:System.Web.Http.HttpConfiguration" /> used to configure this instance.</param>
      <param name="dispatcher">The HTTP dispatcher that will handle incoming requests.</param>
    </member>
    <member name="P:System.Web.Http.HttpServer.Configuration">
      <summary>Gets the <see cref="T:System.Web.Http.HttpConfiguration" /> used to configure this instance.</summary>
      <returns>The <see cref="T:System.Web.Http.HttpConfiguration" /> used to configure this instance.</returns>
    </member>
    <member name="P:System.Web.Http.HttpServer.Dispatcher">
      <summary>Gets the HTTP dispatcher that handles incoming requests.</summary>
      <returns>The HTTP dispatcher that handles incoming requests.</returns>
    </member>
    <member name="M:System.Web.Http.HttpServer.Dispose(System.Boolean)">
      <summary>Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.</summary>
      <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resourc