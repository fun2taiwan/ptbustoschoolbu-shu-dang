System.String,System.String)">
      <summary>Opens a connection to a database using a connection string and the specified provider.</summary>
      <returns>The database instance.</returns>
      <param name="connectionString">The connection string that contains information that is used to connect to a database.</param>
      <param name="providerName">(Optional) The name of the .NET Framework data provider to use to connect to the data source.</param>
      <exception cref="T:System.ArgumentException">
        <paramref name="connectionString" /> is null or empty.</exception>
    </member>
    <member name="M:WebMatrix.Data.Database.Query(System.String,System.Object[])">
      <summary>Executes a SQL query that returns a list of rows as the result.</summary>
      <returns>The rows returned by the SQL query.</returns>
      <param name="commandText">The SQL query to execute.</param>
      <param name="parameters">(Optional) Parameters to pass to the SQL query.</param>
    </member>
    <member name="M:WebMatrix.Data.Database.QuerySingle(System.String,System.Object[])">
      <summary>Executes a SQL query that returns a single row as the result.</summary>
      <returns>The single row returned by the SQL query.</returns>
      <param name="commandText">The SQL query to execute.</param>
      <param name="args">(Optional) Parameters to pass to the SQL query.</param>
    </member>
    <member name="M:WebMatrix.Data.Database.QueryValue(System.String,System.Object[])">
      <summary>Executes a SQL query that returns a single scalar value as the result.</summary>
      <returns>The scalar value returned by the SQL query.</returns>
      <param name="commandText">The SQL query to execute.</param>
      <param name="args">(Optional) Parameters to pass to the SQL query.</param>
    </member>
    <member name="T:WebMatrix.Data.DynamicRecord">
      <summary>Represents a data record by using a custom type descriptor and the capabilities of the Dynamic Language Runtime (DLR).</summary>
    </member>
    <member name="P:WebMatrix.Data.DynamicRecord.Columns">
      <summary>Returns a list that contains the name of each column in the <see cref="T:WebMatrix.Data.DynamicRecord" /> instance.</summary>
      <returns>A list that contains the name of each column.</returns>
    </member>
    <member name="M:WebMatrix.Data.DynamicRecord.GetDynamicMemberNames">
      <summary>Returns a list that contains the name of all dynamic members of the <see cref="T:WebMatrix.Data.DynamicRecord" /> instance.</summary>
      <returns>A list that contains the name of every dynamic member.</returns>
    </member>
    <member name="P:WebMatrix.Data.DynamicRecord.Item(System.Int32)">
      <summary>Returns the value of a column in the <see cref="T:WebMatrix.Data.DynamicRecord" /> instance using the specified index.</summary>
      <returns>The value of the specified column.</returns>
      <param name="index">The zero-based index of the column that contains the value to return.</param>
    </member>
    <member name="P:WebMatrix.Data.DynamicRecord.Item(System.String)">
      <summary>Returns the value of a column in the <see cref="T:WebMatrix.Data.DynamicRecord" /> instance using the specified name.</summary>
      <returns>The value of the specified column.</returns>
      <param name="name">The name of the column that contains the value to return. Name matching is case-insensitive.</param>
      <exception cref="T:System.InvalidOperationException">The <see cref="T:WebMatrix.Data.DynamicRecord" /> instance does not contain a column whose name is a case-insensitive match with the specified name.</exception>
    </member>
    <member name="M:WebMatrix.Data.DynamicRecord.System#ComponentModel#ICustomTypeDescriptor#GetAttributes">
      <summary>Returns a list of custom attributes for this instance of a component.</summary>
      <returns>
        <see cref="P:System.ComponentModel.AttributeCollection.Empty" /> in all cases.</returns>
    </member>
    <member name="M:WebMatrix.Data.DynamicRecord.System#ComponentModel#ICustomTypeDescriptor#GetClassName">
      <summary>Returns the class name for this instance of a component.</summary>
      <returns>null in all cases.</returns>
    </member>
    <member name="M:WebMatrix.Data.DynamicRecord.System#ComponentModel#ICustomTypeDescriptor#GetComponentName">
      <summary>Returns the name for this instance of a component.</summary>
      <returns>null in all cases.</returns>
    </member>
    <member name="M:WebMatrix.Data.DynamicRecord.System#ComponentModel#ICustomTypeDescriptor#GetConverter">
      <summary>Returns the type converter for this instance of a component.</summary>
      <returns>null in all cases.</returns>
    </member>
    <member name="M:WebMatrix.Data.DynamicRecord.System#ComponentModel#ICustomTypeDescriptor#GetDefaultEvent">
      <summary>Returns the default event for this instance of a component.</summary>
      <returns>null in all cases.</returns>
    <