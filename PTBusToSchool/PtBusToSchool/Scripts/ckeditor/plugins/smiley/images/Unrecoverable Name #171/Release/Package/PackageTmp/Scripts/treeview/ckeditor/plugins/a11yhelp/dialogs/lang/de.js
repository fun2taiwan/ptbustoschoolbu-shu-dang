ders.MutableObjectModelBinder.CanUpdateProperty(System.Web.Http.Metadata.ModelMetadata)">
      <summary>Retrieves a value that indicates whether a property can be updated.</summary>
      <returns>true if the property can be updated; otherwise, false.</returns>
      <param name="propertyMetadata">The metadata for the property to be evaluated.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinder.CreateModel(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Creates an instance of the model.</summary>
      <returns>The newly created model object.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinder.EnsureModel(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Creates a model instance if an instance does not yet exist in the binding context.</summary>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinder.GetMetadataForProperties(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext)">
      <summary>Retrieves metadata for properties of the model.</summary>
      <returns>The metadata for properties of the model.</returns>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinder.SetProperty(System.Web.Http.Controllers.HttpActionContext,System.Web.Http.ModelBinding.ModelBindingContext,System.Web.Http.Metadata.ModelMetadata,System.Web.Http.ModelBinding.Binders.ComplexModelDtoResult,System.Web.Http.Validation.ModelValidator)">
      <summary>Sets the value of a specified property.</summary>
      <param name="actionContext">The action context.</param>
      <param name="bindingContext">The binding context.</param>
      <param name="propertyMetadata">The metadata for the property to set.</param>
      <param name="dtoResult">The validation information about the property.</param>
      <param name="requiredValidator">The validator for the model.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinderProvider">
      <summary>Provides a model binder for mutable objects.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinderProvider.#ctor">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinderProvider" /> class.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.MutableObjectModelBinderProvider.GetBinder(System.Web.Http.HttpConfiguration,System.Type)">
      <summary>Retrieves the model binder for the specified type.</summary>
      <returns>The model binder.</returns>
      <param name="configuration">The configuration.</param>
      <param name="modelType">The type of the model to retrieve.</param>
    </member>
    <member name="T:System.Web.Http.ModelBinding.Binders.SimpleModelBinderProvider">
      <summary>No content here will be updated; please do not add material here.</summary>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.SimpleModelBinderProvider.#ctor(System.Type,System.Func{System.Web.Http.ModelBinding.IModelBinder})">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.SimpleModelBinderProvider" /> class.</summary>
      <param name="modelType">The model type.</param>
      <param name="modelBinderFactory">The model binder factory.</param>
    </member>
    <member name="M:System.Web.Http.ModelBinding.Binders.SimpleModelBinderProvider.#ctor(System.Type,System.Web.Http.ModelBinding.IModelBinder)">
      <summary>Initializes a new instance of the <see cref="T:System.Web.Http.ModelBinding.Binders.SimpleModelBinderProvider" /> class by using the specified model type and the model binder.</summary>
      <param name="modelType">The model type.</param>
      <param name="modelBinder">The model binder.</param>
    </member>
    <member name="M:Syst