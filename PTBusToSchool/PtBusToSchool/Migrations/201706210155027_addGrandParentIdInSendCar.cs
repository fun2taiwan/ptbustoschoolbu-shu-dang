namespace PtBusToSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addGrandParentIdInSendCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SendCars", "GrandParentId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SendCars", "GrandParentId");
        }
    }
}
